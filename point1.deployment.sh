#!/usr/bin/env bash

function err {
	echo "error: $1"
	exit 1
}

if [ "x$1" = x ]; then
	echo "usage: $0 {git tag of CastorScript to deploy}"
	echo "example: $0 CastorScript-02-00-01"
	exit 1
fi

DIR=`mktemp -d -p $HOME`
# mktemp creates dir with u+rwx only
chmod o+rx $DIR || err "cannot change permissions of $DIR"
cd $DIR || err "cannot cd into $DIR"

git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-software/CastorScript.git || err "git clone"
cd CastorScript

git checkout $1 || err "could not checkout tag $1"

# Check that git is at a normalized tag
if git status | head -n 1 | grep -E 'HEAD detached at CastorScript(-[0-9]{2}){3}'; then
	ACTUAL_VERSION=`git status | head -n 1 | sed -r 's/.*HEAD detached at (CastorScript(-[0-9]{2}){3}).*/\1/'`
	echo "deploying CastorScript version: $ACTUAL_VERSION"
	echo $ACTUAL_VERSION > VERSION
else
	err "Git working area is not at a normalized tag: aborting deployment"
fi

# DB disaster recovery are deployed as part of the CastorScript
mv ProductionTools/oracle_db_disaster_recovery . || err "cannot mv oracle_db_disaster_recovery"
# operation tools
mv ProductionTools/installed tools || err "cannot mv installed tools"

# remove from local directory what we don't want to be deployed
rm -rf Configs DeploymentTest ProductionTools UnitTests .git || err "cannot delete"
rm .gitignore .gitlab-ci.yml pylintrc readme.md || err "cannot delete"
rm point1.deployment.sh || err "cannot delete"
rm run_docker_container.sh run_pylint.sh run_unit_tests.sh || err "cannot delete"

# compile with python interpreter from TDAQ
# execute in subshell to avoid importing TDAQ stuffs in here
(
  source /sw/tdaq/setup/setup_tdaq-11-02-01.sh
  python -m compileall . || err "compiling"
)

# change persmissions
chmod -R ug+w . || err "chmod"
chgrp -R zp . || err "chgrp"

# synchronize to /sw/castor
sudo -u atdadmin /daq_area/tools/sync/remote_sync.sh -c -t castor -s . || err "synchronizing"

# everything went fine so restore working area
echo "deployment successful"

rm -rf $DIR
