#!/bin/env tdaq_python
from queue import Queue
from time import time
import threading
import sys
import os
import datetime
import signal
import importlib

from cs.Threads.ManagerThread import ManagerThread
from cs.Threads.CopyThread import CopyThread
from cs.Threads.DeleteThread import DeleteThread
from cs.Threads.ERSThread import ERSThread
from cs.Threads.DdmMonitoringThread import DdmMonitoringThread
from cs.Threads.InfoServiceThread import InfoServiceThread
import cs.Tools.ConfigParser as ConfigParser
import cs.Tools.Constants as Constants
import cs.Tools.LogConfig as LogConfig
import cs.Tools.krb as krb
import cs.Tools.Libraries.Database as Database
from cs.Tools.utils import thread_id_string

def main_exit(signal_number, stackframe):
    del signal_number, stackframe
    logger.debug('exit signal received')
    exit_event.set()


def checkDB(db_, logger_, dblogger_, parser_, conf_):
    global db_last_connection_time

    copy_db_flag = (conf_.CopyEnabled == False) or copy_thread.getDBFlag()
    delete_db_flag = (conf_.DeleteEnabled == False) or delete_thread.getDBFlag()
    manager_db_flag = manager_thread.getDBFlag()
    db_flag = copy_db_flag and delete_db_flag and manager_db_flag
    if not db_flag:
        logger_.warning('database connection issue: copy: %s, delete: %s, manager: %s',
                copy_db_flag, delete_db_flag, manager_db_flag)
        db_ = None
    if not db_ and conf_.DBURL:
        logger_.debug('connecting to database')
        try:
            db_ = Database.Database(conf_, dblogger_, parser_)
            logger_.info('connection to database succeeded')
            db_last_connection_time = time()
        except Exception as exc:
            logger_.error('error connecting to DB: %s', str(exc))

        manager_thread.setDB(db_)
        if conf_.CopyEnabled:
            copy_thread.setDB(db_)
        if conf_.DeleteEnabled:
            delete_thread.setDB(db_)
    return db_


if len(sys.argv) >= 2:
    try:
        config = ConfigParser.ConfigHolder(sys.argv[1])
    except Exception as ex:
        print('configuration file parsing failed because:', ex)
        sys.exit(1)
else:
    print('error: no configuration file')
    print('usage: {} {{configfile.cfg}}'.format(sys.argv[0]))
    sys.exit(1)

if not config.CopyEnabled and not config.DeleteEnabled:
    print('error: copy and deletion are both disabled, exiting')
    sys.exit(1)

# Change umask to ensure text files are group writeable
os.umask(0o002)

# Main objects instantiation
exit_event = threading.Event()
db_lock = threading.Lock()

copyqueue = Queue()
deletequeue = Queue()
clearqueue = Queue()

ddmqueue = None
if config.DdmMonitoringEnabled:
    ddmqueue = Queue(config.DdmMonitoringQueueMaxSize)

manager_thread = ManagerThread(config, exit_event, db_lock, copyqueue,
        deletequeue, clearqueue)

copy_thread = None
if config.CopyEnabled:
    copy_thread = CopyThread(config, exit_event, db_lock, copyqueue,
            deletequeue, clearqueue, ddmqueue)

delete_thread = None
if config.DeleteEnabled:
    delete_thread = DeleteThread(config, exit_event, db_lock, deletequeue,
            clearqueue)

if config.DdmMonitoringEnabled:
    ddm_publisher = DdmMonitoringThread(config, exit_event, ddmqueue)

if config.ISEnabled:
    info_service_thread = InfoServiceThread(config, exit_event,
            copy_thread, delete_thread, manager_thread)

if config.ERSEnabled:
    ers_thread = ERSThread(config, exit_event)

# General configuration
LogConfig.enable_root_logger()

if config.EmailLogList:
    LogConfig.enable_mail_logging(config)

logger = LogConfig.enable_file_logging("main", "main.log", config)

# Load filename parser
try:
    filename_parser_module = importlib.import_module(
            "cs.Tools.FilenameParsers." + config.FilenameParser)
except Exception as ex:
    logger.error('cannot import parser module %s because: "%s"; exiting',
                    config.FilenameParser, str(ex))
    sys.exit(1)

if config.FilenameParser not in dir(filename_parser_module):
    logger.error('cannot find class "%s" in the parser module; exiting',
            config.FilenameParser)
    sys.exit(1)

# We do not want to trap exceptions from the module
# initialization function. So, first we check if it exists
# then we execute it outside the try-except environment
filename_parser_init_function = None
try:
    filename_parser_init_function = filename_parser_module.init
except AttributeError:
    pass

if filename_parser_init_function:
    filename_parser_init_function(config.FilenameParserConf)

filename_parser = getattr(filename_parser_module, config.FilenameParser)


missing_symbols = [x for x in Constants.needed_parser_symbols if x not in dir(filename_parser)]

if missing_symbols:
    logger.error('missing functions in the filename parser: %s;  exiting',
            str(missing_symbols))
    sys.exit(1)

manager_thread.setFilenameParser(filename_parser)
if config.CopyEnabled:
    copy_thread.setFilenameParser(filename_parser)

# Set exit signal handlers
# SIGUSR2 = 12
signal.signal(signal.SIGUSR2, main_exit)
signal.signal(signal.SIGINT, main_exit)

# initial setup is done, now starts the real work of the main thread
logger.info(thread_id_string())

# Connect to database
if config.DBURL:
    dblogger = LogConfig.enable_file_logging("database", "database.log", config)
    db = None
    db = checkDB(db, logger, dblogger, filename_parser, config)
    db_last_connection_time = time()

# Setup Kerberos if needed
if config.Keytab:
    krb.setCache(config.KrbCache, logger)
    krb_exp = krb.updateToken(config.Keytab, config.KrbUser, logger)
    if not krb_exp:
        logger.critical('could not initialize kerberos ticket: exiting')
        sys.exit(1)

# Start the threads
manager_thread.start()
if config.ERSEnabled:
    ers_thread.start()
if config.ISEnabled:
    info_service_thread.start()
if config.CopyEnabled:
    copy_thread.start()
if config.DeleteEnabled:
    delete_thread.start()
if config.DdmMonitoringEnabled:
    ddm_publisher.start()

while not exit_event.is_set():

    if config.DBURL:
        logger.debug('checking DB connection')
        db = checkDB(db, logger, dblogger, filename_parser, config)

        # Always keep a fresh connection
        if db and (time() - db_last_connection_time) > config.DBReconnectTimeout:
            logger.info('refreshing database connection')
            with db_lock:
                try:
                    db.Reconnect()
                except Exception as ex:
                    logger.error('error connecting to DB: %s', str(ex))
                    db = None
            db_last_connection_time = time()

    # Update KRB token, if needed:
    if config.Keytab:
        now = datetime.datetime.now()
        if not krb_exp or now >= krb_exp or (krb_exp-now).total_seconds() < 3600:
            krb_exp = krb.updateToken(config.Keytab, config.KrbUser, logger)

    #Check worker states
    mgr_th_ok = manager_thread.is_alive()
    copy_th_ok = (config.CopyEnabled == False) or copy_thread.is_alive()
    del_th_ok = (config.DeleteEnabled == False) or delete_thread.is_alive()
    if not (mgr_th_ok and copy_th_ok and del_th_ok):
        mgr_status = 'running' if mgr_th_ok else 'stopped'
        if config.CopyEnabled:
            copy_status = 'running' if copy_th_ok else 'stopped'
        else:
            copy_status = 'disabled'
        if config.DeleteEnabled:
            del_status = 'running' if del_th_ok else 'stopped'
        else:
            del_status = 'disabled'
        logger.warning('inconsistent worker states; exiting: '
                'manager: %s, copy: %s, delete: %s',
                mgr_status, copy_status, del_status)
        main_exit(None, None)

    exit_event.wait(config.MainThreadEventTimeout)

manager_thread.join()
logger.info('manager thread stopped')
if config.CopyEnabled:
    copy_thread.join()
    logger.info('copy thread stopped')
if config.DeleteEnabled:
    delete_thread.join()
    logger.info('delete thread stopped')
if config.ERSEnabled:
    ers_thread.join()
    logger.info('ers thread stopped')
if config.ISEnabled:
    info_service_thread.join()
    logger.info('IS thread stopped')
if config.DdmMonitoringEnabled:
    ddm_publisher.join()
    logger.info('DdmMonitoring thread stopped')
logger.info('over and out')
