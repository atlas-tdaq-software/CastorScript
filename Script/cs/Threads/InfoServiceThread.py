#!/usr/bin/env tdaq_python
import threading
import psutil
from datetime import datetime
import ispy

from cs.Tools.utils import thread_id_string
from cs.Tools.utils import getmountpoint
from cs.Tools.LogConfig import enable_file_logging, make_tdaq_app_name, make_is_data_identifier

# --------------------------------
# The XML schema associated with CastorScript's IS classes, is part of the SFOng
# package, because the schema needs to be deployed in to the TDAQ release and
# CastorScript is not a TDAQ project:
# https://gitlab.cern.ch/atlas-tdaq-software/SFOng/-/blob/master/schema/CastorScript.IS.schema.xml
# --------------------------------

class InfoServiceThread(threading.Thread):

    def __init__(self, conf, exitevent, copy_thread, delete_thread, manager_thread):
        threading.Thread.__init__(self, name="ISThread")
        self.exitevent = exitevent
        self.conf = conf
        self.time_created = datetime.utcnow()

        self._copy_thread = copy_thread
        self._delete_thread = delete_thread
        self._manager_thread = manager_thread

        # get partition to publish to
        self.ipc_partition = ispy.IPCPartition(conf.ISPartition) #pylint: disable=no-member
        self.partition_connected = True

        self.tdaq_app_name = make_tdaq_app_name(conf)

        self.cpustats_is_id = f'{self.conf.ISServer}.{self.tdaq_app_name}.cpustats'
        self.update_cpu_stats_first = True

        self.iostatscache = {}

        self.logger = enable_file_logging("isthread", "isthread.log", conf)


    def run(self):
        self.logger.info(thread_id_string())

        if self.ipc_partition.isValid():
            self.logger.info('partition %s is valid: IS publication enabled',
                    self.ipc_partition.name())

        # variable to handle the period between the partition becomes valid
        # and the first publication attempt
        # None means that there is nothing to do
        # A valid time means that publication to IS must be postponed to at least
        # partition_valid_at + wait_after_partition_valid
        partition_valid_at = None

        while not self.exitevent.is_set():
            if not self.partition_connected and self.ipc_partition.isValid():
                self.partition_connected = True
                # warning instead of info when publication is restored to be
                # able to keep track of periods where publication was disabled
                self.logger.warning('partition %s is now valid: IS publications enabled'
                        ' (in %d seconds)', self.ipc_partition.name(),
                        self.conf.ISWaitAfterParitionValid)
                partition_valid_at = datetime.utcnow()
            elif self.partition_connected and not self.ipc_partition.isValid():
                self.partition_connected = False
                self.logger.warning('partition %s is now invalid: IS publications disabled',
                        self.ipc_partition.name())
                # if partition was "invalidated" during a wait period this must be reset
                partition_valid_at = None

            if self.ipc_partition.isValid():
                # still need to check if we are in the period after partition became valid
                if partition_valid_at is not None:
                    if (datetime.utcnow() - partition_valid_at).total_seconds() > self.conf.ISWaitAfterParitionValid:
                        # we waited long enough
                        partition_valid_at = None

                if partition_valid_at is None:
                    self.send_update()

            self.exitevent.wait(self.conf.ISPublicationPeriod)

        # to reset the uptime just before normal exit
        self.logger.info('InfoServiceThread stopping')
        self.update_cs_info()


    def send_update(self):
        self.update_cs_info()
        if self.conf.ISFileSystemInfoEnabled:
            self.update_fs_info()
        if self.conf.ISCpuStatsEnabled:
            self.update_cpu_stats()
        if self.conf.ISStorageIoStatsDevices:
            self.update_storage_io_stats()


    def update_cs_info(self):
        is_data_type = "CastorScriptState"
        is_data_identifier = make_is_data_identifier(self.conf.ISServer,
                self.tdaq_app_name)

        try:
            is_data = ispy.ISObject(self.ipc_partition, is_data_identifier, is_data_type)

            is_data.uptime_seconds = self.get_process_uptime_in_seconds()

            is_data.files_to_be_copied = 0
            is_data.files_to_be_deleted = 0
            is_data.files_copied = 0
            is_data.simple_transfer_failures = 0
            is_data.problematic_transfer_failures = 0
            is_data.files_being_copied = 0
            if self._copy_thread:
                is_data.files_to_be_copied = self._copy_thread.copyqueue.qsize()
                is_data.files_to_be_deleted = self._copy_thread.deletequeue.qsize()
                is_data.files_copied = self._copy_thread.files_copied
                is_data.simple_transfer_failures = self._copy_thread.simple_transfer_failures
                is_data.problematic_transfer_failures = self._copy_thread.problematic_transfer_failures
                is_data.files_being_copied = self._copy_thread.nbtransfers

            is_data.files_deleted = 0
            if self._delete_thread:
                is_data.files_deleted = self._delete_thread.files_deleted

            is_data.files_currently_problematic = self._manager_thread.getNbCurrentlyProblematic()
            is_data.checkin()
            self.logger.debug("CastorScriptState sent: %s", str(is_data))
        except Exception as ex:
            self.logger.error("Error occured in update_cs_info(): %s", str(ex))



    def get_process_uptime_in_seconds(self):
        if self.exitevent.is_set():
            # to reset the uptime just before normal exit
            return 0
        else:
            return (datetime.utcnow() - self.time_created).total_seconds()


    def update_fs_info(self):
        for srcdirs_item in self.conf.SrcDirs:
            mountpoint = getmountpoint(srcdirs_item)
            try:
                du = psutil.disk_usage(mountpoint)
            except Exception as e:
                self.logger.error('error getting file system usage: %s', e)
                continue

            fsinfo_id = "{}.{}.fsinfo-{}".format(self.conf.ISServer,
                    self.tdaq_app_name, mountpoint)

            try:
                is_data = ispy.ISObject(self.ipc_partition, fsinfo_id,
                        "FileSystemInfo")

                is_data.total_MB = du.total // 1000000
                is_data.available_MB = du.free // 1000000
                is_data.occupancy_percent = du.percent

                is_data.checkin()
                self.logger.debug("fsinfo published: %s", str(is_data))
            except Exception as ex:
                self.logger.error("error publishing fsinfo: %s", str(ex))


    def update_cpu_stats(self):
        # get utilisation fraction since last call
        c = psutil.cpu_times_percent()

        if self.update_cpu_stats_first:
            # documentation says first value is to be ignored
            self.update_cpu_stats_first = False
            return

        try:
            is_data = ispy.ISObject(self.ipc_partition,
                    self.cpustats_is_id, "CpuStats")

            is_data.user = c.user
            is_data.nice = c.nice
            is_data.system = c.system
            is_data.idle = c.idle
            is_data.iowait = c.iowait
            is_data.hwirq = c.irq
            is_data.swirq = c.softirq

            is_data.checkin()
            self.logger.debug("cpustats published: %s", str(is_data))
        except Exception as ex:
            self.logger.error("error publishing cpustats: %s", str(ex))

    def update_storage_io_stats(self):
        now = datetime.now()

        if not self.iostatscache:
            # cache initialization
            d = psutil.disk_io_counters(perdisk=True, nowrap=True)

            for sd in self.conf.ISStorageIoStatsDevices:
                if sd not in d:
                    self.logger.warning('no stats for device %s', sd)
                    continue
                self.iostatscache[sd] = StorageDeviceIoStats(sd)
                stats = d[sd]
                self.iostatscache[sd].update(now, stats.read_count,
                        stats.write_count, stats.read_bytes, stats.write_bytes,
                        stats.busy_time)
            return

        d = psutil.disk_io_counters(perdisk=True, nowrap=True)
        for sd in d:
            if not sd in self.conf.ISStorageIoStatsDevices:
                continue
            stats = d[sd]
            (rr, wr, rt, wt, up) = self.iostatscache[sd].update(now,
                    stats.read_count, stats.write_count, stats.read_bytes,
                    stats.write_bytes, stats.busy_time)

            obj_id = "{}.{}.iostats-{}".format(self.conf.ISServer,
                    self.tdaq_app_name, sd)

            try:
                is_data = ispy.ISObject(self.ipc_partition, obj_id, "StorageIoStats")

                is_data.read_req_rate = rr
                is_data.write_req_rate = wr
                is_data.read_tput = rt
                is_data.write_tput = wt
                is_data.util = up

                is_data.checkin()
                self.logger.debug("cpustats published: %s", str(is_data))
            except Exception as ex:
                self.logger.error("error publishing cpustats: %s", str(ex))


class StorageDeviceIoStats:
    def __init__(self, name):
        self.name = name
        self.rd_rq = None
        self.wr_rq = None
        self.rd_by = None
        self.wr_by = None
        self.busy_time_ms = None
        self.ts = None

    def update(self, ts, rd_rq, wr_rq, rd_by, wr_by, busy_time_ms):
        if self.ts is None:
            rrr = wrr = rtput = wtput = utilp = None
        else:
            dur = ts.timestamp() - self.ts
            rrr = int((rd_rq - self.rd_rq) / dur)
            wrr = int((wr_rq - self.wr_rq) / dur)
            rtput = int((rd_by - self.rd_by) / dur)
            wtput = int((wr_by - self.wr_by) / dur)
            utilp = round((busy_time_ms - self.busy_time_ms) / (dur * 10), 2)


        self.rd_rq = rd_rq
        self.wr_rq = wr_rq
        self.rd_by = rd_by
        self.wr_by = wr_by
        self.busy_time_ms = busy_time_ms
        self.ts = ts.timestamp()

        return (rrr, wrr, rtput, wtput, utilp)