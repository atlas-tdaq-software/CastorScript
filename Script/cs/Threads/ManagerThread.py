#!/bin/env python
import threading, os
from time import time
import datetime
import pprint
import math
import cs.Tools.Constants as Constants
import re
import queue
from cs.Tools.utils import thread_id_string
from cs.Tools.utils import dir_is_locked
from cs.Tools.LogConfig import enable_file_logging
from functools import cmp_to_key
import pathlib
import cs.Threads.FileMetaData as FileMetaData

class ManagerThread(threading.Thread):

    def __init__(self, conf, exitevent, dblock, copyqueue, deletequeue, clearqueue):

        threading.Thread.__init__(self, name="ManagerThread")

        self.parser = None
        self.conf = conf
        self.exitevent = exitevent
        self.copyqueue = copyqueue
        self.deletequeue = deletequeue
        self.clearqueue = clearqueue

        self.db = None
        self.dblock = dblock
        self.dbflag = True

        self.managedfiles = [] # all files currently handled by CastorScript
        self.problematic = {} # files declared problematic with extra info to manage retries
        self.problematic_lock = threading.Lock()
        self.year = ''
        self.month = ''

        # get logger to files, (Will also log to ERS)
        self.logger = enable_file_logging("managerthread", "manager.log", conf)


    def setDB(self, db):
        self.logger.debug('setting database')
        self.db = db


    def getDBFlag(self):
        flag = self.dbflag
        self.logger.debug('getDBFlag: %s', flag)
        self.dbflag = True
        return flag


    def run(self):
        self.logger.info(thread_id_string())

        try:
            self.initialize()
            while not self.exitevent.is_set():
                self.loopIteration()
                self.exitevent.wait(self.conf.ManagerThreadEventTimeout)
        except IOError as e:
            self.logger.critical('I/O error on file %s: %s; '
                    'stopping ManagerThread', e.filename, e.strerror)

        self.logger.info('ManagerThread stopping')


    def initialize(self):
        # Update current year and month cache
        self.updateYearMonthCache()

        # If deletion is enabled, handle COPIED files first
        if self.conf.DeleteEnabled:
            self.logger.debug('handling existing .COPIED files')
            files_to_delete = []

            for folder in self.conf.SrcDirs:
                pattern = '**/*'+Constants.copied_ext if self.conf.RecursiveSourceFolder \
                        else '*'+Constants.copied_ext
                for oldfile in pathlib.Path(folder).glob(pattern):
                    if oldfile.is_dir(): continue
                    datafile = os.path.splitext(oldfile)[0]
                    fmd = self.getFileMetaData(datafile, folder)
                    if fmd:
                        files_to_delete.append(fmd)
                    else:
                        # file was deleted, nothing we can do
                        ManagerThread.cleanHelperFiles(datafile)

            # sort list to delete oldest files first
            files_to_delete.sort(key=cmp_to_key(FileMetaData.fileOrder))
            for fmd in files_to_delete:
                self.logger.debug('adding to deletequeue: %s', fmd.file_name)
                self.managedfiles.append(fmd.file_name)
                self.deletequeue.put(fmd)


    def loopIteration(self):
        self.updateYearMonthCache()
        self.searchFilesToTransfer()
        self.clearManagedFiles()


    def clearManagedFiles(self):
        self.logger.debug('queue size: %d', self.clearqueue.qsize())

        clear_counter = 0
        while clear_counter < self.conf.ManagerThreadClearedFilesPeriod:
            try:
                fmd = self.clearqueue.get_nowait()
            except queue.Empty:
                break
            self.logger.debug('got file from queue: %s', fmd.file_name)

            if self.exitevent.is_set(): break

            self.managedfiles.remove(fmd.file_name)
            if not os.path.isfile(fmd.file_name + Constants.problematic_ext):
                with self.problematic_lock:
                    self.problematic.pop(fmd.file_name, None)

            self.logger.debug('removed from managed files: %s', fmd.file_name)
            clear_counter += 1

        if clear_counter != 0:
            self.logger.info('removed entries from managed files: %d', clear_counter)


    def searchFilesToTransfer(self):
        self.logger.debug('searching for files to transfer')
        newfiles = self.getOrderedFilesToTransfer()
        if len(newfiles) != 0:
            self.logger.info('found %d new files to handle', len(newfiles))
        for fmd in newfiles:
            if self.exitevent.is_set(): return
            self.logger.info('new file to handle: %s', fmd.file_name)
            self.managedfiles.append(fmd.file_name)
            if self.conf.CopyEnabled:
                self.logger.debug('adding to copyqueue: %s', fmd.file_name)
                self.copyqueue.put(fmd)
            elif self.conf.DeleteEnabled:
                self.logger.debug('adding to deletequeue: %s', fmd.file_name)
                # do as if the file was copied
                os.rename(fmd.file_name + Constants.tobecopied_ext,
                            fmd.file_name + Constants.copied_ext)
                self.deletequeue.put(fmd)
        # else: no copy and no delete is not possible


    def getOrderedFilesToTransfer(self):
        new_files = []

        for folder in self.conf.SrcDirs:
            # Consider only unlocked filesystems
            if dir_is_locked(folder, self.conf.SrcDirs, self.conf.LockFilePattern):
                self.logger.debug('directory is locked, skipping files search: %s', folder)
                continue

            files = []
            for pattern in self.conf.DataFilePattern:
                if self.conf.RecursiveSourceFolder:
                    pattern = '**/' + pattern
                newfiles = [str(f) for f in pathlib.Path(folder).glob(pattern) if not f.is_dir()]
                self.logger.debug('%d files found in folder %s matching %s',
                        len(newfiles), folder, pprint.pformat(pattern))
                files += newfiles

            for filename in files:
                # Protection against bad pattern definition in the previous glob
                if os.path.splitext(filename)[1] in (
                        Constants.tobecopied_ext, Constants.copied_ext,
                        Constants.copying_ext, Constants.problematic_ext):
                    continue

                if self.conf.SkipSymlinkFiles and os.path.islink(filename):
                    self.logger.debug('skipping symlink file: %s', filename)
                    continue

                if filename in self.managedfiles:
                    self.logger.debug('file already managed: %s', filename)
                    continue

                self.logger.debug('new file: %s', filename)

                if self.conf.ExcludeFileRegex is not None and re.match(self.conf.ExcludeFileRegex, filename):
                    self.logger.debug('ignoring file matching exclude regex: %s', filename)
                    continue

                if os.path.isfile(filename + Constants.copied_ext):
                    # The file does not exist in managedfiles but it has an
                    # associated .COPIED file: it happens when deletion is
                    # disabled or DeleteExcludeFileRegex excludes this files; in
                    # this case we don't want to transfer the file over and over
                    # again.
                    self.logger.debug('ignoring file already transferred: %s', filename)
                    continue

                # Check for minimal file size, if needed
                if self.conf.MinSizekB:
                    try:
                        filesize = os.path.getsize(filename)
                    except FileNotFoundError as exc:
                        self.logger.warning(exc)
                        continue

                    if (filesize / 1024.) < self.conf.MinSizekB:
                        self.logger.debug('skipping too small file: %s', filename)
                        if self.conf.RemoveSmallFiles:
                            self.logger.debug('removing too small file: %s', filename)
                            try:
                                os.remove(filename)
                            except OSError as ex:
                                self.logger.warning('error deleting %s: %s', filename, ex.__repr__())
                        continue

                # Check if .PROBLEMATIC file is ready to be copied
                if os.path.isfile(filename + Constants.problematic_ext):
                    time_to_retry = False
                    try:
                        time_to_retry = self.checkProbl(filename)
                    except FileNotFoundError as exc:
                        # file was deleted, nothing we can do
                        self.logger.warning(exc)
                        ManagerThread.cleanHelperFiles(filename)
                    if not time_to_retry:
                        continue

                # If there is a connection to a database, check first that the file
                # already exists in DB. This is to avoid the race condition between
                # the file creator (e.g. SFOng) and the CastorScript instance: the
                # file may exist in the folder but the request to create the database
                # entry still in a queue/thread. If it were to happen, the file could
                # be transferred and the CastorScript could try to update its status
                # while it still hasn't been created in the database.
                if self.conf.DBURL:
                    if self.db:
                        with self.dblock:
                            status, exists = self.db.FileExists(filename)
                            if not status:
                                self.logger.warning('error accessing the database: %s', filename)
                                self.dbflag = False
                                continue
                            if not exists:
                                self.logger.warning('skipping file because no entry in database yet: %s', filename)
                                continue
                    else:
                        # DB is configured but not working at the moment
                        self.logger.warning('skipping file because db not working at the moment: %s', filename)
                        continue

                fmd = self.getFileMetaData(filename, folder)
                if fmd:
                    new_files.append(fmd)
                else:
                    # file was deleted, nothing we can do
                    ManagerThread.cleanHelperFiles(filename)

        self.logger.debug('sorting new files: %d', len(new_files))
        new_files.sort(key=cmp_to_key(FileMetaData.fileOrder))
        return new_files


    @staticmethod
    def cleanHelperFiles(filename):
        try: os.remove(filename + Constants.tobecopied_ext)
        except: pass
        try: os.remove(filename + Constants.copying_ext)
        except: pass
        try: os.remove(filename + Constants.copied_ext)
        except: pass
        try: os.remove(filename + Constants.problematic_ext)
        except: pass


    # problematic is a dictionary of {.PROBLEMATIC file : [timestamp, nretry]}
    def checkProbl(self, filename):
        self.logger.debug('problematic file: %s; check if time to retry transferring', filename)
        with self.problematic_lock:
            if filename not in self.problematic:
                try:
                    problematic_since = os.path.getctime(filename + Constants.problematic_ext)
                except FileNotFoundError as exc:
                    self.logger.warning(exc)
                    problematic_since = time()
                self.problematic[filename] = (problematic_since, 0)
                self.logger.debug('retry #1: %s', filename)
                return True
            (timestamp, nretry) = self.problematic.get(filename)

        # Try to copy the file again only if the timeout is expired
        retry_timeout = self.conf.ProblDelay * math.exp(self.conf.ProblScalingFactor * nretry)
        if (time() - timestamp) > retry_timeout:
            self.logger.debug('retry %d: %s', nretry, filename)
            nretry += 1
            with self.problematic_lock:
                self.problematic[filename] = [time(), nretry]

            # Inform the user if we cannot copy the file and the retry time
            # limit has been reached
            if retry_timeout > self.conf.ProblDelayLimit:
                filesize = os.path.getsize(filename)
                self.logger.critical('problematic file %s reached the delay retry limit of %d seconds; '
                        'filesize= %d, transfer_timeout= %d',
                        filename, self.conf.ProblDelayLimit, filesize, self.conf.TransferTimeout)
            return True
        else:
            self.logger.debug('retry later: %s', filename)
            return False


    def getFileMetaData(self, filename, confdir):
        # confdir is the item in configuration SrcDirs that contains filename
        # filename can be in a sub-directory of confdir

        try:
            mtime = FileMetaData.getMTime(filename)
        except FileNotFoundError as exc:
            self.logger.warning(exc)
            return None

        infofile = ''
        if os.path.isfile(filename + Constants.tobecopied_ext): infofile = filename + Constants.tobecopied_ext
        elif os.path.isfile(filename + Constants.copying_ext): infofile = filename + Constants.copying_ext
        elif os.path.isfile(filename + Constants.problematic_ext): infofile = filename + Constants.problematic_ext
        elif os.path.isfile(filename + Constants.copied_ext): infofile = filename + Constants.copied_ext

        # if no file exists, it defaults to TOBECOPIED
        new_info_file_extension = Constants.tobecopied_ext

        if infofile:
            self.logger.debug('file: %s, reading info from existing helper file %s', filename, infofile)
            with open(infofile,'r') as info_file:
                all_lines = info_file.readlines()

            remotedir = ''
            eosinstance = ''

            for line in all_lines:
                if 'CastorDir' in line or 'RemoteDir' in line:
                    # 'CastorDir' is for backward compatibility
                    try:
                        remotedir = line.split()[2]
                    except IndexError:
                        self.logger.critical('cannot parse CastorDir/RemoteDir line in file: %s', infofile)
                elif 'StageHost' in line or 'EOSInstance' in line:
                    # 'StageHost' is for backward compatibility
                    eosinstance = line.split()[2]

            if remotedir == '' or eosinstance == '':
                # empty file, invalid data, etc.
                self.logger.warning(
                        'invalid metadata in %s (remotedir="%s", eosinstance="%s")'
                        ': deleting infofile and getting metadata from scratch'
                        , infofile, remotedir, eosinstance)
                os.remove(infofile)
                # we need to recreate the same file
                new_info_file_extension = os.path.splitext(infofile)[1]
            else:
                self.logger.debug('filemetadata: remote dir = %s, eos instance = %s',
                        remotedir, eosinstance)

                if not infofile.endswith(Constants.copied_ext):
                    os.rename(infofile, filename + Constants.tobecopied_ext)

                return FileMetaData.FileMetaData(filename, remotedir, eosinstance, mtime)

        try:
            parsed = self.parser(filename)
        except:
            # log to be able to investigate
            self.logger.error('error parsing filename: %s', filename)
            # backward compatibility: re-raise
            raise

        subdir = ''
        if self.conf.ReplicateTreeStructure and os.path.dirname(filename) != os.path.normpath(confdir):
            subdir = os.path.relpath(os.path.dirname(filename), os.path.normpath(confdir))

        #Prepare dictionary for directory definition
        dirdict = {'year':self.year,
                   'month':self.month,
                   'streamtype':parsed.StreamType(),
                   'streamname':parsed.StreamName(),
                   'runnumber':parsed.RunNr(),
                   'filenr':parsed.FileNr(),
                   'lumiblocknr':parsed.LBNr(),
                   'projecttag':parsed.ProjectTag(),
                   'dataset':parsed.Dataset(),
                   'applicationid':parsed.AppId(),
                   'directory':parsed.Directory(),
                   'userdef1':parsed.UserDef1(),
                   'userdef2':parsed.UserDef2(),
                   'userdef3':parsed.UserDef3()}

        self.logger.debug('new file: %s, reading info from configuration', filename)

        # Stream driven case
        def match_pool(drivenPool, parsed):
            return (drivenPool.projecttag.match(parsed.ProjectTag()) and
                    drivenPool.streamtype.match(parsed.StreamType()) and
                    drivenPool.streamname.match(parsed.StreamName()) and
                    drivenPool.lumiblock.match(parsed.LBNr()) and
                    drivenPool.application.match(parsed.AppId()) and
                    drivenPool.directory.match(parsed.Directory()))

        if self.conf.DrivenPool:
            for e in self.conf.DrivenPool:
                if match_pool(e, parsed):
                    remotedir = os.path.normpath(e.targetdir%dirdict)
                    if subdir:
                        remotedir = os.path.join(remotedir, subdir)
                    eosinstance = e.eosinstance
                    self.logger.debug('stream driven, filemetadata: remote dir = %s, eos instance = %s',
                            remotedir, eosinstance)
                    fmd = FileMetaData.FileMetaData(filename, remotedir, eosinstance,
                            mtime)
                    fmd.writeToFile(new_info_file_extension, self.logger)
                    return fmd

        # Normal case
        eosinstance = self.conf.EOSInstance
        remotedir = os.path.normpath(self.conf.CopyDir%dirdict)
        if subdir:
            remotedir = os.path.join(remotedir, subdir)
        self.logger.debug('normal case, filemetadata: remote dir = %s, eos instance = %s',
                remotedir, eosinstance)
        fmd = FileMetaData.FileMetaData(filename, remotedir, eosinstance, mtime)
        fmd.writeToFile(new_info_file_extension, self.logger)
        return fmd


    def updateYearMonthCache(self):
        self.year = datetime.date.today().year
        self.month = datetime.date.today().month


    def setFilenameParser(self, parser):
        self.parser = parser


    def getNbCurrentlyProblematic(self):
        with self.problematic_lock:
            return len(self.problematic)
