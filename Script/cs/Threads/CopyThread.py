#!/bin/env python
import threading
import os
from time import time
import signal
import queue
import uuid
import cs.Tools.Constants as Constants
from cs.Tools.utils import thread_id_string, adler32, get_bw, dir_is_locked
from cs.Tools.LogConfig import enable_file_logging


class TransferInfo:
    def __init__(self, process, file_meta_data, retry, start_time, local_file_size,
            checksum, db_file_size):
        self.process = process
        self.file_info = file_meta_data
        self.retry = retry
        self.start_time = start_time
        self.local_file_size = local_file_size
        self.checksum = checksum
        self.db_file_size = db_file_size


class CopyThread(threading.Thread):

    def __init__(self, conf, exitevent, dblock, copyqueue, deletequeue, clearqueue,
            ddmmonitoringqueue):

        threading.Thread.__init__(self,name="CopyThread")

        self.conf = conf
        self.exitevent = exitevent
        self.dblock = dblock
        self.copyqueue = copyqueue
        self.deletequeue = deletequeue
        self.clearqueue = clearqueue
        self.ddmmonitoringqueue = ddmmonitoringqueue

        # State variables
        self.simple_transfer_failures = 0
        self.problematic_transfer_failures = 0
        self.files_copied = 0

        self.transfers = []
        self.nbtransfers = 0 # no lock needed for multithread access in python
        self.db = None
        self.dbflag = True
        #timestamp indicating since when the queue is full
        self.ddmqueuefullsince = None
        self.bwcache = None
        self.bwcache_validity = 5 # update cache every N requests

        self.filenameparser = None

        # get logger to files, (Will also log to ERS)
        self.logger = enable_file_logging("copythread", "copy.log", conf)


    def setFilenameParser(self, parser):
        self.filenameparser = parser


    def run(self):
        self.logger.info(thread_id_string())

        while not self.exitevent.is_set():
            start_t = time()
            self.checkTransfers()
            self.startTransfers()

            work_time_s = (time() - start_t)
            # Make sure the sleep time is positive
            sleep_time = max(self.conf.CopyThreadEventTimeout - work_time_s, 0)

            if sleep_time == 0:
                self.logger.debug("Abnormal CopyThread cycle: "\
                                  "duration %s s > timeout %s s",
                                  work_time_s,
                                  self.conf.CopyThreadEventTimeout)

            self.exitevent.wait(sleep_time)

        self.logger.info('CopyThread stopping')
        while self.nbtransfers != 0:
            self.checkTransfers()


    def getCurrentBW(self):
        self.bwcache_validity -= 1
        if self.bwcache_validity == 0 or self.bwcache is None:
            self.bwcache = get_bw(self.conf.BWDevice)
            self.bwcache_validity = 5
        return self.bwcache


    def startTransfers(self):
        self.logger.debug('queue size: %d', self.copyqueue.qsize())

        while True:
            if self.nbtransfers >= self.conf.MaxConcurrentCopy:
                self.logger.debug('max nb transfer reached: %d', self.nbtransfers)
                return
            if self.conf.BWDevice is not None:
                current_bw = self.getCurrentBW()
                if current_bw >= self.conf.BWLimit:
                    self.logger.debug('max bw reached: %s', str(current_bw))
                    return
            if self.exitevent.is_set():
                return

            try:
                fmd = self.copyqueue.get_nowait() # fmd: file meta data
            except queue.Empty:
                return
            self.logger.debug('got file from queue: %s', fmd)

            # Check if filesystem was locked while the file was in the copyqueue
            if dir_is_locked(os.path.dirname(fmd.file_name), self.conf.SrcDirs,
                    self.conf.LockFilePattern):
                # Put the locked files into the clearqueue
                # The Manager will take it when the directory will be unlocked
                self.logger.debug('file system is locked, send back to manager: %s',
                        fmd.file_name)
                self.clearqueue.put(fmd)
                continue

            remote_file = os.path.join(fmd.remote_dir, os.path.basename(fmd.file_name))
            self.logger.debug('create remote directory: %s', fmd.remote_dir)
            self.conf.backend.mkdir(fmd.remote_dir, fmd.eos_instance, self.logger)

            # consistency checks
            checksum = None
            dbfilesize = None

            try:
                local_filesize = os.stat(fmd.file_name).st_size
            except FileNotFoundError as exc:
                self.logger.warning(exc)
                # delete associated file (ignoring errors)
                try: os.remove(fmd.file_name + Constants.tobecopied_ext)
                except: pass
                self.clearqueue.put(fmd)
                continue

            if self.db:
                with self.dblock:
                    dbchecksum, dbfilesize, dbfilehealth = self.db.FileInfo(fmd.file_name)

                if dbfilehealth == 'TRUNCATED':
                    # Fill the database with local info
                    checksum = adler32(fmd.file_name)
                    dbfilesize = local_filesize
                    with self.dblock:
                        self.db.UpdateTruncated(fmd.file_name, dbfilesize, checksum)
                else:
                    checksum = dbchecksum

                if dbfilesize != local_filesize:
                    self.logger.critical('file invalid for copy: %s, '
                            'dbsize=%d localsize=%d'
                            , fmd.file_name, dbfilesize, local_filesize)
                    os.rename(fmd.file_name + Constants.tobecopied_ext,
                              fmd.file_name + Constants.problematic_ext)
                    self.problematic_transfer_failures += 1
                    self.clearqueue.put(fmd)
                    continue
            elif self.conf.DBURL:
                self.logger.warning('no connection to database, cannot get/set '
                        'data for: %s', fmd.file_name)
            elif self.conf.ComputeChecksumFromLocalFile:
                checksum = adler32(fmd.file_name)

            try:
                copyproc = self.conf.backend.backgroundcopy(fmd.file_name,
                        remote_file, fmd.eos_instance, self.logger)
            except:
                self.logger.error('error starting copy: %s', fmd.file_name)
                raise

            try:
                os.rename(fmd.file_name + Constants.tobecopied_ext,
                          fmd.file_name + Constants.copying_ext)
            except FileNotFoundError as exc:
                self.logger.warning(exc)
                # something external to CastorScript deleted it
                # it is actually quite easy to recreate it
                fmd.writeToFile(Constants.copying_ext, self.logger)
            except OSError as exc:
                if exc.errno == 28:
                    # no space left on device: generate critical to inform user
                    self.logger.critical('%s', str(exc))

            self.transfers.append(TransferInfo(copyproc, fmd, 0, time(),
                    local_filesize, checksum, dbfilesize))
            self.nbtransfers += 1
            self.logger.debug('started transfer: %s', fmd.file_name)


    def checkTransfers(self):
        toremove = []
        for transfer in self.transfers:
            status = self.conf.backend.copystate(transfer.process[0])

            if status == None:
                self.handleOngoingTransferProcess(transfer, toremove)
            elif status == 0:
                self.handleTransferProcessSucceeded(transfer, toremove)
            else:
                msg = 'transfer process failed, exit code: %s; process output:\n%s' % (
                        str(status), self.conf.backend.copystdout(transfer.process[0]))
                self.handleTransferFailure(transfer, msg, 'PROCESS', toremove)

        for t in toremove:
            self.transfers.remove(t)
        self.nbtransfers = len(self.transfers)


    def handleOngoingTransferProcess(self, transfer, toremove):
        filename = transfer.file_info.file_name
        self.logger.debug('transfer still ongoing: %s', filename)

        if self.conf.TransferTimeout and \
                (time() - transfer.start_time) > self.conf.TransferTimeout:
            os.kill(transfer.process[1], signal.SIGKILL)
            msg = 'transfer timeout exceeded, kill and retry'
            self.handleTransferFailure(transfer, msg, 'TIMEOUT', toremove)
            return

        # If FS got locked, stop the copy
        if self.conf.StopOngoingTransferAfterLock and \
                dir_is_locked(os.path.dirname(filename), self.conf.SrcDirs,
                        self.conf.LockFilePattern):
            self.logger.debug('file system locked during transfer, '
                    'stop and postpone: %s', filename)
            os.kill(transfer.process[1], signal.SIGKILL)
            remote_file = os.path.join(transfer.file_info.remote_dir,
                    os.path.basename(filename))
            self.conf.backend.remove(remote_file, transfer.file_info.eos_instance,
                    self.logger)
            os.rename(filename + Constants.copying_ext,
                    filename + Constants.tobecopied_ext)
            toremove.append(transfer)
            self.simple_transfer_failures += 1
            self.clearqueue.put(transfer.file_info)


    def handleTransferProcessSucceeded(self, transfer, toremove):
        filename = transfer.file_info.file_name
        self.logger.debug('transfer process succeeded: %s', filename)

        remote_file = os.path.join(transfer.file_info.remote_dir,
            os.path.basename(filename))
        local_filesize = transfer.local_file_size
        remote_filesize, remote_checksum = self.conf.backend.sizechecksum(
                remote_file, transfer.file_info.eos_instance, self.logger)

        if remote_filesize is None or remote_checksum is None:
            failure_reason = 'backend sizechecksum error, assuming copy failure'
            self.handleTransferFailure(transfer, failure_reason,
                    'FILEINTEGRITY', toremove)
            return

        if local_filesize != remote_filesize:
            failure_reason = 'local and remote file sizes differ: '\
                    f'{local_filesize}, {remote_filesize}'
            self.handleTransferFailure(transfer, failure_reason,
                    'FILEINTEGRITY', toremove)
            return

        if transfer.db_file_size is not None:
            if remote_filesize != transfer.db_file_size:
                failure_reason = 'remote and db file sizes differ: '\
                        f'{remote_filesize}, {transfer.db_file_size}'
                self.handleTransferFailure(transfer, failure_reason,
                        'FILEINTEGRITY', toremove)
                return

        if transfer.checksum is not None:
            if transfer.checksum.lower() != remote_checksum.lower():
                failure_reason = 'checksums differ: '\
                        f'{transfer.checksum.lower()}, {remote_checksum.lower()}'
                self.handleTransferFailure(transfer, failure_reason,
                        'FILEINTEGRITY', toremove)
                return

        # If we reached this point, the transfer was successfull
        self.logger.info('transfer successful: %s', filename)

        if self.db:
            self.logger.debug('update database, transfer of %s', filename)
            with self.dblock:
                self.dbflag = self.db.Transfer(filename, remote_file)
        elif self.conf.DBURL:
            self.logger.critical('no connection to database, '
                    'database will not be updated for transfer of %s', filename)

        try:
            os.rename(filename + Constants.copying_ext,
                    filename + Constants.copied_ext)
        except FileNotFoundError as exc:
            self.logger.warning(exc)
            # something external to CastorScript deleted it
            # it is actually quite easy to recreate it
            transfer.file_info.writeToFile(Constants.copied_ext, self.logger)

        if self.conf.DdmMonitoringEnabled:
            self.publishToDdmMonitoring(
                    os.path.basename(filename), # filename
                    local_filesize,  # filesize
                    int(transfer.start_time * 1000000),  # timestamp in us for start of copy
                    int(time() * 1000000),  # timestamp in us for end of copy
                    'DONE',  # copy status
                    )

        if self.conf.DeleteEnabled:
            self.deletequeue.put(transfer.file_info)
        else:
            self.skipDelete(transfer.file_info)

        toremove.append(transfer)
        self.files_copied += 1


    def handleTransferFailure(self, transfer, reason, short_reason, toremove):
        """
        transfer -- Transfer object
        reason -- warning message for local logger
        short_reason -- string describing the error, used for publication to DDM
                  monitoring. Keep it short and explicit.
        toremove -- list of Transfer objects to remove from self.transfers
        """
        filename = transfer.file_info.file_name
        self.logger.warning('transfer failure: %s; reason: %s; retries: %d/%d',
                filename, reason, transfer.retry, self.conf.MaxCopyImmediateRetry)

        # early notification of humans for known issues that require their intervention
        # critical log usually means email (good for getting human attention)
        if 'disk quota exceeded' in reason.lower():
            self.logger.critical('no space left in remote directory: %s',
                    transfer.file_info.remote_dir)

        if transfer.retry < self.conf.MaxCopyImmediateRetry:
            self.simple_transfer_failures += 1

            remote_file = os.path.join(transfer.file_info.remote_dir,
                    os.path.basename(filename))
            self.conf.backend.remove(remote_file,
                    transfer.file_info.eos_instance, self.logger)

            if self.conf.DdmMonitoringEnabled:
                self.publishToDdmMonitoring(
                        os.path.basename(filename),  # filename
                        transfer.local_file_size,  # filesize
                        int(transfer.start_time * 1000000),  # timestamp in us for start of copy
                        int(time() * 1000000),  # timestamp in us for end of copy
                        short_reason,  # copy status
                        )

            if self.exitevent.is_set():
                toremove.append(transfer)
                return

            self.logger.debug('immediate transfer retry: %s', filename)
            copyproc = self.conf.backend.backgroundcopy(filename, remote_file,
                    transfer.file_info.eos_instance, self.logger)
            transfer.process = copyproc
            transfer.retry += 1
            transfer.start_time = time()

        else:
            self.logger.info('immediate retries exceeded, problematic: %s', filename)
            try:
                os.rename(filename + Constants.copying_ext,
                        filename + Constants.problematic_ext)
                self.problematic_transfer_failures += 1
            except FileNotFoundError as exc:
                self.logger.warning(exc)
                # something external to CastorScript deleted it
                # it is actually quite easy to recreate it
                transfer.file_info.writeToFile(Constants.problematic_ext, self.logger)

            if self.conf.DdmMonitoringEnabled:
                self.publishToDdmMonitoring(
                        os.path.basename(filename),  # filename
                        transfer.local_file_size,  # filesize
                        int(transfer.start_time * 1000000),  # timestamp in us for start of copy
                        int(time() * 1000000),  # timestamp in us for end of copy
                        'FAILED_{}'.format(short_reason),  # format agreed with DDM
                        )

            self.clearqueue.put(transfer.file_info)
            toremove.append(transfer)


    def setDB(self, db):
        self.logger.debug('setting database')
        self.db = db


    def getDBFlag(self):
        flag = self.dbflag
        self.logger.debug('getDBFlag: %s', flag)
        self.dbflag = True
        return flag


    def skipDelete(self, file_entry):
        # Send the file back to the ManagerThread where it will be removed
        # from the CopyFileList. The existence of the .COPIED file prevents
        # it from being retransferred.
        self.clearqueue.put(file_entry)


    def publishToDdmMonitoring(self, filename, filesize, copy_start_ts_us,
            copy_end_ts_us, status):

        # for filename data21_comm.00390617.calibration_SCTNoise.daq.RAW._lb0000._SFO-7._0006.data
        # scope = data21_comm
        # dataset = data21_comm.00390617.calibration_SCTNoise.daq.RAW
        # for SFOFileNameParser scope is projecttag and dataset is dataset
        scope = ''
        dataset = ''
        if self.filenameparser:
            try:
                parsed_fn = self.filenameparser(filename)
                scope = parsed_fn.ProjectTag()
                dataset = parsed_fn.Dataset()
            except:
                self.logger.error('error parsing filename: %s', filename)
                raise

        d = {
                'uuid': str(uuid.uuid4()).replace('-', '').lower(),
                'account': '',
                'dataset': dataset,
                'scope': scope,
                'eventType': 'sfo2eos',
                'clientState': status,
                'timeStart': copy_start_ts_us,
                'timeEnd': copy_end_ts_us,
                'filename': filename,
                'filesize': filesize,
                'remoteSite': 'CERN-PROD_TZDISK',
                'localSite': 'CERN-P1_SFO'
        }

        try:
            self.ddmmonitoringqueue.put_nowait(d)
            if self.ddmqueuefullsince is not None:
                self.ddmqueuefullsince = None
                self.logger.warning('ddmmonitoringqueue is no longer full')
        except queue.Full:
            # If the network connection (gateway) or the server cannot cope with
            # the request rate, it's better to discard updates here. Otherwise
            # the memory consumption will increase indefinitely.

            # We had problems recovering after connection loss issue:
            # here the queue remain full forever (18 days). So I changed the
            # logic: we log transition from "not full" to full and from full to
            # "not full". And we log a critical if this situation lasts for more
            # than X minutes
            # The DdmMonitoring logfile will contain more details on the problem

            if self.ddmqueuefullsince is None:
                # first occurrence: log warning
                self.logger.warning('ddmmonitoringqueue is full: '
                        'discarding request for %s', filename)
                self.ddmqueuefullsince = time()
            else:
                self.logger.debug('ddmmonitoringqueue is full: '
                        'discarding request for %s', filename)

                if (time() - self.ddmqueuefullsince) > 300:
                    self.logger.critical('ddmmonitoringqueue is full '
                            'for more than 300 seconds: clearing queue. '
                            'Check DdmMonitoring logs and check connection '
                            'to remote server (with ddm.test.py)')
                    with self.ddmmonitoringqueue.mutex:
                        self.ddmmonitoringqueue.queue.clear()
                    # At the next self.ddmmonitoringqueue.put attempt,
                    # ddmqueuefullsince will be reset so we don't log critical
                    # forever
