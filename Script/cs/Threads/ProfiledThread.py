""" @package ProfiledThread
Simple class to instrument threads with a profiler
"""
import threading
import cProfile

class ProfiledThread(threading.Thread):
    # Overrides threading.Thread.run()
    def run(self):
        profiler = cProfile.Profile()
        res =  profiler.runcall(threading.Thread.run, self)
        profiler.dump_stats('/afs/cern.ch/user/v/vandelli/working/nightly/CastorScript/myprofile-%d.profile' % (self.ident,))
        print('Dump done')
        return res
