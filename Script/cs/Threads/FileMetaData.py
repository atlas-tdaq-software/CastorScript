import os

class FileMetaData:
    def __init__(self, filename, remote_dir, eos_instance, modification_time):
        self.file_name = filename # path + file name
        self.remote_dir = remote_dir
        self.eos_instance = eos_instance
        self.modification_time = modification_time

    def writeToFile(self, extension, logger=None):
        if logger:
            logger.debug('Write metadata to %s%s', self.file_name, extension)
        with open(self.file_name + extension, 'w') as info_file:
            info_file.write(f'FileName = {self.file_name}\n')
            info_file.write(f'RemoteDir = {self.remote_dir}\n')
            info_file.write(f'EOSInstance = {self.eos_instance}\n')

    def isExpress(self):
        return 'express' in self.file_name.lower()

    def __repr__(self):
        return f'({self.file_name}, {self.remote_dir}, {self.eos_instance}, ' \
                f'{self.modification_time})'

    def __eq__(self, rhs):
        return self.file_name == rhs.file_name \
                and self.remote_dir == rhs.remote_dir \
                and self.eos_instance == rhs.eos_instance \
                and self.modification_time == rhs.modification_time

def fileOrder(lhs, rhs):
    '''
    Files are ordered by increasing mtime, and files from express stream
    go first
    '''
    lhs_is_express = lhs.isExpress()
    rhs_is_express = rhs.isExpress()
    if lhs_is_express != rhs_is_express:
        if lhs_is_express: # a is express and not b: a < b
            return -1
        else: # b is express and not a: a > b
            return 1
    else:
        if lhs.modification_time < rhs.modification_time:
            return -1
        elif lhs.modification_time > rhs.modification_time:
            return 1
        else:
            return 0

def getMTime(filename):
    '''
    Helper class to get the file's modification time
    '''
    return os.stat(filename)[8]
