#!/bin/env python
import threading
import os
import importlib
import time
import re
import cs.Tools.Constants as Constants
from cs.Tools.utils import thread_id_string
from cs.Tools.utils import getmountpoint
from cs.Tools.utils import dir_is_locked
from cs.Tools.LogConfig import enable_file_logging


class DeleteThread(threading.Thread):

    def __init__(self, conf, exitevent, dblock, deletequeue, clearqueue):

        threading.Thread.__init__(self, name="DeleteThread")

        self.conf = conf
        self.exitevent = exitevent
        self.dblock = dblock
        self.deletequeue = deletequeue
        self.clearqueue = clearqueue
        self.files_deleted = 0
        self.migration_waitroom = {}
        self.db = None
        self.dbflag = True
        self.deletenow = False
        self.last_fs_usage_check_time = 0
        self.fs_usages = {}

        # get logger to files, (Will also log to ERS)
        self.logger = enable_file_logging("deletethread", "delete.log", conf)

        self.migration_module = None
        if self.conf.MigrationModuleName:
            pymod = importlib.import_module(
                    f"cs.Tools.MigrationCheckers.{self.conf.MigrationModuleName}")
            self.migration_module = pymod.MigrationChecker(
                    self.conf.MigrationModuleConf, self.logger, exitevent)


    def run(self):
        self.logger.info(thread_id_string())

        while not self.exitevent.is_set():
            self.loopIteration()
            self.exitevent.wait(self.conf.DeleteThreadEventTimeout)

        self.logger.info('DeleteThread stopping')
        if self.migration_module:
            self.migration_module.stop()


    def loopIteration(self):
        # copy entries from deletequeue to process each candidate once and then
        # wait a bit (DeleteThreadEventTimeout)

        deletelist = []
        # lock access to ensure that this copy does not go on forever
        with self.deletequeue.mutex:
            try:
                while True:
                    deletelist.append(self.deletequeue.queue.popleft())
            except IndexError:
                pass

        self.logger.debug('queue size: %d', len(deletelist))
        for fmd in deletelist:
            if self.exitevent.is_set(): return
            filename = fmd.file_name
            self.logger.debug('got file from queue: %s', filename)

            # Check exclude regex
            if self.conf.DeleteExcludeFileRegex is not None:
                if re.match(self.conf.DeleteExcludeFileRegex, filename):
                    self.logger.debug('not deleting file matching exclude regex: %s', filename)
                    self.skipDelete(fmd)
                    continue

            # Do not delete file if it's too recent
            if self.conf.DeleteMinFileAge != 0:
                if (time.time() - fmd.modification_time) < self.conf.DeleteMinFileAge:
                    self.logger.debug('file too young to be deleted yet: %s', filename)
                    self.deletequeue.put(fmd)
                    continue

            directory = os.path.dirname(filename)

            # Check directory locking
            if not self.conf.DeleteIgnoreLock:
                if dir_is_locked(directory, self.conf.SrcDirs, self.conf.LockFilePattern):
                    self.logger.debug('file system is locked, do not delete yet: %s', filename)
                    self.deletequeue.put(fmd)
                    continue

            # Check file system usage
            fs_usage = self.getFileSystemUsage(filename)

            if fs_usage >= self.conf.DeleteHighCriticalMark: self.deletenow = True
            if fs_usage <= self.conf.DeleteLowCriticalMark: self.deletenow = False

            if self.deletenow:
                self.logger.warning('critical deletion: %s', filename)
                self.delete(fmd)
                continue

            lowwatermark = self.getLowWaterMark(directory)
            self.logger.debug('usage/lowwatermark for %s: %f/%f', filename, fs_usage, lowwatermark)

            if fs_usage < lowwatermark:
                self.logger.debug('filesystem usage is below threshold, do not delete: %s', filename)
                self.deletequeue.put(fmd)
                continue

            # If migration is not required, this is it
            if not self.conf.MigrationCheck:
                self.delete(fmd)
                continue

            # Migration
            if not self.checkMigrationDelayExpired(filename):
                self.logger.debug('do not check for migration yet: %s', filename)
                self.deletequeue.put(fmd)
                continue

            if self.checkMigration(fmd):
                self.logger.debug('file migrated: %s', filename)
                self.delete(fmd)
            else:
                self.logger.debug('file not yet migrated: %s', filename)
                self.deletequeue.put(fmd)


    def checkMigration(self, fmd):
        self.logger.debug('checking migration: %s', fmd.file_name)
        remotefile = os.path.join(fmd.remote_dir, os.path.basename(fmd.file_name))

        if self.migration_module is None:
            migrated = self.conf.backend.migrated(remotefile, fmd.eos_instance, self.logger)
        else:
            migrated = self.migration_module.isMigrated(remotefile)

        if migrated:
            return True

        # check if this file has been waiting for too long
        if self.conf.MigrationCheckMaxDurationHour and \
                ((time.time() - fmd.modification_time) / 3600) \
                > self.conf.MigrationCheckMaxDurationHour:
            self.logger.log(
                    self.conf.MigrationCheckMaxDurationLogLevel,
                    'old file still waiting for deletion: %s, mtime: %s',
                    fmd.file_name,
                    time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(fmd.modification_time)))

        return False


    def checkMigrationDelayExpired(self, filename):
        if self.conf.MigrationCheckDelay == 0:
            return True
        transfer_completed_time = 0
        if filename not in self.migration_waitroom:
            try:
                transfer_completed_time = os.path.getctime(filename + Constants.copied_ext)
            except FileNotFoundError as exc:
                self.logger.warning(exc)
                transfer_completed_time = time.time()
            self.migration_waitroom[filename] = transfer_completed_time
        else:
            transfer_completed_time = self.migration_waitroom.get(filename)
        if (time.time() - transfer_completed_time) > self.conf.MigrationCheckDelay:
            return True
        return False


    def setDB(self,db):
        self.logger.debug('setting database')
        self.db = db


    def getDBFlag(self):
        flag = self.dbflag
        self.logger.debug('getDBFlag: %s', flag)
        self.dbflag = True
        return flag


    def delete(self, fmd):
        '''
        fmd: FileMetaData object
        '''
        self.logger.info('deleting file %s', fmd.file_name)

        try:
            os.remove(fmd.file_name)
        except FileNotFoundError as exc:
            self.logger.warning(exc)

        try:
            os.remove(fmd.file_name + Constants.copied_ext)
        except FileNotFoundError as exc:
            self.logger.warning(exc)

        if self.db:
            self.logger.debug('update database: deletion of %s', fmd.file_name)
            with self.dblock:
                self.dbflag = self.db.Deletion(fmd.file_name)
        elif self.conf.DBURL:
            self.logger.warning('no connection to database, database will not be updated for deletion of %s', fmd.file_name)

        #increment files deleted, used by the Information Service Thread
        self.files_deleted += 1

        # Send back to manager thread for final cleaning
        self.clearqueue.put(fmd)

        try:
            del self.migration_waitroom[fmd.file_name]
        except KeyError:
            # There are several cases in which the files are not in the
            # migration waiting room: migration check is disabled, high critical
            # mark was reached, etc.
            pass


    def skipDelete(self, file_entry):
        # Send the file back to the ManagerThread where it will be removed
        # from the CopyFileList. The existence of the .COPIED file prevents
        # it from being retransferred.
        self.clearqueue.put(file_entry)


    def getLowWaterMark(self, directory):
        # Get the entry in LowWaterMark associated with the given directory
        idx = None
        for i, srcdirs_item in enumerate(self.conf.SrcDirs):
            if directory == srcdirs_item or directory.startswith(srcdirs_item):
                idx = i
                break
        if idx is None:
            self.logger.error('cannot find SrcDirs entry for %s, assuming lowwatermark=100', directory)
            lowwatermark = 100.
        else:
            lowwatermark = self.conf.DeleteLowWaterMark[idx]
        return lowwatermark


    def getFileSystemUsage(self, filename):
        now = time.time()
        diff = now - self.last_fs_usage_check_time
        if diff >= self.conf.DeleteFileSystemUsagePeriod:
            self.logger.debug('getting file system usage after %d seconds', diff)
            self.fs_usages = self._getFileSystemUsage()
            self.last_fs_usage_check_time = now
            self.logger.debug('fs usages: %s', str(self.fs_usages))

        try:
            fs_usage = self.fs_usages[getmountpoint(filename)]
        except KeyError:
            self.logger.error('cannot get fs associated with %s, assuming usage=0', filename)
            fs_usage = 0.
        return fs_usage


    def _getFileSystemUsage(self):
        fs_usages = {}
        for srcdirs_item in self.conf.SrcDirs:
            mountpoint = getmountpoint(srcdirs_item)
            if mountpoint in fs_usages: continue

            try:
                stats = os.statvfs(mountpoint)
            except OSError as e:
                self.logger.error('statvfs error %s: %s', e.filename, e.strerror)
                continue

            if stats.f_bsize == 0 or stats.f_blocks == 0:
                self.logger.error('statvfs error on %s: stats=%s', mountpoint, str(stats))
                continue

            fs_usages[mountpoint] = 100. * (1 -
                    float(stats.f_bavail) / float(stats.f_blocks))

        return fs_usages
