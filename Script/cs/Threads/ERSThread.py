import threading
import logging
import os
import ers
from ipc import IPCPartition #pylint: disable=no-name-in-module
from cs.Tools.utils import thread_id_string
from cs.Tools.LogConfig import enable_file_logging, set_log_level, make_tdaq_app_name

# There is a thread dedicated to monitoring the validity (existence)
# of the partition used for ERS loggging, attaching/detaching the
# ERS logger when partition starts/stops.

class ERSThread(threading.Thread):


    def __init__(self, conf, exitevent):
        threading.Thread.__init__(self, name="ERSThread")
        self.exitevent = exitevent

        self.ers_timeout = conf.ERSTimeout
        self.log_level = conf.ERSLogLevel

        self.partition_connected = False
        self.ipc_partition = IPCPartition(conf.ERSPartition)
        self.ers_handler = None

        set_environment(conf)

        self.logger = enable_file_logging("ersthread", "ersthread.log", conf)


    def run(self):
        self.logger.info(thread_id_string())

        if not self.ipc_partition.isValid():
            self.logger.warning('partition %s is not valid: ers logging disabled for now'
                    ', will re-enable when partition starts',
                    self.ipc_partition.name())
        else:
            # seems redundant with the transition to partition_connected in the
            # following while loop but you don't want to log a warning
            # in the common working case
            self.logger.info('partition %s is valid: logging to ERS enabled', self.ipc_partition.name())
            self.ers_handler = ers.LoggingHandler()
            set_log_level(self.log_level, self.ers_handler)
            logging.getLogger().addHandler(self.ers_handler)
            self.partition_connected = True

        while not self.exitevent.is_set():
            if self.ipc_partition.isValid() and not self.partition_connected:
                self.partition_connected = True
                # this is a warning and not an info to be able to keep track of time
                # periods where logging was disabled even if logs level is set to warning
                self.logger.warning('partition %s is now valid: logging to ERS enabled', self.ipc_partition.name())

                # ers handler late instantiation: if it is created while the partition
                # is not valid, there are some error messages that we prefer to avoid
                # so we create it only when the partition is valid
                if self.ers_handler is None:
                    self.ers_handler = ers.LoggingHandler()
                    set_log_level(self.log_level, self.ers_handler)

                logging.getLogger().addHandler(self.ers_handler)

            elif self.partition_connected and not self.ipc_partition.isValid():
                self.partition_connected = False
                self.logger.warning('partition %s is now invalid: logging to ERS disabled', self.ipc_partition.name())

                logging.getLogger().removeHandler(self.ers_handler)

            self.exitevent.wait(self.ers_timeout)

        self.logger.info('ERSThread stopping')


def set_environment(config):
    ##### Set ERS Variables #####
    # notice that this only changes the environment of the sub scope of the cs process
    os.environ["TDAQ_APPLICATION_NAME"] = make_tdaq_app_name(config)

    os.environ["TDAQ_PARTITION"] = config.ERSPartition
    os.environ["TDAQ_ERS_FATAL"] = config.ERSEnvFatal
    os.environ["TDAQ_ERS_ERROR"] = config.ERSEnvError
    os.environ["TDAQ_ERS_WARNING"] = config.ERSEnvWarning
    os.environ["TDAQ_ERS_INFO"] = config.ERSEnvInfo
    os.environ["TDAQ_ERS_DEBUG"] = config.ERSEnvDebug
