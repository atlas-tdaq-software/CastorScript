#!/bin/env python
import threading
import urllib.request, urllib.error, urllib.parse
import json
import queue
import time

from cs.Tools.utils import thread_id_string
from cs.Tools.LogConfig import enable_file_logging

class DdmMonitoringThread(threading.Thread):

    def __init__(self, conf, exitevent, ddm_monitoring_queue):

        threading.Thread.__init__(self, name="DdmMonitoringThread")

        self.conf = conf
        self.exitevent = exitevent
        self.DdmMonitoringQueue = ddm_monitoring_queue
        self.requestErrorSince = None
        self.requestErrorCriticalLogged = False

        # get logger to files, (Will also log to ERS)
        self.logger = enable_file_logging("ddmmonitoringthread", "ddmmonitoring.log", conf)


    def run(self):
        self.logger.info(thread_id_string())

        if self.conf.DdmMonitoringProxy:
            self.logger.info('enabling proxy for DdmMonitoring publication: %s', self.conf.DdmMonitoringProxy)
            proxy = urllib.request.ProxyHandler({'https': self.conf.DdmMonitoringProxy})
            opener = urllib.request.build_opener(proxy)
            urllib.request.install_opener(opener)

        self.logger.info('DDMMonitoring publication endpoint: %s', self.conf.DdmMonitoringEndpoint)

        while True:
            if self.exitevent.is_set(): break

            try:
                data = self.DdmMonitoringQueue.get_nowait()
                self.logger.debug('processing entry for %s', data['filename'])
            except queue.Empty:
                self.exitevent.wait(self.conf.DdmMonitoringTimeout)
                continue

            try:
                req = urllib.request.Request(self.conf.DdmMonitoringEndpoint)
                req.add_header('Content-Type', 'application/json')
                urllib.request.urlopen(req, json.dumps(data).encode('utf-8'), timeout=self.conf.DdmMonitoringTimeout)
                self.logger.info('published entry for %s: %s',
                        data['filename'], data['clientState'])

                if self.requestErrorSince is not None:
                    self.logger.warning('request error stopped: duration= %s',
                            time.time() - self.requestErrorSince)
                    self.requestErrorSince = None
                    self.requestErrorCriticalLogged = False
            except Exception as exc:
                self.logger.warning('could not publish data for %s: %s',
                        data['filename'], str(exc))
                if self.exitevent.is_set():
                    self.logger.warning(
                            'exit requested: will not retry to publish data for %s',
                            data['filename'])
                else:
                    self.logger.debug('will retry to publish data for %s',
                            data['filename'])
                    try:
                        self.DdmMonitoringQueue.put(data, block=True,
                                timeout=self.conf.DdmMonitoringTimeout)
                    except queue.Full:
                        self.logger.warning('could not put entry back in the queue for %s: '
                                'timeout, queue is full', data['filename'])

                if self.requestErrorSince is None:
                    self.requestErrorSince = time.time()
                else:
                    if not self.requestErrorCriticalLogged:
                        if (time.time() - self.requestErrorSince) > 300:
                            self.logger.critical('request error for more than 300 seconds')
                            self.requestErrorCriticalLogged = True

        self.logger.info('DdmMonitoringThread stopping')
