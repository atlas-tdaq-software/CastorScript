# This filename parser does nothing. It is used by the DeploymentTest configs.
# Once there was an issue with module loading, so this is here to check that
# a filename parser can be loaded.

from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

class TestFileNameParser(BaseFileNameParser):

    def __init__(self,filename):
        BaseFileNameParser.__init__(self, filename)

    def RunNr(self):
        return ''

    def LBNr(self):
        return ''

    def FileNr(self):
        return ''

    def StreamType(self):
        return ''

    def StreamName(self):
        return ''

    def FileTag(self):
        return ''

    def AppId(self):
        return ''

    def ProjectTag(self):
        return ''

    def Dataset(self):
        return ''

    def Directory(self):
        return ''

    def UserDef1(self):
        return ''

    def UserDef2(self):
        return ''

    def UserDef3(self):
        return ''
