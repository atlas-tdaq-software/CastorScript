#.data --> Standard file --> Projecttag
#.tgz --> L1CaloRateArchive_yyyymmdd.tgz --> projecttag = l1calo_ratearchive
#                                        --> Userdef1 = yyyy

from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser
from cs.Tools.FilenameParsers.SFOFileNameParser import SFOFileNameParser
from cs.Tools.Constants import needed_parser_symbols
import os.path

class L1CFileNameParser(BaseFileNameParser):

    def __init__(self,filename):

        BaseFileNameParser.__init__(self, filename)

        basefilename = os.path.basename(filename)
        ext = os.path.splitext(basefilename)[1]

        if ext == '.data':
            self.parser = SFOFileNameParser(filename)
            self.project = self.parser.ProjectTag()
            self.year = ''
        elif ext == '.tgz':
            self.parser = BaseFileNameParser(filename)
            self.project = 'l1calo_ratearchive'
            self.year = filename.split('_')[1][:4]
        else:
            self.project = ''
            self.year = ''


        for sym in needed_parser_symbols:
            if not sym in ['ProjectTag', 'UserDef1']:
                setattr(self, sym, getattr(self.parser, sym))


    def ProjectTag(self):
        return self.project

    def UserDef1(self):
        return self.year
