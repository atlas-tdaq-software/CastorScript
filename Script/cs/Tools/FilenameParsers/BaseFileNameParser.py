class BaseFileNameParser(object):

    def __init__(self, filename):
        self.filename = filename

    def RunNr(self):
        return ''

    def LBNr(self):
        return ''

    def FileNr(self):
        return ''

    def StreamType(self):
        return ''

    def StreamName(self):
        return ''

    def FileTag(self):
        return ''

    def AppId(self):
        return ''

    def ProjectTag(self):
        return ''

    def Dataset(self):
        return ''

    def Directory(self):
        return ''

    def UserDef1(self):
        return ''

    def UserDef2(self):
        return ''

    def UserDef3(self):
        return ''
