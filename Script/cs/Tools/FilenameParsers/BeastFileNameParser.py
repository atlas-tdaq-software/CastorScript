#!/usr/bin/env tdaq_python
import os.path
from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

basepath = ""


def init(params):
    global basepath
    basepath = params['basepath']


class BeastFileNameParser(BaseFileNameParser):

    def __init__(self, filename):
        BaseFileNameParser.__init__(self, filename)
        fulldir = os.path.dirname(filename)
        if basepath:
            self.dirname = os.path.relpath(fulldir, basepath)
        else:
            self.dirname = fulldir

    def Directory(self):
        return self.dirname


if __name__=='__main__':
    import sys
    init({'basepath':sys.argv[1]})
    p = BeastFileNameParser(sys.argv[2])
    print(p.Directory())
