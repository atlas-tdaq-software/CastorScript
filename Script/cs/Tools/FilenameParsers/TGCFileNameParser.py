import os.path

from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

#<runnumber>.xxxxxxx.run --> streamtype is "data"
#TGCgnam_2009_08_11.tar  --> streamtype is "histogram"

class TGCFileNameParser(BaseFileNameParser):

    def __init__(self,filename):
        BaseFileNameParser.__init__(self, filename)
        basefilename = os.path.basename(filename)
        self.runnr = ''
        self.streamtype = ''

        if 'TGCgnam' in basefilename:
            self.streamtype = 'histogram'
        else:
            pieces = basefilename.split('.')
            self.runnr = int(pieces[0])
            self.streamtype = 'data'

    def RunNr(self):
        return self.runnr

    def StreamType(self):
        return self.streamtype
