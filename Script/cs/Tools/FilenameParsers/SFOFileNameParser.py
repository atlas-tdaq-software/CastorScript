import os.path

from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

#OLD: daq.NoTag.0087600.debug.hlterror.LB0000.SFO-4._0001.data
#NEW: data_test.00087600.debug_hlterror.daq.RAW._lb0000._SFO-4._0001.data

class SFOFileNameParser(BaseFileNameParser):

    def __init__(self,filename):
        BaseFileNameParser.__init__(self, filename)
        basefilename = os.path.basename(filename)
        pieces = basefilename.split('.')

        if 'LB' in pieces[5]:
            #Old
            self.filenr = str(int(pieces[7][1:]))
            self.appid = pieces[6]
            self.runnr = pieces[2]
            self.lbnr = str(int(pieces[5][2:]))
            self.streamtype = pieces[3]
            self.streamname = pieces[4]
            self.filetag = pieces[1]
            self.projecttag = ''
            self.dataset = ''
        else:
            #New
            self.filenr = str(int(pieces[7][1:]))
            self.appid = pieces[6][1:]
            self.runnr = pieces[1]
            self.lbnr = str(int(pieces[5][3:]))
            self.streamtype = (pieces[2].split('_'))[0]
            self.streamname = (pieces[2].split('_', 1))[1]
            self.filetag = ''
            self.projecttag = pieces[0]
            self.dataset = basefilename.partition('._lb')[0]

    def RunNr(self):
        return self.runnr

    def LBNr(self):
        return self.lbnr

    def FileNr(self):
        return self.filenr

    def StreamType(self):
        return self.streamtype

    def StreamName(self):
        return self.streamname

    def FileTag(self):
        return self.filetag

    def AppId(self):
        return self.appid

    def ProjectTag(self):
        return self.projecttag

    def Dataset(self):
        return self.dataset

    def Directory(self):
        return ''

    def UserDef1(self):
        return ''

    def UserDef2(self):
        return ''

    def UserDef3(self):
        return ''
