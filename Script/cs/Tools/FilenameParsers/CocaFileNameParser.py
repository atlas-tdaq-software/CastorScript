import os.path
from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

#  CDR parser for file names produced by coca. Main idea is that
#  we start writing coca files to subdirectories and repeat the
#  same directory structure in EOS. We split files between
#  subdirectories based on year and dataset name, this should
#  bring down number of files in single directory to a reasonable
#  number (<30k).
#
#  This parser understands following directory structure
#
#  /.../YYYY/<dataset>/name.ext
#      directory:  YYYY/<dataset>
#      userdef1:   YYYY
#      streamname: <dataset>
#
#  /.../YYYYMM/<dataset>/name.ext
#      directory:  YYYYMM/<dataset>
#      userdef1:   YYYYMM
#      streamname: <dataset>
#
#  /.../name.ext
#      directory:  ""
#      userdef1:   ""
#      streamname: ""
#
#  To reduce possibility of false match we check that YYYY is in range 2000-2099
#  and MM if present is between 01 and 12.
#

class CocaFileNameParser(BaseFileNameParser):

    def __init__(self, filename):

        BaseFileNameParser.__init__(self, filename)

        self.dir = ""
        self.udef1 = ""
        self.ds = ""

        # split path into components
        directory, _ = os.path.split(filename)
        directory, dir1 = os.path.split(directory)
        directory, dir2 = os.path.split(directory)

        # if it is YYYY or YYYYMM then we expect YYYY to be in range 2000-2099
        if dir2.isdigit() and dir2.startswith('20') and \
            (len(dir2) == 4 or len(dir2) == 6 and 1 <= int(dir2[-2:]) <= 12):

            self.udef1 = dir2
            self.ds = dir1
            self.dir = os.path.join(self.udef1, self.ds)


            self.udef1 = dir2
            self.ds = dir1
            self.dir = os.path.join(self.udef1, self.ds)

    def StreamName(self):
        return self.ds

    def Directory(self):
        return self.dir

    def UserDef1(self):
        return self.udef1

def _test():

    # list of tuples (filename, run, lb, dataset, directory, udef1, udef2)
    testData = [
        ("/data/coca/cdr/name.zip", "", "", ""),
        ("/data/coca/cdr/2012/CSCGnam/name.zip", "CSCGnam", "2012/CSCGnam", "2012"),
        ("/data/coca/cdr/201208/CSCGnam/name.zip", "CSCGnam", "201208/CSCGnam", "201208"),
        ("201208/CSCGnam/name.zip", "CSCGnam", "201208/CSCGnam", "201208"),
        ("/data/coca/cdr/20120812/CSCGnam/name.zip", "", "", ""),
        ("/data/coca/cdr/201201/CSCGnam/name.zip", "CSCGnam", "201201/CSCGnam", "201201"),
        ("/data/coca/cdr/201212/CSCGnam/name.zip", "CSCGnam", "201212/CSCGnam", "201212"),
        ("/data/coca/cdr/201200/CSCGnam/name.zip", "", "", ""),
        ("/data/coca/cdr/201213/CSCGnam/name.zip", "", "", ""),
    ]

    for rec in testData:

        parser = CocaFileNameParser(rec[0])
        if (parser.StreamName(), parser.Directory(), parser.UserDef1()) != rec[1:]:
            raise RuntimeError(rec)
