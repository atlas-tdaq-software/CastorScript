from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser
import os.path

# source: <basedir>/dira/dirb/dirc/file.ext
# dest: <eosbasedir>/dira/dirb/dirc/file.ext

# config['basedir'] --> base directory path to be stripped off

config = {}

def init(d):
    global config
    config = d


class SCTFileNameParser(BaseFileNameParser):

    def __init__(self,filename):
        BaseFileNameParser.__init__(self,filename)
        directory = os.path.dirname(filename)
        directory = os.path.abspath(directory)

        basedir = config['basedir']

        self.dir = directory.partition(basedir)[-1]

    def Directory(self):
        return self.dir


if __name__ == "__main__":
    import sys

    init({'basedir':sys.argv[2]})
    sct = SCTFileNameParser(sys.argv[1])
    print(sct.Directory())
