__version__ = '$Revision$'

import os.path
from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

# /det/<xx>/path/to/data/to_eos
#
# directory --> /path/to/data


class BCMSimpleFileNameParser(BaseFileNameParser):

    def __init__(self, filename):
        BaseFileNameParser.__init__(self, filename)
        directory = os.path.dirname(filename)
        directory = os.path.abspath(directory)

        # Remove 'to_eos'
        directory, _ = os.path.split(directory)

        # Drop the first two levels
        self.dir = directory.split('/', 3)[-1]

    def Directory(self):
        return self.dir

if __name__ == "__main__":
    import sys

    bcm = BCMSimpleFileNameParser(sys.argv[1])
    print(bcm.Directory())
