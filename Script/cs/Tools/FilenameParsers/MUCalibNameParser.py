from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser
import os.path

class MUCalibNameParser(BaseFileNameParser):

    def __init__(self,filename):
        BaseFileNameParser.__init__(self,filename)
        pieces = os.path.basename(filename).split('.')
        self.projecttag = pieces[0]
        self.filenr = str(int(pieces[5][1:]))
        self.appid = 'MUCalServer'
        self.runnr = pieces[1]
        self.lbnr = '0'
        self.streamtype = pieces[2][0:pieces[2].find('_')]
        self.streamname = pieces[2][pieces[2].find('_')+1:]
        self.dataset = os.path.basename(filename).partition('._')[0]
        self.filetag = ''

    def RunNr(self):
        return self.runnr

    def LBNr(self):
        return self.lbnr

    def FileNr(self):
        return self.filenr

    def ProjectTag(self):
        return self.projecttag

    def StreamType(self):
        return self.streamtype

    def StreamName(self):
        return self.streamname

    def FileTag(self):
        return self.filetag

    def AppId(self):
        return self.appid

    def Dataset(self):
        return self.dataset

    def Directory(self):
        return ''

    def UserDef1(self):
        return ''

    def UserDef2(self):
        return ''

    def UserDef3(self):
        return ''
