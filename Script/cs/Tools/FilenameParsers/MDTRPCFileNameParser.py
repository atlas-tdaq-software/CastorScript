from cs.Tools.FilenameParsers.SFOFileNameParser import SFOFileNameParser
import os.path


class MDTRPCFileNameParser(SFOFileNameParser):

    def __init__(self,filename):
        SFOFileNameParser.__init__(self, filename)
        self.dirname = os.path.split(os.path.dirname(filename))[1]

    def Directory(self):
        return self.dirname
