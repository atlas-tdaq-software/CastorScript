import os.path
from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

#*.lumi --> /remote_base/lumi/BCM
#*.blm  --> /remote_base/lumi/BLM
#*.blmpm --> /remote_base/lumi/BLM/PM
#*.gz -->
#     if dump_data_v4 in path:
#          /remote/base/BCM/DEVPM/<dirname after dump_data_v4>
#     else: /remote_base/BCM/PM



class BCMFileNameParser(BaseFileNameParser):

    def __init__(self,filename):
        BaseFileNameParser.__init__(self,filename)
        basename = os.path.basename(filename)

        ext = os.path.splitext(basename)[1]

        self.type = self.dir = None
        if ext == '.lumi':
            self.dir = 'BCM'
            self.type = 'lumi'
        elif ext == '.blm':
            self.dir = 'BLM'
            self.type = 'lumi'
        elif ext == '.blmpm':
            self.dir = 'BLM/PM'
            self.type = 'lumi'
        elif ext == '.gz':
            if 'dump_data_v4' in filename:
                self.dir = os.path.split(os.path.dirname(filename))[1]
                self.type = 'BCM/DEVPM'
            else:
                self.dir = 'PM'
                self.type = 'BCM'
        else:
            self.dir = 'unknown'


    def Directory(self):
        return self.dir


    def UserDef1(self):
        return self.type

if __name__ == "__main__":
    import sys

    bcm = BCMFileNameParser(sys.argv[1])
    print(bcm.UserDef1(), bcm.Directory())
