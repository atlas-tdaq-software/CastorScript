from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

tobecopied_ext = '.TOBECOPIED'
copied_ext = '.COPIED'
copying_ext = '.COPYING'
problematic_ext = '.PROBLEMATIC'
#human_ext = '.HUMAN_INTERVENTION'
#human_ext_info = '.HUMAN_INTERVENTION.info'

needed_parser_symbols = [basefilenamepaser_element for basefilenamepaser_element
        in dir(BaseFileNameParser) if '__' not in basefilenamepaser_element]
