import zlib
import ctypes
import time
import datetime
import os
import pathlib

def gettid():
    #This is linux specific ...
    SYS_gettid = 186
    libc = ctypes.cdll.LoadLibrary('libc.so.6')
    tid = libc.syscall(SYS_gettid)
    return tid


def thread_id_string():
    import threading
    me = threading.currentThread()
    return '%s TID: %d' % (me.getName(),gettid())


def adler32(filename):
    BS = 1024*1024
    f = open(filename, 'rb')

    ck = 1
    while True:
        data = f.read(BS)
        if not data: break
        ck = zlib.adler32(data,ck)

    return ('%x' % ctypes.c_uint(ck).value).zfill(8)


def get_tx_bytes(device):
    with open('/proc/net/dev', 'r') as f:
        for line in f.readlines():
            if device in line:
                return int(line.split()[9])
    return 0

def get_bw(device, interval=1):
    if not device:
        return -1

    tx_bytes_0 = get_tx_bytes(device)
    t0 = datetime.datetime.now()

    time.sleep(interval)

    tx_bytes_1 = get_tx_bytes(device)
    tdiff = (datetime.datetime.now() - t0).total_seconds()

    if tx_bytes_1 < tx_bytes_0:
        return -1

    return 1. * (tx_bytes_1 - tx_bytes_0) / tdiff / 1000 / 1000

def getmountpoint(path):
    path = os.path.realpath(os.path.abspath(path))
    while path != os.path.sep:
        if os.path.ismount(path):
            return path
        path = os.path.abspath(os.path.join(path, os.pardir))
    return path

def dir_is_locked(directory, dirlist, lockfile_pattern):
    # N.B. lockfile can be a glob pattern
    # directory could be a subdir: look for high-level dir
    if lockfile_pattern is None:
        return False

    lockfile_directory = None
    if directory in dirlist:
        lockfile_directory = directory
    else:
        for updir in dirlist:
            if directory.startswith(updir):
                lockfile_directory = updir

    if lockfile_directory is not None:
        if list(pathlib.Path(lockfile_directory).glob(lockfile_pattern)):
            return True
        else:
            # empty list means no match, i.e. no lock file
            return False
    else:
        raise RuntimeError(f"cannot find SrcDirs directory for {directory}")
