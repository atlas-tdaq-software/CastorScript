#!/bin/env python
from cs.Tools.Libraries.config import Config
from cs.Tools import LogConfig
import importlib
from collections import namedtuple
import re
import os
import logging

class REWrap:
###
# Wrap python re objects such that a None
# pattern results in a positive match
###
    def __init__(self, pattern):
        if pattern is not None:
            self.re = re.compile(pattern)
        else:
            self.re = None

    def match(self, string):
        if self.re:
            return self.re.match(string)
        else:
            return True


class DrivenPool(namedtuple('DrivenPool', ['targetdir','eosinstance',
        'projecttag','streamtype','streamname','lumiblock','application',
        'directory'])):
###
# namedtuple is subclassed to
# a) allow default values (None) for the filtering parameters
# b) crate regex objects from the filtering parameters
###
    def __new__(cls, targetdir, eosinstance,
                projecttag=None, streamtype=None,
                streamname=None, lumiblock=None,
                application=None, directory=None):
        return super(DrivenPool, cls).__new__(cls, targetdir, eosinstance,
                                        REWrap(projecttag),
                                        REWrap(streamtype),
                                        REWrap(streamname),
                                        REWrap(lumiblock),
                                        REWrap(application),
                                        REWrap(directory))

class ConfigHolder(object):
    """
    We made the choice of initializing all parameters in all cases, even if they
    can't be used. For example, EmailLogLevel has a default value ('CRITICAL') even
    if EmailLogList is empty, in which case EmailLogLevel is not used.
    """

    def __init__(self, cfgfile):

        self.FileName = os.path.split(cfgfile)[1]
        cfg = Config(cfgfile)

        # GENERAL PARAMETERS

        # Directory for log files
        self.LogDir = cfg.LogDir
        # Log severity level (DEBUG,INFO,WARNING,ERROR,CRITICAL)
        try:
            self.LogLevel = cfg.LogLevel
        except AttributeError:
            self.LogLevel = 'WARNING'
        try:
            self.LogFileMode = cfg.LogFileMode
        except AttributeError:
            self.LogFileMode = 'a'
        try:
            self.LogFileMaxBytes = cfg.LogFileMaxBytes
        except AttributeError:
            self.LogFileMaxBytes = 1000000
        try:
            self.LogFileBackupCount = cfg.LogFileBackupCount
        except AttributeError:
            self.LogFileBackupCount = 4

        # Email list which will receive error messages (list of strings)
        self.EmailLogList = []
        self.EmailLogLevel = 'CRITICAL'
        self.EmailLogSender = 'atlascdr@cern.ch'
        try:
            self.EmailLogList = [m for m in cfg.EmailLogList]
        except AttributeError: pass

        if self.EmailLogList:
            try: # Email Log severity level (DEBUG,INFO,WARNING,ERROR,CRITICAL)
                self.EmailLogLevel = cfg.EmailLogLevel
            except AttributeError: pass
            try: # Email sender
                self.EmailLogSender = cfg.EmailLogSender
            except AttributeError: pass

        # Managed directory lock file glob pattern
        try:
            self.LockFilePattern = cfg.LockFilePattern
        except AttributeError:
            self.LockFilePattern = None

        # ERS LOGGING
        self.ERSEnabled = False
        self.ERSTimeout = 10
        self.ERSLogLevel = 'error'
        self.ERSPartition = 'initial'
        self.ERSEnvFatal = ''
        self.ERSEnvError = ''
        self.ERSEnvWarning = ''
        self.ERSEnvInfo = ''
        self.ERSEnvDebug = ''

        try: self.ERSEnabled = cfg.ERSEnabled
        except AttributeError: pass

        if self.ERSEnabled:
            try: # ERS thread loop event timeout in second
                self.ERSTimeout = cfg.ERSTimeout
            except AttributeError: pass
            try: # ERS severity level (DEBUG,INFO,WARNING,ERROR,CRITICAL)
                self.ERSLogLevel = cfg.ERSLogLevel
            except AttributeError: pass
            try: # Partition to log to
                self.ERSPartition = cfg.ERSPartition
            except AttributeError: pass

            # Stream ers to
            try:
                self.ERSEnvFatal = cfg.ERSEnvFatal
            except AttributeError: pass
            try:
                self.ERSEnvError = cfg.ERSEnvError
            except AttributeError: pass
            try:
                self.ERSEnvInfo = cfg.ERSEnvInfo
            except AttributeError: pass
            try:
                self.ERSEnvWarning = cfg.ERSEnvWarning
            except AttributeError: pass
            try:
                self.ERSEnvDebug = cfg.ERSEnvDebug
            except AttributeError: pass

        # IS STATS PUBLICATION
        self.ISEnabled = False
        self.ISPartition = 'initial'
        self.ISServer = 'RunParams'
        self.ISPublicationPeriod = 5
        self.ISFileSystemInfoEnabled = False
        self.ISWaitAfterParitionValid = 60
        self.ISCpuStatsEnabled = False
        self.ISStorageIoStatsDevices = []

        try:
            self.ISEnabled = cfg.ISEnabled
        except AttributeError: pass

        if self.ISEnabled:
            try: # Partition to publish to
                self.ISPartition = cfg.ISPartition
            except AttributeError: pass
            try: # IS Server to publish to
                self.ISServer = cfg.ISServer
            except AttributeError: pass
            try: # Publication period in second
                self.ISPublicationPeriod = cfg.ISPublicationPeriod
            except AttributeError: pass
            try: # Publish File System Occupancy Info of directories in SrcDirs
                self.ISFileSystemInfoEnabled = cfg.ISFileSystemInfoEnabled
            except AttributeError: pass
            try:
                # after the partition used for IS becomes valid we have to wait a bit
                # before trying to publish (otherwise unrecoverable errors)
                self.ISWaitAfterParitionValid = cfg.ISWaitAfterParitionValid
            except AttributeError: pass
            try: # Publish global CPU stats
                self.ISCpuStatsEnabled = cfg.ISCpuStatsEnabled
            except AttributeError: pass
            try: # Publish storage devices I/O stats for ISStorageIoStatsDevices
                # psutil.disk_io_counters() returns a dictionary where keys are
                # device names
                for d in cfg.ISStorageIoStatsDevices:
                    dd = d
                    if os.path.islink(d):
                        # resolve link
                        dd = os.path.basename(
                                os.path.abspath(
                                os.path.join(
                                os.path.dirname(d),
                                os.readlink(d))))
                    elif os.path.isabs(d):
                        dd = os.path.basename(d)
                    self.ISStorageIoStatsDevices.append(dd)
            except AttributeError: pass

        # METADATA DATABASE

        # Oracle connection string
        # Use None to disable DB access
        # To avoid having credential in plain text in config files, we now rely
        # on authentication.xml file containing credentials.
        # The authentication.xml is looked up in $CORAL_AUTH_PATH.
        #
        # Example:
        #   self.connection = 'oracle://atonr_conf/ATLAS_SFO_T0'
        self.DBURL = None
        self.DBFileTable = ''
        self.DBLBTable = ''
        self.DBRunTable = ''

        try:
            self.DBURL = cfg.DBURL
        except AttributeError: pass

        if self.DBURL:
            # File table name
            self.DBFileTable = cfg.DBFileTable
            # Lumiblock table name
            self.DBLBTable = cfg.DBLBTable
            # Run table name
            self.DBRunTable = cfg.DBRunTable


        # MAIN THREAD

        # If Oracle database is down, retry to connect to it every
        # MainThreadEventTimeout, in second
        # This is also the main thread loop event timeout
        self.MainThreadEventTimeout = cfg.MainThreadEventTimeout

        # The Oracle DB connection is renewed every DBReconnectTimeout seconds
        try:
            self.DBReconnectTimeout = cfg.DBReconnectTimeout
        except AttributeError:
            self.DBReconnectTimeout = 3600

        # Filename parser module
        try:
            self.FilenameParser = cfg.FilenameParser
        except AttributeError:
            self.FilenameParser = 'BaseFileNameParser'

        # Filename parser configuration
        try:
            self.FilenameParserConf = dict(cfg.FilenameParserConf)
        except AttributeError:
            self.FilenameParserConf = {}

        # Backend module
        try:
            self.BackendModule = cfg.BackendModule
        except AttributeError:
            self.BackendModule = 'eosstorage'
        self.backend = importlib.import_module("cs.StorageBackends." + self.BackendModule)

        # Backend params
        try:
            self.backend.init(dict(cfg.BackendModuleConf))
        except AttributeError:
            pass

        # Kerberos auth
        self.Keytab = None
        self.KrbUser = None
        self.KrbCache = None

        try:
            self.Keytab = cfg.Keytab
        except AttributeError: pass

        if self.Keytab:
            self.KrbUser = cfg.KrbUser
            self.KrbCache = cfg.KrbCache

        # MANAGER THREAD

        # Directory List where to get Data Files (list of strings)
        self.SrcDirs = [os.path.normpath(f) for f in cfg.SrcDirs]
        if len(self.SrcDirs) == 0:
            raise RuntimeError('SrcDirs is empty')

        # Search for source files in local folders AND subfolders
        try:
            self.RecursiveSourceFolder = cfg.RecursiveSourceFolder
        except AttributeError:
            self.RecursiveSourceFolder = True

        # If true local_dir/subdir/a.data goes to remote_dir/subdir/a.data
        # else local_dir/subdir/a.data goes to remote_dir/a.data
        try:
            self.ReplicateTreeStructure = cfg.ReplicateTreeStructure
        except AttributeError:
            self.ReplicateTreeStructure = True

        # Unix pathname pattern for file selection
        # self.FilePattern = ['*.data', '*.out']
        # List comprehension is used to transform the Config::sequence in a proper list
        self.DataFilePattern = [p for p in cfg.DataFilePattern]
        if len(self.DataFilePattern) == 0:
            raise RuntimeError('DataFilePattern is empty')

        # A regex to exclude some files matching DataFilePattern
        try:
            self.ExcludeFileRegex = cfg.ExcludeFileRegex
        except AttributeError:
            self.ExcludeFileRegex = None

        # Number of files to be deleted before to update the list of files
        # to be copied
        try:
            self.ManagerThreadClearedFilesPeriod = cfg.ManagerThreadClearedFilesPeriod
        except AttributeError:
            self.ManagerThreadClearedFilesPeriod = 10

        # Manager thread loog event timeout in second
        self.ManagerThreadEventTimeout = cfg.ManagerThreadEventTimeout

        # Delay (s) before to put .PROBLEMATIC file back into the CopyQueue
        try:
            self.ProblDelay = cfg.ProblDelay
        except AttributeError:
            self.ProblDelay = 600

        # At each retry, the delay for .PROBLEMATIC files is scaled
        # using this factor:
        # ActualProblDelay = ProblDelay*e^(ProblScalingFactor*#retries)
        try:
            self.ProblScalingFactor = cfg.ProblScalingFactor
        except AttributeError:
            self.ProblScalingFactor = 0.5

        # Actual .PROBLEMATIC delay limit to enable CRITICAL messages
        try:
            self.ProblDelayLimit = cfg.ProblDelayLimit
        except AttributeError:
            self.ProblDelayLimit = 36000

        # Minimal file size (in kB)
        try:
            self.MinSizekB = cfg.MinSizekB
        except AttributeError:
            self.MinSizekB = None

        #If small files should be removed
        try:
            self.RemoveSmallFiles = cfg.RemoveSmallFiles
        except AttributeError:
            self.RemoveSmallFiles = False

        # Skip files that are symlinks
        try:
            self.SkipSymlinkFiles = cfg.SkipSymlinkFiles
        except AttributeError:
            self.SkipSymlinkFiles = False

        # Directory where to copy the Data Files (string)
        # Keywords from the filename parser can be used here
        self.CopyDir = cfg.CopyDir

        # Stage Host
        self.EOSInstance = cfg.EOSInstance

        # Stream driven pool
        # [{'targetdir':'/some/where', 'eosinstance':'eosatlas',
        # 'projecttag':'data_test', 'streamtype':'physics',
        # 'streamname':'something' ,'lumiblock':'22?', 'application':'SFO*',
        # 'directory':'/some/dir'},]
        self.DrivenPool = []
        try:
             # acess to trigger AttributeError exception if absent
            tmp = cfg.DrivenPool
            del tmp
            for i in cfg.DrivenPool:
                d = dict(i)
                try:
                    self.DrivenPool.append(DrivenPool(**d))
                except TypeError as e:
                    print('Parsing driven pool entry failed: %s' % str(i))
                    raise e
        except AttributeError:
            pass

        # COPY THREAD
        self.CopyEnabled = True
        self.MaxConcurrentCopy = 1
        self.MaxCopyImmediateRetry = 2
        self.CopyThreadEventTimeout = 10
        self.TransferTimeout = 0
        self.StopOngoingTransferAfterLock = False
        self.ComputeChecksumFromLocalFile = False
        self.BWDevice = None
        self.BWLimit = 0

        try:
            self.CopyEnabled = cfg.CopyEnabled
        except AttributeError:
            self.CopyEnabled = True

        if self.CopyEnabled:
            # Copy thread loop event timeout in second
            self.CopyThreadEventTimeout = cfg.CopyThreadEventTimeout
            # Timeout (s) after which kill and retry a copy process in None status
            self.TransferTimeout = cfg.TransferTimeout

            try: # Max number of concurrent cp processes
                self.MaxConcurrentCopy = cfg.MaxConcurrentCopy
            except AttributeError: pass
            try:
                # In case of failure, max number of retries on the spot
                # (before declaring problematic)
                self.MaxCopyImmediateRetry = cfg.MaxCopyImmediateRetry
            except AttributeError: pass
            try: # Kill copy processes if FS gets locked
                self.StopOngoingTransferAfterLock = cfg.StopOngoingTransferAfterLock
            except AttributeError: pass
            try: # Compute adler32 checksum from the file
                self.ComputeChecksumFromLocalFile = cfg.ComputeChecksumFromLocalFile
            except AttributeError: pass
            try: # Bandwidth limit
                self.BWLimit = cfg.BWLimit
            except AttributeError: pass
            try: # Bandwidth device
                self.BWDevice = cfg.BWDevice
            except AttributeError: pass


        # DELETE THREAD
        self.DeleteEnabled = True
        self.DeleteThreadEventTimeout = 10
        self.DeleteHighCriticalMark = 90
        self.DeleteLowCriticalMark = 80
        self.DeleteLowWaterMark = []
        self.DeleteIgnoreLock = False
        self.DeleteFileSystemUsagePeriod = 300
        self.DeleteMinFileAge = 0
        self.MigrationCheck = False
        self.MigrationCheckDelay = 1800
        self.MigrationModuleName = None
        self.MigrationModuleConf = {}
        self.MigrationCheckMaxDurationHour = None
        self.MigrationCheckMaxDurationLogLevel = None
        self.DeleteExcludeFileRegex = None

        try: # Enable/disable deletion thread and functionality completely
            self.DeleteEnabled = cfg.DeleteEnabled
        except AttributeError: pass

        if self.DeleteEnabled:
            # Delete thread loop event timeout in second
            self.DeleteThreadEventTimeout = cfg.DeleteThreadEventTimeout

            try:# Max total filesystem usage allowed while waiting for migration (in %)
                self.DeleteHighCriticalMark = cfg.DeleteHighCriticalMark
            except AttributeError: pass
            try: # Min total filesystem usage allowed while waiting for migration (in %)
                self.DeleteLowCriticalMark = cfg.DeleteLowCriticalMark
            except AttributeError: pass

            # Low watermark: do not delete if filesystems usage is below threshold (in %)
            try:
                self.DeleteLowWaterMark = [i for i in cfg.DeleteLowWaterMark]
            except AttributeError: pass
            self.DeleteLowWaterMark.extend([0] * (len(self.SrcDirs)-len(self.DeleteLowWaterMark)))

            try: # Is simultaneous writing and deleting allowed?
                self.DeleteIgnoreLock = cfg.DeleteIgnoreLock
            except AttributeError: pass
            try: # Get filesystem info after DeleteFileSystemUsagePeriod seconds
                self.DeleteFileSystemUsagePeriod = cfg.DeleteFileSystemUsagePeriod
            except AttributeError: pass
            try: # Do not delete files more recent than DeleteMinFileAge, in seconds
                self.DeleteMinFileAge = cfg.DeleteMinFileAge
            except AttributeError: pass

            try: # Is migration required before deleting?
                self.MigrationCheck = cfg.MigrationCheck
            except AttributeError: pass
            if self.MigrationCheck:
                try:
                    self.MigrationCheckDelay = cfg.MigrationCheckDelay
                except AttributeError:
                    pass
                try:
                    self.MigrationModuleName = cfg.MigrationModuleName
                except AttributeError:
                    pass
                if self.MigrationModuleName:
                    try:
                        self.MigrationModuleConf = dict(cfg.MigrationModuleConf)
                    except AttributeError:
                        pass

            try:
                # One can restrict the files to be deleted by making this a subset
                # of DataFilePattern
                self.DeleteExcludeFileRegex = cfg.DeleteExcludeFileRegex
            except AttributeError: pass

            try:
                # alert when a file waiting for migration delete is older than
                # this value
                # old is determined from the file's modification time
                self.MigrationCheckMaxDurationHour = cfg.MigrationCheckMaxDurationHour
                # default value when previous parameter is valued
                self.MigrationCheckMaxDurationLogLevel = logging.CRITICAL
                self.MigrationCheckMaxDurationLogLevel = \
                        LogConfig.str_to_log_level(cfg.MigrationCheckMaxDurationLogLevel)
            except AttributeError: pass
        # ENDIF DeleteEnabled

        # DDM MONITORING
        self.DdmMonitoringEnabled = False
        self.DdmMonitoringTimeout = 10
        self.DdmMonitoringProxy = 'atlasgw-exp:3128'
        self.DdmMonitoringEndpoint = 'https://rucio-lb-prod.cern.ch/traces/'
        self.DdmMonitoringQueueMaxSize = 200

        try: self.DdmMonitoringEnabled = cfg.DdmMonitoringEnabled
        except AttributeError: pass

        if self.DdmMonitoringEnabled:
            try: # DdmMonitoringThread loop event timeout
                self.DdmMonitoringTimeout = cfg.DdmMonitoringTimeout
            except AttributeError: pass
            try: self.DdmMonitoringProxy = cfg.DdmMonitoringProxy
            except AttributeError: pass
            try: self.DdmMonitoringEndpoint = cfg.DdmMonitoringEndpoint
            except AttributeError: pass
            try: self.DdmMonitoringQueueMaxSize = cfg.DdmMonitoringQueueMaxSize
            except AttributeError: pass
