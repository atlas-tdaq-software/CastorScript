#!/bin/env python
import os.path
import re
import importlib
from operator import itemgetter

class MigrationChecker:

    def __init__(self, conf, logger, exitevent):
        """
        conf is a dictionary that contains or may contain the following keys:
        'eosinstance': EOS instance for storage backend
        'basedir': base path in backend's EOS instance
        'nameparser': name of the module to parse filenames (optional)
        'cache_validity_s': validity duration of cache entries in seconds (optional)
        'backend': storage backend used to search migrated files (optional)
        """
        nameparser = conf.get('nameparser', 'SFOFileNameParser')
        module = importlib.import_module(f'cs.Tools.FilenameParsers.{nameparser}')
        self.filenameparser = getattr(module, nameparser)

        backend_name = conf.get('backend', 'ctastorage')
        self.backend = importlib.import_module(f'cs.StorageBackends.{backend_name}')

        self.eosinstance = conf['eosinstance']
        self.basedir = conf['basedir']
        self.logger = logger

        cache_validity_s = conf.get('cache_validity_s', None)
        self.backend_cache = None
        if cache_validity_s is not None:
            self.backend_cache = self.backend.start_cache(cache_validity_s,
                    self.logger, exitevent)


    def stop(self):
        if self.backend_cache is not None:
            self.backend_cache.join()
            self.backend_cache = None


    def isMigrated(self, remote_file, verbose=False):
        """
        Destination directory:
        /<basedir>/<projecttag>/<streamtype_streamname>/<runnr-8digit>/<dataset>/

        A file name is:
        1) if all SFO files of a LB fit are merged into a single file
           <project>.<runnr>.<stream>.daq.RAW._lbXXXX._SFO-N._<fileseqnr>.data, N=4,5,9,10
           --> <project>.<runnr>.<stream>.merge.RAW._lbXXXX._SFO-ALL._0001.<attemptnr>

        2) if all SFO files of a LB don't merge into a single file
           <project>.<runnr>.<stream>.daq.RAW._lbXXXX._SFO-N._<fileseqnr>.data, N=4,5,9,10
           --> <project>.<runnr>.<stream>.merge.RAW._lbXXXX._SFO-N._<fileseqnr>.<attemptnr>, N=4,5,9,10

        3) if not merged
           <project>.<runnr>.<stream>.daq.RAW._lbXXXX._SFO-N._<fileseqnr>.data, N=4,5,9,10 --> <project>.<runnr>.<stream>.daq.RAW._lbXXXX._SFO-N._<fileseqnr>.data, N=4,5,9,10

        All filenames may have a _<timestamp> appended in case of DDM transfer failure. In this case the large timestamp should be considered

        debug_* are redirected to debug_all, but file names are preserved.
        """
        if verbose:
            print(f'checking migration of {remote_file}')
        original_basename = os.path.basename(remote_file)

        ### Build the target directory name
        parsed = self.filenameparser(original_basename)

        streamname = parsed.StreamName()
        dataset = parsed.Dataset()

        if parsed.StreamType() == 'debug':
            streamname = 'all'
            dataset = dataset.replace("_%s" % parsed.StreamName(),
                                      "_%s" % streamname)

        org_path = os.path.join(self.basedir, parsed.ProjectTag(),
                             '%s_%s' % (parsed.StreamType(),
                                        streamname),
                             '%s' % parsed.RunNr(),
                             '%s' % dataset)

        merged_path = os.path.join(self.basedir, parsed.ProjectTag(),
                            '%s_%s' % (parsed.StreamType(),
                                       streamname),
                            '%s' % parsed.RunNr(),
                            '%s' % dataset.replace(".daq.",".merge."))

        if verbose:
            print('Search path (ORG):',org_path)
            print('Search path (MER):',merged_path)

        ### Look for files into the dataset dir
        success, all_files = self.backend.listdir(
                org_path, self.eosinstance, self.logger, self.backend_cache)

        if success:
            ### Match original filename + transfer errors
            regex = "^" + original_basename + "(?:_(?P<timestamp>[0-9]+))?$"
            regex = regex.replace('.','[.]')
            if verbose:
                print('Original file regex',regex)

            regex = re.compile(regex)
            match = regex.match

            found = [(int(m.groupdict(0)['timestamp']), m.group(0)) for f in all_files for m in (match(f),) if m]

            ### Take the most recent attempt (0 is the firt attempt)
            found.sort(key=itemgetter(0))
            if found:
                if verbose: print('Found:', found)
                return self.backend.migrated(
                        os.path.join(org_path, found[-1][-1]),
                        self.eosinstance,
                        self.logger)

        ### Look for files into the dataset dir
        success, all_files = self.backend.listdir(merged_path, self.eosinstance,
                                                self.logger, self.backend_cache)

        ### If fails, return false
        if not success: return False

        #^name[.](?:(?:_SFO-1._ZZZZ)|(?:_SFO-ALL._0001))[.](?P<t0attempt>[0-9]+)(?:_(?P<timestamp>[0-9]+))?$

        ### Create the two type of name we expect (neglecting attempt number)
        basename = os.path.splitext(original_basename)[0]
        merged = basename.replace('.daq.','.merge.')

        original_sfo = merged.split('.',6)[-1]
        basename = '.'.join(merged.split('.',6)[:-1])

        regex = "^" + basename + \
            ".(?:(?:%s)|(?:_SFO-ALL._0001))."  % original_sfo + \
            "(?P<t0attempt>[0-9]+)(?:_(?P<timestamp>[0-9]+))?$"
        regex = regex.replace('.','[.]')
        if verbose:
            print('Merged file regex',regex)
        regex = re.compile(regex)
        match = regex.match

        ### The 't0attempt' must be always present hence
        ### we do not need a default
        found = [(int(m.groupdict()['t0attempt']), int(m.groupdict(0)['timestamp']), m.group(0)) for f in all_files for m in (match(f),) if m]

        found.sort(key=itemgetter(0,1))
        if found:
            if verbose: print('Found:', found)
            return self.backend.migrated(
                    os.path.join(merged_path, found[-1][-1]),
                    self.eosinstance,
                    self.logger)
        return False


if __name__ == '__main__':
    import argparse
    import logging
    import sys

    argp = argparse.ArgumentParser(description='Check file tape migration',
                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    argp.add_argument('-e', '--eosinstance',
            default='eosctaatlas',
            help='name of the EOS instance used by the storage backend')
    argp.add_argument('-b', '--basedir',
            default='/eos/ctaatlas/archive/grid/atlas/rucio/raw',
            help="base path to search for files in backend")
    argp.add_argument('file',
            help='full remote path of the file to check')
    args = argp.parse_args()

    config = {
            'eosinstance': args.eosinstance,
            'basedir': args.basedir,
            }

    logr = logging.getLogger()
    logr.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    logr.addHandler(handler)

    # cache_validity_s is not set so the exitevent is not used: None
    checker = MigrationChecker(config, logr, None)
    print(checker.isMigrated(args.file, verbose=True))
