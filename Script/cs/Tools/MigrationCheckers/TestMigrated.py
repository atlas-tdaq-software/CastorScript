#!/bin/env python
import cs.Tools.MigrationCheckers.DDMMigrated as DDMMigrated

class MigrationChecker(DDMMigrated.MigrationChecker):
    def isMigrated(self, remote_file, verbose=False):
        del verbose
        # we don't need this step here but it is used by DDMMigrated
        # so I still call it to test it somehow
        _, _ = self.backend.listdir(self.basedir, self.eosinstance, self.logger,
                self.backend_cache)
        self.logger.debug(f'TestMigrated.isMigrated: remote_file: {remote_file}')
        return self.backend.migrated(remote_file, self.eosinstance, self.logger)
