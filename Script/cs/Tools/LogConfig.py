import socket
import os
import logging
import mailinglogger

def make_is_data_identifier(is_server,tdaq_app_name):
    return "{}.{}.State".format(is_server,tdaq_app_name)

def make_tdaq_app_name(config):
    hostname = socket.gethostname()
    cleanFileName = config.FileName.replace("Config", "",1).replace("Conf","",1)\
            .replace(".cfg","",1).replace("-","").replace("_","").replace(".testcfg","",1)
    if len(cleanFileName) == 0:
        return "CastorScript-{}".format(hostname)
    return "CastorScript-{}-{}".format(hostname, cleanFileName)

def enable_root_logger():
    #let all logs get to the root logger
    #(log levels are set in each logger)
    logging.getLogger().setLevel("DEBUG")

def enable_mail_logging(config):
    app_name = make_tdaq_app_name(config)
    # by default max email sending rate is 10 per hour
    mail_handler = mailinglogger.MailingLogger(config.EmailLogSender,
            config.EmailLogList, subject=f'CastorScript message: {app_name}',
            mailhost='localhost')

    mail_handler.setFormatter(formatter)
    set_log_level(config.EmailLogLevel, mail_handler)
    logging.getLogger().addHandler(mail_handler)

def str_to_log_level(level_str):
    level_str = level_str.upper()
    levelmap = {'DEBUG':logging.DEBUG,
                'INFO':logging.INFO,
                'WARNING':logging.WARNING,
                'ERROR':logging.ERROR,
                'CRITICAL':logging.CRITICAL}
    return levelmap[level_str]

def set_log_level(level, logger):
    logger.setLevel(str_to_log_level(level))

formatter = logging.Formatter('%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s')

def enable_file_logging(loggername, filename, config):
    '''
    The following parameters are used from config:
    - LogDir: directory for log files
    - LogLevel: logs with severity below this level are discarded
    - LogFileMode: file opening mode ('a', 'w', etc.)
    - LogFileMaxBytes: size after which the file is rolled over
    - LogFileBackupCount: max number of backup files to keep
    See python's RotatingFileHandler documentation for mode, maxbytes,
    and backupcount
    '''
    logpathname = os.path.join(str(config.LogDir), filename)
    filehandler = logging.handlers.RotatingFileHandler(logpathname,
            mode=config.LogFileMode, maxBytes=config.LogFileMaxBytes,
            backupCount=config.LogFileBackupCount)
    filehandler.setFormatter(formatter)

    set_log_level(config.LogLevel, filehandler)
    logger = logging.getLogger(loggername)
    logger.addHandler(filehandler)

    return logger
