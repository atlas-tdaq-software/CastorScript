#!/bin/env python
import sys
import datetime
import time
import coral_auth
import oracledb
oracledb.init_oracle_client()

# Errors for Oracle Database
# StandardError:
#   Warning
#   Error:
#     InterfaceError
#     DatabaseError:
#       DataError
#       OperationalError
#       IntegrityError
#       InternalError
#       ProgrammingError
#       NotSupportedError

class OraDB(object):

    def __init__(self, url):
        self.connstring = url
        self.conn_user, self.conn_pwd, self.conn_dbn = coral_auth.get_connection_parameters_from_connection_string(self.connstring)
        self.orcl = oracledb.connect(user=self.conn_user, password=self.conn_pwd, dsn=self.conn_dbn)
        self.curs = self.orcl.cursor()
        self.lastKeys = {}

    def execute(self,args,keys):
        self.lastKeys = keys
        self.curs.execute(*args,**keys)
        self.orcl.commit()
        return self.curs.rowcount

    def getNextRow(self):
        return self.curs.fetchone()

    def getDateVar(self):
        return self.curs.var(oracledb.DATETIME)

    def lastOp(self):
        return self.curs.statement

    def getLastKeys(self):
        return self.lastKeys

    def refresh(self):
        self.orcl.commit()
        self.orcl.close()
        self.orcl = oracledb.connect(user=self.conn_user, password=self.conn_pwd, dsn=self.conn_dbn)
        self.curs = self.orcl.cursor()

class Database():

    def __init__(self, conf, logger, parser):
        self.conf = conf
        self.parser = parser

        ##### Set Database parameters #####
        self.db = OraDB(self.conf.DBURL)
        self.file_table = self.conf.DBFileTable
        self.lb_table = self.conf.DBLBTable
        self.run_table = self.conf.DBRunTable

        #### Set Logger for Database #####
        self.logger = logger

    def Reconnect(self):
        self.db.refresh()
        self.logger.info("DB connection refreshed.")

    def FileExists(self, pfn):
        """
        Return boolean pair:
        - first to indicate if there was an error
        - second to indicate if the file entry exists
        """
        req = "select count(1) from {} where sfopfn=:sfopfn".format(self.file_table)
        try:
            self.db.execute([req], {'sfopfn':pfn})
            res = self.db.getNextRow()
            if 1 in res:
                return (True, True)
            else:
                return (True, False)
        except (oracledb.InterfaceError,oracledb.DatabaseError) as ex:
            errstr = 'database error: {}, request: "{}", request keys:"{}"'.format(
                str(ex), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)
            return (False, False)
        except:
            errstr = 'error: {}, request: "{}", request keys:"{}"'.format(
                str(sys.exc_info()), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)
            return (False, False)


    def FileInfo(self, sfofile):

        sql="select checksum,filesize,filehealth from " + self.file_table + " "
        sql+="where sfopfn = :ssfopfn"
        args=[]
        keys={}
        keys['ssfopfn']=sfofile
        args.append(sql)
        result = (None, None, None)
        try:
            self.db.execute(args,keys)
            row = self.db.getNextRow()
            if row is None:
                # cannot find the file in the DB
                return (None, None, None)
            result = row[:3]
        except (oracledb.InterfaceError,oracledb.DatabaseError) as ex:
            errstr = 'database error: {}, request: "{}", request keys:"{}"'.format(
                str(ex), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)
        except:
            errstr = 'error: {}, request: "{}", request keys:"{}"'.format(
                str(sys.exc_info()), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)

        return result

    def Deletion(self,sfofile):
        try:
            self.filedeletion(sfofile)
            return True
        except (oracledb.InterfaceError,oracledb.DatabaseError) as ex:
            errstr = 'database error: {}, request: "{}", request keys:"{}"'.format(
                str(ex), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)
            return False
        except:
            errstr = 'error: {}, request: "{}", request keys:"{}"'.format(
                str(sys.exc_info()), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)
            return True

    def Transfer(self, sfofile, remotefile):

        retry = True
        while retry:
            retry = False
            try:
                self.filetransfer(sfofile, remotefile)
            except (oracledb.InterfaceError, oracledb.DatabaseError) as ex:
                ex_info = str(ex)
                errstr = 'database error: {}, request: "{}", request keys:"{}"'.format(
                    str(ex), str(self.db.lastOp()), str(self.db.getLastKeys()))
                self.logger.error(errstr)

                if 'ORA-20199' in ex_info and 'NEXTVAL' in ex_info:
                    retry = True
                else:
                    return False

            except:
                errstr = 'error: {}, request: "{}", request keys:"{}"'.format(
                    str(sys.exc_info()), str(self.db.lastOp()), str(self.db.getLastKeys()))
                self.logger.error(errstr)
                return True


        try:
            parsed=self.parser(sfofile)

            # mark the corresponding SFO_TZ_RUN entry as TRANSFERRING for Tier 0
            self.markStreamTransferring(parsed)

            if self.notTransFiles(parsed)==0:
                self.lbtransfer(parsed)

            if self.notTransLBs(parsed)==0:
                self.runtransfer(parsed)

            return True

        except (oracledb.InterfaceError, oracledb.DatabaseError) as ex:
            errstr = 'database error: {}, request: "{}", request keys:"{}"'.format(
                str(ex), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)
            return False

        except:
            errstr = 'error: {}, request: "{}", request keys:"{}"'.format(
                str(sys.exc_info()), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)
            return True


    def UpdateTruncated(self, sfofile, size, checksum):

        sql="update " + self.file_table + " "
        sql+="set filesize=:ssize, checksum=:schecksum "
        sql+="where sfopfn = :ssfopfn"
        args=[]
        keys={}
        keys['ssfopfn']=sfofile
        keys['ssize']=size
        keys['schecksum']=checksum.upper()
        args.append(sql)

        try:
            self.db.execute(args,keys)
            return True
        except (oracledb.InterfaceError,oracledb.DatabaseError) as ex:
            errstr = 'database error: {}, request: "{}", request keys:"{}"'.format(
                str(ex), str(self.db.lastOp()), str(self.db.getLastKeys())
            )
            self.logger.error(errstr)
        except:
            errstr = 'error: {}, request: "{}", request keys:"{}"'.format(
                str(sys.exc_info()), str(self.db.lastOp()), str(self.db.getLastKeys()))
            self.logger.error(errstr)

        return False


    def filedeletion(self,sfofile):
        sql="update " + self.file_table + " "
        sql+="set filestate='DELETED', t_stamp = :newdate "
        sql+="where sfopfn = :ssfopfn and filestate = 'CLOSED'"
        args=[]
        keys={}
        keys['newdate']=datetime.datetime.utcfromtimestamp(time.time())
        keys['ssfopfn']=sfofile
        args.insert(0,sql)
        nrow = self.db.execute(args,keys)
        if nrow != 1:
            self.logger.error('Deletion of file: %s. Not exactly one entry updated in file table: %s', sfofile, nrow)
        else:
            self.logger.debug('File table. File state updated to DELETED for file: ' + sfofile)

    def filetransfer(self, sfofile, remotefile):
        sql = "update "+ self.file_table + " "
        sql += "set transferstate='TRANSFERRED', t_stamp = :newdate, "
        sql += "pfn = :newpfn, svcclass = :newsvcclass "
        sql += "where sfopfn = :ssfopfn and filestate = 'CLOSED'"

        args=[]
        keys={}
        keys['newdate'] = datetime.datetime.utcfromtimestamp(time.time())
        keys['newpfn'] = remotefile
        keys['ssfopfn'] = sfofile
        keys['newsvcclass'] = ''
        args.insert(0,sql)

        nrow = self.db.execute(args,keys)

        if nrow != 1:
            self.logger.critical('transfer of file %s to %s: not exactly one entry updated in file table (%s)',
                    sfofile, remotefile, str(nrow))
        else:
            self.logger.debug('File table. File state updated to TRANSFERRED for file: ' + sfofile)

    def lbtransfer(self,parsed):

        keys={}
        keys['ssfo']=parsed.AppId()
        keys['srunnr']=parsed.RunNr()
        keys['slbnr']=parsed.LBNr()
        keys['sstreamt']=parsed.StreamType()
        keys['sstreamn']=parsed.StreamName()

        sql="select state from " + self.lb_table + " "
        sql+="where sfoid = :ssfo and runnr = :srunnr and lumiblocknr = :slbnr"
        sql+=" and streamtype = :sstreamt and stream = :sstreamn"

        args = [sql,]
        self.db.execute(args,keys)
        state = self.db.getNextRow()[0]

        if state == 'CLOSED':
            sql="update "+ self.lb_table + " "
            sql+="set state='TRANSFERRED', t_stamp = :newdate "
            sql+="where sfoid = :ssfo and runnr = :srunnr "
            sql+=" and lumiblocknr = :slbnr"
            sql+=" and streamtype = :sstreamt and stream = :sstreamn "
            sql+=" and state = 'CLOSED'"

            keys['newdate']=datetime.datetime.utcfromtimestamp(time.time())

            args = [sql,]
            nrow = self.db.execute(args,keys)

            if nrow != 1:
                self.logger.error('Not exactly one entry updated in '+
                                  'lumiblock table for SFO '
                                  + str(parsed.AppId())+
                                  ', run ' + str(parsed.RunNr()) +
                                  ', lumiblock ' + str(parsed.LBNr()) +
                                  ', stream type ' + str(parsed.StreamType()) +
                                  ', stream name ' + str(parsed.StreamName()))
            else:
                self.logger.debug('Lumiblock table. Lumiblock state updated '+
                                  'to TRANSFERRED for SFO '
                                  + str(parsed.AppId()) +
                                  ', run ' + str(parsed.RunNr()) +
                                  ', lumiblock ' + str(parsed.LBNr()) +
                                  ', stream type ' + str(parsed.StreamType()) +
                                  ', stream name ' + str(parsed.StreamName()))

    def runtransfer(self,parsed):
        keys={}
        keys['ssfo']=parsed.AppId()
        keys['srunnr']=parsed.RunNr()
        keys['sstreamt']=parsed.StreamType()
        keys['sstreamn']=parsed.StreamName()

        sql="select state from "+ self.run_table + " "
        sql+="where sfoid = :ssfo and runnr = :srunnr "
        sql+=" and streamtype = :sstreamt and stream = :sstreamn "

        args=[sql,]
        self.db.execute(args,keys)
        state = self.db.getNextRow()[0]

        if state == 'CLOSED':

            sql="update "+ self.run_table
            sql+=" set state='TRANSFERRED', transferstate='TRANSFERRED', t_stamp = :newdate"
            sql+=" where sfoid = :ssfo and runnr = :srunnr "
            sql+=" and streamtype = :sstreamt and stream = :sstreamn "
            sql+=" and state = 'CLOSED'"

            keys['newdate']=datetime.datetime.utcfromtimestamp(time.time())

            args = [sql,]
            nrow = self.db.execute(args,keys)

            if nrow != 1:
                self.logger.error('Not exactly one entry updated'
                                  ' in run table '+
                                  'for SFO ' + str(parsed.AppId()) +
                                  ', run ' + str(parsed.RunNr()) +
                                  ', stream type ' + str(parsed.StreamType()) +
                                  ', stream name ' + str(parsed.StreamName()))
            else:
                self.logger.debug('Run table. Run state updated '
                                  + 'to TRANSFERRED ' +
                                  'for SFO ' + str(parsed.AppId()) +
                                  ', run ' + str(parsed.RunNr()) +
                                  ', stream type ' + str(parsed.StreamType()) +
                                  ', stream name ' + str(parsed.StreamName()))


    def markStreamTransferring(self, parsed):
        keys={}
        keys['ssfo']=parsed.AppId()
        keys['srunnr']=parsed.RunNr()
        keys['sstreamt']=parsed.StreamType()
        keys['sstreamn']=parsed.StreamName()

        sql="update "+ self.run_table
        sql+=" set transferstate='TRANSFERRING', t_stamp = :newdate"
        sql+=" where sfoid = :ssfo and runnr = :srunnr"
        sql+=" and streamtype = :sstreamt and stream = :sstreamn"
        sql+=" and (transferstate is NULL or transferstate != 'TRANSFERRING')" # avoid unnecessary update and overwriting of t_stamp

        keys['newdate'] = datetime.datetime.utcfromtimestamp(time.time())

        args = [sql,]
        nrow = self.db.execute(args,keys)
        self.logger.debug(
            'transferstate update for SFO %s, run %s, stream type %s, stream name %s: %s row updated'
            , parsed.AppId(), parsed.RunNr(), parsed.StreamType()
            , parsed.StreamName(), str(nrow))


    def notTransFiles(self,parsed):
        sql="select count(lfn) from " + self.file_table + " "
        sql+="where sfoid = :ssfo and runnr = :srunnr and lumiblocknr = :slbnr "
        sql+=" and streamtype = :sstreamt and stream = :sstreamn "
        sql+=" and transferstate = 'ONDISK'"
        args=[]
        args.insert(0,sql)
        keys={}
        keys['ssfo']=parsed.AppId()
        keys['srunnr']=parsed.RunNr()
        keys['slbnr']=parsed.LBNr()
        keys['sstreamt']=parsed.StreamType()
        keys['sstreamn']=parsed.StreamName()
        self.db.execute(args,keys)
        self.logger.debug('Count number of files ONDISK for SFO ' +
                          str(parsed.AppId()) +
                          ', run ' + str(parsed.RunNr()) +
                          ', lumiblock ' + str(parsed.LBNr()) +
                          ', stream type ' + str(parsed.StreamType()) +
                          ', stream name ' + str(parsed.StreamName()))

        return self.db.getNextRow()[0]

    def notTransLBs(self,parsed):
        sql="select count(state) from " + self.lb_table + " "
        sql+="where sfoid = :ssfo and runnr = :srunnr "
        sql+=" and streamtype = :sstreamt and stream = :sstreamn "
        sql+=" and state <> 'TRANSFERRED'"
        args=[]
        args.insert(0,sql)
        keys={}
        keys['ssfo']=parsed.AppId()
        keys['srunnr']=parsed.RunNr()
        keys['sstreamt']=parsed.StreamType()
        keys['sstreamn']=parsed.StreamName()
        self.db.execute(args,keys)
        self.logger.debug('Count number of NOT TRANSFERRED ' +
                          'lumiblocks for SFO ' + str(parsed.AppId()) +
                          ', run ' + str(parsed.RunNr()) +
                          ', stream type ' + str(parsed.StreamType()) +
                          ', stream name ' + str(parsed.StreamName()))

        return self.db.getNextRow()[0]

    def dbConf(self,conf):
        self.logger.info('Update configuration')
        self.conf = conf
