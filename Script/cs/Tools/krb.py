import subprocess
import os
import datetime

def setCache(krbcachefile, logger):
    try:
        os.environ['KRB5CCNAME'] = krbcachefile
    except TypeError as ex:
        logger.error('KRB Cache file setting failed: %s ' % str(ex))


def updateToken(keytab, user, logger):
    kinit = subprocess.Popen(['kinit', '-kt', keytab, user], #'-l','3900s'],
                  stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = kinit.wait()

    if ret:
        logger.error('kinit failed. Ret code: %d Output: %s' %
                       (ret, kinit.stdout.read()))
        return None
    else:
        logger.debug('kinit succeeded. Output: %s' % kinit.stdout.read())
        return tokenExpiration(logger)


def tokenExpiration(logger):
    # on SLC6 klist (v1.10.3) does not take LC_ALL into account
    # on CentOS 9 klist (v1.14.1) DOES take it into account
    # This resulted in discrepancies in their output especially for timestamps
    # So it's safer to always set LC_ALL to a 'default' constant
    envm = os.environ.copy()
    envm['LC_ALL'] = 'C'
    klist = subprocess.Popen(['klist',], env = envm, stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
    ret = klist.wait()
    out = klist.stdout.read().decode()
    logger.debug('klist done. Return code: %d  Output: %s' % (ret,out))

    if ret:
        logger.error('klist failed: retcode=%s, output=%s', str(ret), out)
        return datetime.datetime(1970, 1, 1)

    ticket = None
    out = out.split('\n')
    for idx,l in enumerate(out):
        if 'Valid' in l:
            ticket = out[idx+1]
            break

    if ticket is None:
        logger.error('error parsing klist output: output=%s', out)
        return datetime.datetime(1970, 1, 1)

    ticket = ticket.split()
    logger.debug('Ticket expiration: %s' % ticket)
    try:
        expiration_date = datetime.datetime.strptime(' '.join(ticket[2:4]),
                '%m/%d/%y %H:%M:%S')
    except ValueError as exc:
        logger.error('error converting string to date: ticket="%s", date_string="%s"',
                ticket,  ' '.join(ticket[2:4]))
        raise exc
    return expiration_date
