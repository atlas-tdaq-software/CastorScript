import subprocess

# if this file runs as a script instead of as a module.
# add /../../CastorScript/Script to path so imports keeps working
if __name__=='__main__':
    from os.path import dirname, abspath, join
    import sys

    # Very crude implementation
    SCRIPT_DIR = abspath(join(dirname(__file__), '..','..'))
    sys.path.append(SCRIPT_DIR)

from cs.Tools.utils import adler32

def sizechecksum(filename, stager, logger=None):
    """
    This still needs to return the size and checksum of the original file
    otherwise transfers fail inevitably.
    """
    del stager

    ls = subprocess.Popen(['ls', '-l1', filename],
                          stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = ls.wait()
    ls_output = ls.stdout.read().decode()

    if logger:
        logger.debug(ls_output)

    size = checksum = None
    if ret == 0:
        size = int(ls_output.split()[4])
        checksum = adler32(filename)

    return size, checksum

def listdir(directory, stager, logger=None):
    del directory, stager, logger
    return (True, [])

def migrated(filename, stager, logger=None):
    del filename, stager, logger
    return False

def mkdir(directory, stager, logger=None):
    del directory, stager, logger
    return 0

def remove(dstfile, stager, logger=None):
    del dstfile, stager, logger
    return 0

def backgroundcopy(srcfile, dstfile, stager, logger=None):
    """
    This still needs to return a process (subprocess object) so it spawns the
    simplest process that succeeds.
    """
    del srcfile, dstfile, stager, logger # unused args
    cp = subprocess.Popen(['true'], stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
    return cp, cp.pid

def copystate(proc):
    return proc.poll()

def copystdout(proc):
    return proc.stdout.read().decode()

if __name__=='__main__':
    import sys
    import cs.StorageBackends.storagetester as storagetester
    storagetester.execute(sys.modules[__name__])
