"""
Provides EOS backend functionalities.
Tape migration is not supported by EOS.
"""
import subprocess
import os
import sys

# if this file runs as a script instead of as a module.
if __name__=='__main__':
    from os.path import dirname, abspath, join

    # add /../../CastorScript/Script to path so imports keeps working
    SCRIPT_DIR = abspath(join(dirname(__file__), '..','..'))
    sys.path.append(SCRIPT_DIR)


## Global object storing configuration parameters ##
## copy_extra_params --> extra parameters appended to copy operations
config_dict = {}

def init(d):

    global config_dict
    config_dict = d


def sizechecksum(filename, stager, logger=None):
    if logger:
        logger.debug('Fetching checksum from eos for: %s', filename)

    cmd = ['xrdfs', stager, 'stat', filename]
    xrdfs = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = xrdfs.wait()
    out = xrdfs.stdout.read().decode()

    # Find and get the size, log exception if any
    size = None
    if not ret:
        try:
            for line in out.split('\n'):
                if 'Size:' in line:
                    size = int(line.split()[1])

        except (KeyError,AttributeError,ValueError):
            if logger:
                logger.warning('Error parsing size for %s, command: %s, output: %s',
                        filename, cmd, out, exc_info=True)
    else:
        if logger:
            logger.warning('Size not found for %s: %s', filename, out)

    cmd = ['xrdfs', stager, 'query', 'checksum', filename]
    xrdfs = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = xrdfs.wait()
    out = xrdfs.stdout.read().decode()

    # Get Checksum, log exception if any
    checksum = None
    if not ret:
        try:
            checksum = out.split()[1]
        except (KeyError, AttributeError, ValueError, IndexError):
            if logger:
                logger.warning('Error parsing checksum for %s, command: %s, output: %s',
                        filename, cmd, out, exc_info=True)
    else:
        if logger:
            logger.warning('Checksum not found for %s: %s', filename, out)

    return size, checksum


def remove(dstfile, stager, logger=None):
    cmd = ['xrdfs', stager, 'rm', dstfile]
    xrdfs = subprocess.Popen(cmd, stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
    ret = xrdfs.wait()

    if ret != 0 and logger is not None:
        logger.warning('error removing %s, cmd: %s, output: %s', dstfile,
                str(cmd), xrdfs.stdout.read().decode())

    return ret


def backgroundcopy(srcfile, dstfile, stager, logger=None):
    dstfile = 'root://%s/%s' % (stager, dstfile)
    cmd = ['xrdcp', '-f', '--nopbar', srcfile, dstfile]

    try:
        cmd.append(config_dict['copy_extra_params'])
    except KeyError:
        pass

    # Disable default XRootD's writing recovery policy
    modified_env = os.environ.copy()
    if 'XRD_WRITERECOVERY' in config_dict:
        modified_env['XRD_WRITERECOVERY'] = str(config_dict['XRD_WRITERECOVERY'])
    if 'XRD_STREAMTIMEOUT' in config_dict:
        modified_env['XRD_STREAMTIMEOUT'] = str(config_dict['XRD_STREAMTIMEOUT'])

    logger.debug("starting background copy, cmd: %s", str(cmd))
    xrdcp = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT, env=modified_env)

    return xrdcp, xrdcp.pid


def mkdir(directory, stager, logger=None):
    """
    In EOS directories are automagically created during copy, so this is just a dummy method.
    """
    del directory, stager, logger # unused args
    return 0


def migrated(filename, stager, logger=None):
    del filename, stager, logger # unused args
    return False


def copystate(proc):
    return proc.poll()


def copystdout(proc):
    return proc.stdout.read().decode()


if __name__=='__main__':
    import cs.StorageBackends.storagetester as storagetester
    storagetester.execute(sys.modules[__name__])
