
"""
Helper module to test storage backends

"""
import sys
import logging
import argparse
import time


def copywrapper(module, srcfile, dstfile, stager, logger):

    handle, _ = module.backgroundcopy(srcfile, dstfile, stager, logger)

    state = None
    while state == None:
        state = module.copystate(handle)
        time.sleep(1)

    print(module.copystdout(handle))


def execute(module):

    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler(sys.stdout))

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--command', dest='command')
    parser.add_argument('-t', '--target', dest='target')
    parser.add_argument('-s', '--stager', dest='stager')
    parser.add_argument('-o', '--opttarget', dest='opttarget', default=None)

    args = parser.parse_args()

    stdopt = (args.target, args.stager, logger)

    switch = {
        'sizechecksum':(module.sizechecksum, stdopt),
        'remove':(module.remove, stdopt),
        'mkdir':(module.mkdir, stdopt),
        'migrated':(module.migrated, stdopt),
        'backgroundcopy':(copywrapper, (module, args.target,
                                        args.opttarget, args.stager,
                                        logger))
        }

    function, args = switch[args.command]

    print(function(*args))
