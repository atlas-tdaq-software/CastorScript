import subprocess

# if this file runs as a script instead of as a module.
# add /../../CastorScript/Script to path so imports keeps working
if __name__=='__main__':
    from os.path import dirname, abspath, join
    import sys

    # Very crude implementation
    SCRIPT_DIR = abspath(join(dirname(__file__), '..','..'))
    sys.path.append(SCRIPT_DIR)

from cs.Tools.utils import adler32

def sizechecksum(filename, stager, logger=None):
    del stager

    ls = subprocess.Popen(['ls', '-l1', filename],
                          stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = ls.wait()
    lsOut = ls.stdout.read().decode()

    if logger:
        logger.debug(lsOut)

    size = checksum = None
    if ret == 0:
        size = int(lsOut.split()[4])
        checksum = adler32(filename)

    return size,checksum


def listdir(directory, stager, logger=None):
    del stager

    ls = subprocess.Popen(['ls', '-1', directory],
                          stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    outs, _ = ls.communicate()
    lsOut = outs.decode()

    if logger:
        logger.debug(lsOut)

    files = [f for f in lsOut.split('\n') if f]

    return ((ls.returncode == 0), files)


def migrated(filename, stager, logger=None):
    del filename, stager

    if logger:
        logger.fatal('Migration not support on local FS')

    sys.exit(1)


def mkdir(directory, stager, logger=None):
    del stager

    mkdir_proc = subprocess.Popen(['mkdir', '-p', directory], stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
    ret = mkdir_proc.wait()

    if logger:
        logger.debug(mkdir_proc.stdout.read())

    return ret


def remove(dstfile, stager, logger=None):
    del stager

    rm = subprocess.Popen(['rm', dstfile], stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)
    ret = rm.wait()

    if logger:
        logger.debug(rm.stdout.read())

    return ret


def backgroundcopy(srcfile, dstfile, stager, logger=None):
    del stager, logger # unused args

    cp = subprocess.Popen(['cp', srcfile, dstfile], stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)

    return cp, cp.pid


def copystate(proc):

    return proc.poll()


def copystdout(proc):

    return proc.stdout.read().decode()


if __name__=='__main__':
    import sys
    import cs.StorageBackends.storagetester as storagetester
    storagetester.execute(sys.modules[__name__])
