# Backend storage module used by DDMMigrated_Test
import os.path

def start_cache(validity_s, logger):
    del validity_s, logger
    #noop


LISTING = {
'/eos/dummy/base/data_test/physics_test/00751981/data_test.00751981.physics_test.daq.RAW':
    # test_not_merged_simple
    { 'data_test.00751981.physics_test.daq.RAW._lb0001._SFO-0._0001.data': True
    # test_not_merged_simple_not_migrated
    , 'data_test.00751981.physics_test.daq.RAW._lb0001._SFO-0._0002.data': False
    # test_not_merged_simple_not_exist: _lb0001..._0003
    # test_not_merged_with_ts
    , 'data_test.00751981.physics_test.daq.RAW._lb0002._SFO-0._0001.data': False
    , 'data_test.00751981.physics_test.daq.RAW._lb0002._SFO-0._0001.data_20220507141700100': False
    , 'data_test.00751981.physics_test.daq.RAW._lb0002._SFO-0._0001.data_20220507141700201': True
    # test_not_merged_with_ts_wrong_migrated: the migrated file is not the most recent
    , 'data_test.00751981.physics_test.daq.RAW._lb0004._SFO-0._0001.data_20220507141700100': True
    , 'data_test.00751981.physics_test.daq.RAW._lb0004._SFO-0._0001.data_20220507141700201': False
    }
, '/eos/dummy/base/data_test/debug_all/00751981/data_test.00751981.debug_all.daq.RAW':
    # test_not_merged_debug
    { 'data_test.00751981.debug_crash.daq.RAW._lb0001._SFO-0._0001.data': True
    # test_not_merged_debug_with_ts: check that different names end up in the same dir (and dataset)
    , 'data_test.00751981.debug_wrongdata.daq.RAW._lb0002._SFO-0._0001.data': False
    , 'data_test.00751981.debug_wrongdata.daq.RAW._lb0002._SFO-0._0001.data_20220507141700100': False
    , 'data_test.00751981.debug_wrongdata.daq.RAW._lb0002._SFO-0._0001.data_20220507141700201': True
    }
, '/eos/dummy/base/data_test/physics_merged/00751981/data_test.00751981.physics_merged.merge.RAW':
    # test_merged_simple
    { 'data_test.00751981.physics_merged.merge.RAW._lb0010._SFO-ALL._0001.0': True
    # test_merged_simple_not_migrated
    , 'data_test.00751981.physics_merged.merge.RAW._lb0011._SFO-ALL._0001.0': False
    # test_merged_not_exist: _lb0012
    # test_merged_with_attemptnr
    , 'data_test.00751981.physics_merged.merge.RAW._lb0013._SFO-ALL._0001.0': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0013._SFO-ALL._0001.1': True
    # test_merged_with_attemptnr_wrong_migrated
    , 'data_test.00751981.physics_merged.merge.RAW._lb0014._SFO-ALL._0001.0': True
    , 'data_test.00751981.physics_merged.merge.RAW._lb0014._SFO-ALL._0001.1': False
    # test_merged_with_attemptnr_with_timestamp
    , 'data_test.00751981.physics_merged.merge.RAW._lb0015._SFO-ALL._0001.0': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0015._SFO-ALL._0001.0_20220507141700100': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0015._SFO-ALL._0001.0_20220507141700201': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0015._SFO-ALL._0001.1': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0015._SFO-ALL._0001.1_20220507141700100': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0015._SFO-ALL._0001.1_20220507141700201': True
    # test_merged_with_attemptnr_with_timestamp_wrong_attemptnr_migrated
    , 'data_test.00751981.physics_merged.merge.RAW._lb0016._SFO-ALL._0001.0': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0016._SFO-ALL._0001.0_20220507141700100': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0016._SFO-ALL._0001.0_20220507141700201': True
    , 'data_test.00751981.physics_merged.merge.RAW._lb0016._SFO-ALL._0001.1': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0016._SFO-ALL._0001.1_20220507141700100': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0016._SFO-ALL._0001.1_20220507141700201': False
    # test_merged_with_attemptnr_with_timestamp_wrong_timestamp_migrated
    , 'data_test.00751981.physics_merged.merge.RAW._lb0017._SFO-ALL._0001.0': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0017._SFO-ALL._0001.0_20220507141700100': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0017._SFO-ALL._0001.0_20220507141700201': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0017._SFO-ALL._0001.1': True
    , 'data_test.00751981.physics_merged.merge.RAW._lb0017._SFO-ALL._0001.1_20220507141700100': False
    , 'data_test.00751981.physics_merged.merge.RAW._lb0017._SFO-ALL._0001.1_20220507141700201': False

    }
}


def migrated(filename, stager, logger=None):
    del stager, logger
    (d, f) = os.path.split(filename)
    return LISTING.get(d, {}).get(f, False)


def listdir(directory, stager, logger=None, dir_cache=None):
    # returns (success, [list of filenames with no path])
    del stager, logger, dir_cache
    if directory not in LISTING:
        return (False, [])
    else:
        return (True, list(LISTING[directory]))
