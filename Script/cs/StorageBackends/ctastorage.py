import subprocess
import threading
from time import time
import sys
from cs.Tools.utils import thread_id_string

class DirCache(threading.Thread):

    def __init__(self, validity_s, exitevent, logger=None):
        threading.Thread.__init__(self, name="CTADirCacheThread")
        self.cache = {}
        self.lock = threading.Lock()
        self.validity_s = validity_s
        self.exitevent = exitevent
        self.granularity_s = 10
        self.logger = logger

    def run(self):
        if self.logger: self.logger.info(thread_id_string())
        while not self.exitevent.is_set():
            now = time()
            to_delete = []
            with self.lock:
                for dirname in self.cache:
                    last_update_ts = self.cache[dirname][0]
                    if (now - last_update_ts) > self.validity_s:
                        to_delete.append(dirname)
                for dirname in to_delete:
                    if self.logger: self.logger.debug('deleting cache entry: %s', dirname)
                    del self.cache[dirname]

            self.exitevent.wait(self.granularity_s)

        if self.logger: self.logger.info('ctastorage dircache stopping')


    def listdir(self, directory, stager):
        del stager
        if self.logger: self.logger.debug('checking cache for directory: %s', directory)
        with self.lock:
            if directory in self.cache:
                return self.cache[directory][1]
            else:
                return None

    def feed(self, dirname, dir_contents):
        if self.logger: self.logger.debug('feeding cache for directory: %s', dirname)
        with self.lock:
            self.cache[dirname] = (time(), dir_contents)


def start_cache(validity_s, logger, exitevent):
    cache = DirCache(validity_s, exitevent, logger)
    cache.start()
    return cache


def migrated(filename, stager, logger=None):
    cmd = [ 'xrdfs'
          , 'root://{}'.format(stager)
          , 'stat'
          , '-q'
          , 'BackUpExists'
          , filename
          ]
    sp = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = sp.wait()
    output = sp.stdout.read().decode()
    if logger: logger.debug(output)
    return (ret == 0)


def listdir(directory, stager, logger=None, dir_cache=None):
    if dir_cache is not None:
        cache_result = dir_cache.listdir(directory, stager)
        if cache_result is not None:
            if logger: logger.debug("cache hit for directory: %s", directory)
            return (True, cache_result)
        else:
            if logger: logger.debug("cache miss for directory: %s", directory)

    cmd = [ 'xrdfs'
          , 'root://{}'.format(stager)
          , 'ls'
          , directory
          ]

    sp = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    try:
        outs, _ = sp.communicate(timeout=60)
    except subprocess.TimeoutExpired:
        if logger: logger.error("subprocess timed out: killing it ({})".format(' '.join(cmd)))
        sp.kill()
        outs, _ = sp.communicate()

    ret = sp.returncode
    output = outs.decode()
    if logger: logger.debug(output)

    # Remove the absolute path from the output to return only the file names
    # (backward compatibility with retired castorstorage)
    if ret == 0:
        dir_contents = [f.split('/')[-1] for f in output.split('\n') if f]
        if dir_cache is not None:
            if logger: logger.debug("cache feed for directory: %s", directory)
            dir_cache.feed(directory, dir_contents)
        return (True, dir_contents)
    else:
        return (False, [])


if __name__ == '__main__':
    # import sys
    if len(sys.argv) < 4 or (sys.argv[1] != 'mig' and sys.argv[1] != 'ls'):
        print('usage: {} {{mig|ls}} stager file'.format(sys.argv[1]))
        sys.exit(1)
    mcmd = sys.argv[1]
    mstager = sys.argv[2]
    mpath = sys.argv[3]

    if mcmd == 'mig':
        if migrated(mpath, mstager, None):
            print('File {} exists on tape'.format(mpath))
        else:
            print('File {} does not exist on tape'.format(mpath))
    elif mcmd == 'ls':
        print(listdir(mpath, mstager))
