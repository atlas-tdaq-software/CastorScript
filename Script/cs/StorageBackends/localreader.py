"""
Do not transfer files. Just read them.
Used for testing SFO hardware without exporting files.
"""
import subprocess
import sys

# if this file runs as a script instead of as a module.
if __name__=='__main__':
    from os.path import dirname, abspath, join

    # add /../../CastorScript/Script to path so imports keeps working
    SCRIPT_DIR = abspath(join(dirname(__file__), '..','..'))
    sys.path.append(SCRIPT_DIR)

## Global object storing configuration parameters ##
## copy_extra_params --> extra parameters appended to copy operations
config_dict = {}

def init(d):

    global config_dict
    config_dict = d


def sizechecksum(filename, stager, logger=None):
    del stager

    # filename is the complete destination filename: configuration must specify
    # input directory as CopyDir

    if logger:
        logger.debug('computing checksum for: %s', filename)

    # SIZE
    # Query for generalized stats
    cmd=['ls', '-l', filename]

    lsl = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = lsl.wait()
    out = lsl.stdout.read().decode()

    # Find and get the size, log exception if any
    size = None
    if not ret:
        try:
            size = int(out.split()[4])
        except (KeyError,AttributeError,ValueError):
            if logger:
                logger.warning('error parsing size for %s, command: %s, output: %s'
                        , filename, cmd, out, exc_info=True)
    else:
        if logger:
            logger.warning('size not found for %s: %s', filename, out)

    # CHECKSUM
    # Query for checksum
    cmd=['xrdadler32', filename]

    xrdad = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    ret = xrdad.wait()
    out = xrdad.stdout.read().decode()

    # Get Checksum, log exception if any
    checksum = None
    if not ret:
        try:
            checksum = out.split()[0]
        except (KeyError, AttributeError, ValueError, IndexError):
            if logger:
                logger.warning('error parsing checksum for %s, command: %s, output: %s'
                        , filename, cmd, out, exc_info=True)
    else:
        if logger:
            logger.warning('checksum not found for %s: %s', filename, out)

    return size,checksum


def remove(dstfile, stager, logger=None):
    del dstfile, stager, logger # unused args
    return 0


def backgroundcopy(srcfile, dstfile, stager, logger=None):
    del logger, stager, dstfile #unused args

    read_script = './read_script.sh'
    if 'READ_SCRIPT' in config_dict:
        read_script = config_dict['READ_SCRIPT']

    read_process = subprocess.Popen([read_script, srcfile],
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    return read_process, read_process.pid


def mkdir(directory, stager, logger=None):
    del directory, stager, logger #unused args
    return 0


def migrated(filename, stager, logger=None):
    del filename, stager, logger # unused args
    return False


def copystate(proc):
    return proc.poll()


def copystdout(proc):
    return proc.stdout.read().decode()


if __name__=='__main__':
    import cs.StorageBackends.storagetester as storagetester
    storagetester.execute(sys.modules[__name__])
