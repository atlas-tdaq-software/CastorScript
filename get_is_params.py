import sys
from os.path import dirname, abspath, join

# add CastorScript/Script to path so import works
SCRIPT_DIR = abspath(join(dirname(__file__), 'Script'))
sys.path.append(SCRIPT_DIR)

# import used tools from castorscript
import cs.Tools.LogConfig as LogConfig
import cs.Tools.ConfigParser as ConfigParser

if len(sys.argv) != 2:
    sys.exit(1)

try:
    config = ConfigParser.ConfigHolder(sys.argv[1])
except Exception as ex:
    #print('Configuration file parsing failed because:', ex)
    sys.exit(2)

if not config.ISEnabled:
    sys.exit(3)

is_object_name = LogConfig.make_is_data_identifier(config.ISServer,
        LogConfig.make_tdaq_app_name(config))
print(f'{config.ISPartition} {is_object_name}')
