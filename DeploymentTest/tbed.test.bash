#!/usr/bin/env bash

DEFAULT_CONFIG=./tbed_noop.cfg

function usage {
  echo "usage: $EXECNAME [path to config file; default: $DEFAULT_CONFIG]"
}

EXECNAME=`basename $0`
CS_CONFIG_FILE=$DEFAULT_CONFIG
TESTHOST=localhost

if [ x"$1" != x ]; then
  CS_CONFIG_FILE=$1
  shift
fi
if [ x"$1" != x ]; then
  echo "error: too many arguments"
  usage
  exit 1
fi

cd `dirname $0`

running_cs=`ps -ef | grep CastorScript | grep -v -c grep`

if [ "$running_cs" != "0" ]; then
  echo "error: there seems to be some CastorScript running on $TESTHOST"
  exit 2
else
  echo "OK: no running CastorScript instances on $TESTHOST"
fi

SOURCE_DIR=`pwd`/..
REMOTE_DIR=$(sed -rn "s/^CopyDir: '(.*)'/\1/p" $CS_CONFIG_FILE)
BASEFILENAME="dummy"

COPY_ENABLED=True
TMP_CE=$(sed -rn "s/^CopyEnabled: (.*)/\1/p" $CS_CONFIG_FILE)
if [[ "x$TMP_CE" != "x" ]]; then
	COPY_ENABLED=$TMP_CE
fi

RECURSIVE_SOURCE=True
TMP_RSF=$(sed -rn "s/^RecursiveSourceFolder: (.*)/\1/p" $CS_CONFIG_FILE)
if [[ "x$TMP_RSF" != "x" ]]; then
	RECURSIVE_SOURCE=$TMP_RSF
fi

TMP_DIR=/tmp/castorscripttest
echo "Cleaning tmp directory: $TMP_DIR"
rm -rf $TMP_DIR
mkdir -p $TMP_DIR/logs

echo "Creating dummy data files"
for i in `seq 0 7`; do
  dd if=/dev/zero of=$TMP_DIR/${BASEFILENAME}$i.data bs=1k count=1 &>/dev/null
  if [ $? -ne 0 ]; then
    echo "error creating file: $TMP_DIR/${BASEFILENAME}$i.data"
  fi
done
mkdir -p $TMP_DIR/subdir1/subdir2
dd if=/dev/zero of=$TMP_DIR/subdir1/${BASEFILENAME}8.data bs=1k count=1 &>/dev/null
if [ $? -ne 0 ]; then
  echo "error creating file: $TMP_DIR/${BASEFILENAME}8.data"
fi
dd if=/dev/zero of=$TMP_DIR/subdir1/subdir2/${BASEFILENAME}9.data bs=1k count=1 &>/dev/null
if [ $? -ne 0 ]; then
  echo "error creating file: $TMP_DIR/${BASEFILENAME}9.data"
fi

echo "Starting CastorScript with config file: $CS_CONFIG_FILE"
if ! [ -v TDAQ_LCG_RELEASE ]; then
    source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-01
fi
python -u $SOURCE_DIR/Script/CastorScript.py $CS_CONFIG_FILE &>$TMP_DIR/logs/stdouterr &

echo "Watching files to transfer"
if [[ $RECURSIVE_SOURCE == True ]]; then
  LISTING_COMMAND="find $TMP_DIR -name ${BASEFILENAME}* | sort"
else
  LISTING_COMMAND="find $TMP_DIR -maxdepth 1 -name ${BASEFILENAME}* | sort"
fi

echo "TERM=$TERM"

LISTING_OUTPUT=`eval $LISTING_COMMAND`
if [ x$TERM != xdumb ]; then
  NL=`echo "$LISTING_OUTPUT" | wc -l`
  for i in `seq 0 $NL`; do echo; done
  for i in `seq 0 $NL`; do tput cuu1; done
fi

TIMEOUT=20

while [ "x$LISTING_OUTPUT" != x ]; do
  if [ $TIMEOUT -eq 0 ];then
    echo "timeout"
    exit 4
  fi
  if [ x$TERM != xdumb ]; then
    echo "$LISTING_OUTPUT"
  fi

  sleep 1

  if [ x$TERM != xdumb ]; then
    for i in `seq 0 $NL`; do tput cuu1; done
    NCOLS=`tput cols`
    for i in `seq 0 $NL`; do
      for ((i=0; i<NCOLS; i++));do printf " "; done;
      echo
    done
    for i in `seq 0 $((NL-1))`; do tput cuu1; done
  fi

  # Check that the CastorScript instance is still running
  NB_RUNNING_PROCESSES=`ps -ef | grep "CastorScript.py $CS_CONFIG_FILE" | grep -v grep | wc -l`
  if [ $NB_RUNNING_PROCESSES -ne 1 ]; then
    echo "number of running CastorScript: $NB_RUNNING_PROCESSES =! 1 => exiting"
    exit 3
  fi

  LISTING_OUTPUT=`eval $LISTING_COMMAND`
  NL=`echo "$LISTING_OUTPUT" | wc -l`
  ((TIMEOUT-=1))
done

echo "Stopping CastorScript"
$SOURCE_DIR/ProductionTools/installed/castor.signal 12

NB_ERR=$(grep -c -e WARNING -e ERROR -r $TMP_DIR/logs | awk -F: 'BEGIN{sum=0} //{sum += $2} END{print sum}')
echo "number of errors and warnings in the log files: $NB_ERR"

if [ $NB_ERR -eq 0 ]; then
  echo "deleting tmp dir (including logs)"
  rm -rf $TMP_DIR
else
  echo "Check logs: grep -e WARNING -e ERROR -r $TMP_DIR/logs"
  echo "Don't forget to: rm -rf $TMP_DIR"
fi
