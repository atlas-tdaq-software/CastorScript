#!/usr/bin/env bash

# This script is called by p1.test.user.bash
# It runs as atlascdr on the test host.

if [ x"$1" = x ]; then
  echo "usage: $0 {CastorScript config file}"
  exit 1
fi

cd `dirname $0`
CS_CONFIG_FILE="$1"

SOURCE_DIR=`pwd`/..
REMOTE_DIR=$(sed -rn "s/^CopyDir: '(.*)'/\1/p" $CS_CONFIG_FILE)
BASEFILENAME="dummy"
EOSINSTANCE=$(sed -rn "s/^EOSInstance: '(.*)'/\1/p" $CS_CONFIG_FILE)

COPY_ENABLED=True
TMP_CE=$(sed -rn "s/^CopyEnabled: (.*)/\1/p" $1)
if [[ "x$TMP_CE" != "x" ]]; then
	COPY_ENABLED=$TMP_CE
fi

RECURSIVE_SOURCE=True
TMP_RSF=$(sed -rn "s/^RecursiveSourceFolder: (.*)/\1/p" $1)
if [[ "x$TMP_RSF" != "x" ]]; then
	RECURSIVE_SOURCE=$TMP_RSF
fi

KEYTAB_FILE="/daq_area/castor/`hostname -s`/atlascdr/atlascdr.keytab"
kinit -kt $KEYTAB_FILE atlascdr

if [[ $COPY_ENABLED == True && $REMOTE_DIR == /eos/* ]]; then
  echo "Cleaning destination directory: $REMOTE_DIR"
  for i in `seq 0 9`; do
    xrdfs $EOSINSTANCE rm $REMOTE_DIR/${BASEFILENAME}$i.data &> /dev/null
  done
  xrdfs $EOSINSTANCE rm $REMOTE_DIR/subdir1/${BASEFILENAME}8.data &> /dev/null
  xrdfs $EOSINSTANCE rm $REMOTE_DIR/subdir1/subdir2/${BASEFILENAME}9.data &> /dev/null
  xrdfs $EOSINSTANCE rmdir $REMOTE_DIR/subdir1/subdir2 &> /dev/null
  xrdfs $EOSINSTANCE rmdir $REMOTE_DIR/subdir1 &> /dev/null
fi

TMP_DIR=/tmp/atlascdr
echo "Cleaning tmp directory: $TMP_DIR"
rm -rf $TMP_DIR
mkdir -p $TMP_DIR/logs

echo "Creating dummy data files"
for i in `seq 0 7`; do
  dd if=/dev/zero of=$TMP_DIR/${BASEFILENAME}$i.data bs=1k count=1 &>/dev/null
  if [ $? -ne 0 ]; then
    echo "error creating file: $TMP_DIR/${BASEFILENAME}$i.data"
  fi
done
mkdir -p $TMP_DIR/subdir1/subdir2
dd if=/dev/zero of=$TMP_DIR/subdir1/${BASEFILENAME}8.data bs=1k count=1 &>/dev/null
if [ $? -ne 0 ]; then
  echo "error creating file: $TMP_DIR/${BASEFILENAME}8.data"
fi
dd if=/dev/zero of=$TMP_DIR/subdir1/subdir2/${BASEFILENAME}9.data bs=1k count=1 &>/dev/null
if [ $? -ne 0 ]; then
  echo "error creating file: $TMP_DIR/${BASEFILENAME}9.data"
fi

echo "Starting CastorScript with config file: $CS_CONFIG_FILE"
source $SOURCE_DIR/script_setup.sh
python -u $SOURCE_DIR/Script/CastorScript.py $CS_CONFIG_FILE &>$TMP_DIR/logs/stdouterr &

# through ssh and su, TERM is not set and tput is lost
export TERM=xterm

echo "Watching files to transfer"
if [[ $RECURSIVE_SOURCE == True ]]; then
  LISTING_COMMAND="find $TMP_DIR -name ${BASEFILENAME}* | sort"
else
  LISTING_COMMAND="find $TMP_DIR -maxdepth 1 -name ${BASEFILENAME}* | sort"
fi

LISTING_OUTPUT=`eval $LISTING_COMMAND`
NL=`echo "$LISTING_OUTPUT" | wc -l`
for i in `seq 0 $NL`; do echo; done
for i in `seq 0 $NL`; do tput cuu1; done

while [ "x$LISTING_OUTPUT" != x ]; do
  echo "$LISTING_OUTPUT"
  sleep 1

  for i in `seq 0 $NL`; do tput cuu1; done
  NCOLS=`tput cols`
  for i in `seq 0 $NL`; do
    for ((i=0; i<NCOLS; i++));do printf " "; done;
    echo
  done
  for i in `seq 0 $((NL-1))`; do tput cuu1; done

  # Check that the CastorScript instance is still running
  NB_RUNNING_PROCESSES=`ps -ef | grep "CastorScript.py $CS_CONFIG_FILE" | grep -v grep | wc -l`
  if [ $NB_RUNNING_PROCESSES -ne 1 ]; then
    echo "number of running CastorScript: $NB_RUNNING_PROCESSES, =!1 => exiting"
    exit
  fi

  LISTING_OUTPUT=`eval $LISTING_COMMAND`
  NL=`echo "$LISTING_OUTPUT" | wc -l`
done

echo "Stopping CastorScript"
/sw/castor/tools/castor.signal 12

if [[ $COPY_ENABLED == True && $REMOTE_DIR == /eos/* ]]; then
  echo "Contents of remote directory:"
  xrdfs $EOSINSTANCE ls -l -R $REMOTE_DIR

  echo "Cleaning destination directory: $REMOTE_DIR"
  for i in `seq 0 9`; do
    xrdfs $EOSINSTANCE rm $REMOTE_DIR/${BASEFILENAME}$i.data &> /dev/null
  done
  xrdfs $EOSINSTANCE rm $REMOTE_DIR/subdir1/${BASEFILENAME}8.data &> /dev/null
  xrdfs $EOSINSTANCE rm $REMOTE_DIR/subdir1/subdir2/${BASEFILENAME}9.data &> /dev/null
  xrdfs $EOSINSTANCE rmdir $REMOTE_DIR/subdir1/subdir2 &> /dev/null
  xrdfs $EOSINSTANCE rmdir $REMOTE_DIR/subdir1 &> /dev/null
fi

NB_ERR=$(grep -c -e WARNING -e ERROR -r $TMP_DIR/logs | awk -F: 'BEGIN{sum=0} //{sum += $2} END{print sum}')
echo "number of errors and warnings in the log files: $NB_ERR"

if [ $NB_ERR -eq 0 ]; then
  echo "deleting tmp dir (including logs)"
  rm -rf $TMP_DIR
else
  echo "Check logs: grep -e WARNING -e ERROR -r $TMP_DIR/logs"
  echo "Don't forget to: rm -rf $TMP_DIR"
fi
