#!/usr/bin/env bash

function usage {
  echo "usage: $EXECNAME [--isers | --mig] {test host}"
  echo ""
  echo "    --isers       enable IS and ERS publication features"
  echo "                  (requires running 'castorscript' partition)"
  echo "    --mig         enable migration check before deletion"
  echo "                  (uses /eos/ctaatlaspps/daqtest)"
  echo "    --localreader local file reader only, no transfer"
  echo "                  --mig is incompatible with this option"
}

EXECNAME=`basename $0`
TESTHOST=""
ISERS_ENABLED="false"
MIG_ENABLED="false"
LOCALREADER="false"

while [[ $# -gt 0 ]]; do
  case $1 in
    --isers)
    ISERS_ENABLED="true"
    shift
    ;;
    --mig)
    MIG_ENABLED="true"
    shift
    ;;
    --localreader)
    LOCALREADER="true"
    shift
    ;;
    -*) # unknown option
    usage
    exit 1
    ;;
    *)
    TESTHOST=$1
    shift
    ;;
  esac
done

if [ "x$TESTHOST" = x ]; then
  usage
  exit 1
fi

if [ $LOCALREADER = "true" -a $ISERS_ENABLED = "true"  ]; then
  CS_CONFIG_FILE=test_config_localreader_is_ers.cfg
elif [ $LOCALREADER = "true" -a $ISERS_ENABLED = "false"  ]; then
  CS_CONFIG_FILE=test_config_localreader.cfg
elif [ $ISERS_ENABLED = "true" -a $MIG_ENABLED = "true" ]; then
  CS_CONFIG_FILE=test_config_is_ers_mig.cfg
elif [ $ISERS_ENABLED = "true" -a $MIG_ENABLED = "false" ]; then
  CS_CONFIG_FILE=test_config_is_ers.cfg
elif [ $ISERS_ENABLED = "false" -a $MIG_ENABLED = "true" ]; then
  CS_CONFIG_FILE=test_config_mig.cfg
else
  CS_CONFIG_FILE=test_config.cfg
fi

cd `dirname $0`

running_cs=`ssh $TESTHOST "ps -ef | grep CastorScript | grep -v -c grep"`

if [ "$running_cs" != "0" ]; then
  echo "error: there seems to be some CastorScript running on $TESTHOST"
  exit 2
else
  echo "OK: no running CastorScript instances on $TESTHOST"
fi

KEYTAB_FILE="/daq_area/castor/$TESTHOST/atlascdr/atlascdr.keytab"
if ! ssh $TESTHOST "sudo -u atlascdr -i cat $KEYTAB_FILE &>/dev/null"; then
  echo "error: keytab file not readble by atlascdr ($KEYTAB_FILE)"
  exit 3
else
  echo "OK: readable keytab file: $KEYTAB_FILE"
fi

if ! sed -r "s:/daq_area/castor/[a-zA-Z0-9-]+/atlascdr/atlascdr.keytab:/daq_area/castor/$TESTHOST/atlascdr/atlascdr.keytab:" $CS_CONFIG_FILE -i; then
  echo "error: cannot modify CastorScript config file: $CS_CONFIG_FILE"
  exit 4
else
  echo "OK: CastorScript config file modified: $CS_CONFIG_FILE "
fi

ssh $TESTHOST sudo -u atlascdr -i $PWD/p1.test.atlascdr.bash $PWD/$CS_CONFIG_FILE
