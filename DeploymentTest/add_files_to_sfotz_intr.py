import coral_auth
import oracledb
oracledb.init_oracle_client()

user, pwd, dbn = coral_auth.get_connection_parameters_from_connection_string(
        'oracle://int8r/ATLAS_SFO_T0')
orcl = oracledb.connect(user=user, password=pwd, dsn=dbn)
curs = orcl.cursor()

try:
    # "for update" locks the row until commit
    curs.execute("select seqindex from SFO_TZ_SEQINDEX where seqname='FILE_SEQNUM' for update")
    file_seqnum = curs.fetchone()[0]

    keys = {}
    keys['sfoid'] = 'fakesfo'
    keys['runnr'] = 1664
    keys['lumiblocknr'] = 1
    keys['streamtype'] = 'test'
    keys['stream'] = 'teststream'
    keys['filehealth'] = 'SANE'

    sql = "insert into SFO_TZ_FILE \
            (sfopfn, filenr, sfoid, runnr, lumiblocknr, streamtype, \
            stream, filehealth, filesize, checksum, file_seqnum) \
            values \
            (:sfopfn, :filenr, :sfoid, :runnr, :lumiblocknr, :streamtype, \
            :stream, :filehealth, :filesize, :checksum, :file_seqnum)"

    # files = [
    #         '/tmp/atlascdr/dummy1.data',
    #         '/tmp/atlascdr/dummy2.data',
    #         '/tmp/atlascdr/dummy3.data',
    #         '/tmp/atlascdr/dummy4.data',
    #         ]

    # primary key is (runnr, lbnr, streamtype, stream, sfoid, filenr)
    # for index, sfopfn in enumerate(files):
    for index in range(5, 10):
        keys['sfopfn'] = '/tmp/atlascdr/dummy{}.data'.format(index)
        keys['filenr'] = index
        keys['filesize'] = 1024
        keys['checksum'] = '04000001'
        keys['file_seqnum'] = file_seqnum
        file_seqnum += 1
        curs.execute(sql, **keys)

    curs.execute(
            "update SFO_TZ_SEQINDEX set seqindex=:file_seqnum where seqname='FILE_SEQNUM'"
            , file_seqnum=file_seqnum)

    curs.connection.commit()
except:
    curs.connection.rollback()
    raise
