#!/bin/bash
SHORTHOSTNAME=`hostname -s`

SWDIR="/sw/castor"
CONFDIR="/daq_area/castor/${SHORTHOSTNAME}/${USER}"
CFG_EXTENSION="cfg"

if [ x"$1" = xTEST ]; then
	SWDIR="/atlas-home/1/vandelli/CastorScript"
	CFG_EXTENSION="testcfg"
	echo "running test configurations: swdir=$SWDIR, cfg_ext=$CFG_EXTENSION"
fi

# Prevents "*.cfg" to be returned if no files are present
shopt -s nullglob

for CFG in $CONFDIR/*.$CFG_EXTENSION; do

	#get is parameters from config file
	IS_PARTITION_NAME=$(grep 'ISPartition' $CFG | awk '{print $2}' | tr -d "'" | tr -d "\"")
	IS_SERVER=$(grep 'ISServer: ' $CFG | awk '{print $2}' | tr -d "'" | tr -d "\"")

	#check if a python process running the castorscript, with this config is running
	ps -ef | grep python | grep CastorScript.py | grep "$CFG" > /dev/null 2>&1

	if [ $? -eq 1 ]; then

		#get logdir from config file (find LogDir: in CFG | pipe 2nd part | del all ' & " | del last char if == / )
		logdir=$(grep 'LogDir:' $CFG | awk '{print $2}' | tr -d "'" | tr -d "\"" | sed "s/\/$//")

		# If the CastorScript crashed, especially on uncaught exceptions, this watchdog
		# restarts it and we won't notice. Uncaught exception messages end up in
		# stdouterr: mail it to Fabrice if the file has non-zero length.
		# But first filter out unavoidable ERS messages which are not interesting for us
		# These messages appear when the partition used for ERS logging exits, before
		# the ERS thread has the time to remove the ERS handler.
		if [ -f $logdir/stdouterr ]; then
			cp $logdir/stdouterr $logdir/stdouterr.raw
			sed '/Partition ".*" does not exist/d;/CORBA system exception "TRANSIENT(1096024066=TRANSIENT_ConnectFailed)" has been raised/d' -i $logdir/stdouterr
			if [ -s $logdir/stdouterr ]; then
				echo "$CFG" | mail -s "CastorScript instance crashed" -a $logdir/stdouterr -r "atlascdr@cern.ch" wainer.vandelli@cern.ch
			fi
		fi

		# Source required environment
		if ! source $SWDIR/script_setup.sh; then
			echo "error: could not source script_setup.sh"
			exit 1
		fi

		# write to IS that this instance has an uptime of 0.0
		# if it crashes before being able to publish at least we'll see it in IS
		IS_PARAMS=$(python $SWDIR/get_is_params.py $CFG)
		if [ $? -eq 0 ]; then
			read IS_PARTITION IS_OBJECT_NAME <<<$(echo "$IS_PARAMS")
			is_write -p $IS_PARTITION -n $IS_OBJECT_NAME -t CastorScriptState -r -a uptime_seconds -v 0.0 > /dev/null 2>&1
		fi

		# Start script
		python -u $SWDIR/Script/CastorScript.py $CFG > $logdir/stdouterr 2>&1 &

		# Log messages
		date
		echo "CastorScript instance restarted: $CFG"
		echo
	fi

done
