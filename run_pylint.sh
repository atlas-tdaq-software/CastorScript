#!/bin/bash
cd `dirname $0`

if ! [ -v TDAQ_LCG_RELEASE ]; then
    source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-01
fi

/cvmfs/sft.cern.ch/lcg/views/$TDAQ_LCG_RELEASE/$CMTCONFIG/bin/pylint $(find . -name '*.py' -a -not -name config.py)
