#!/bin/bash
# how to use docker for local testing
# install docker

# Logout and login again and run.
#================================

# run this command that does the following:
# 1 docker!
# 2 Create a new container,
# 3 with an interactive shell,
# 4 mount this dir (hopefully the CastorScript dir!) to the container,
# 5 mount other important storage locations (AFS, cvmfs etc),
#     AFS no longer needed. Was: -v /afs:/afs:ro
# 6 remove the container after shutdown,
# 7 and run the centos7 image,
# 8 when started, run the run_unit_tests script in bash
# 1      2   3                4                       5                                          6         7                                                           8
# docker run -it --privileged -v `pwd`:`pwd` -w `pwd` -v /cvmfs:/cvmfs:shared,ro --rm=true gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9 bash ./run_unit_tests.sh
# or without running the script:
docker run -it --privileged -v `pwd`:`pwd` -w `pwd` -v /cvmfs:/cvmfs:shared,ro --rm=true gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9 bash
