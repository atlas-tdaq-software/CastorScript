This is the software that transfers file from ATLAS online infrastructure to
offline storage. The name, CastorScript, has historical reasons: it used to copy
files directly to the CASTOR system (it does not anymore).

# Contents

- Configs: reference, template and test configs
- DeploymentTest: deployment test
- Script: the actual code
- ProductionTools: set of Bash and Python scripts used on the production system
  to ease maintenance tasks
- pylintrc: configuration of pylint (used by CI)
- readme: this file (are you even following?)
- run_docker_container.sh: script to start a docker container in CWD for testing (must be run in CastorScript folder)
- run_pylint.sh: script to run pylint on all files in Script
- run_unit_tests.sh: script to search and run Python unit tests
- script_setup.sh: script used in P1 to setup the running environment
- watchdog.sh: script called by sysadmin's cron task that starts and maintains
  CastorScript instance up and running. Also handles log turn over.

# Semi-Automated Tests In P1

Choose a "CastorScript compatible", unused computer in P1 (unused calib
machines are a good choice).

The, in P1, from the code source directory

    ./DeploymentTest/p1.test.user.bash pc-tdq-calib-21

The output should be self-explanatory.

There are a number of options to this test script to test specific features
(online documentation is available):

- `--isers`: enable IS and ERS publication features; requires running a
  partition named 'castorscript';
- `--mig`: enable migration check before deletion; uses
  `/eos/ctaatlaspps/daqtest` as CTA target ;
- `--localreader`: no transfer takes place, files are only read locally.

The last two options are mutually exclusive.

# Installing a New Version in P1

    # Tag the commit you want to install in git
    ssh atlasgw
    # git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-software/CastorScript.git
    cd CastorScript
    git pull
    ./point1.deployment.sh CastorScript-00-00-00
    # Let it flow (and check for errors)
