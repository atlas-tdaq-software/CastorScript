#!/bin/bash
TDAQ_RELEASE=tdaq-11-02-01

echo "setting up environment for centos, tdaq release: $TDAQ_RELEASE"
if ! source /sw/tdaq/setup/setup_$TDAQ_RELEASE.sh; then
	echo "error: could not source TDAQ release $TDAQ_RELEASE"
	return 1
fi

# take the latest version of XROOTD available for this TDAQ release
XROOTD_VERSION=`ls $LCG_INST_PATH/$TDAQ_LCG_RELEASE/xrootd/ | \
		awk -F. 'BEGIN{max=0; dir="NULL"} {ver = $1*10000 + $2 * 100 + $3;\
		if (ver > max) {max=ver; dir=$1"."$2"."$3}} END{print dir}'`
if [ "x$XROOTD_VERSION" = "xNULL" ]; then
	echo "error: could not find xrootd version"
	return 1
fi
echo "found xrootd version: $XROOTD_VERSION"

# Add path for xrdcp binary matching this release
export PATH="$LCG_INST_PATH/$TDAQ_LCG_RELEASE/xrootd/$XROOTD_VERSION/$CMTCONFIG/bin/:$PATH"
