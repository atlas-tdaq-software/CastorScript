#!/usr/bin/env python
# run as atlascdr on pc-tdq-grape
import sys
import argparse
import os
import subprocess
import time
from socket import gethostname
from getpass import getuser
from oracle_run_status import oracle_run_status
from oracle_create_missing_and_close_opened import oracle_create_missing_and_close_opened
from oracle_check_closed_ondisk import oracle_check_closed_ondisk
from oracle_check_closed_transferred import oracle_check_closed_transferred

# FILESTATE: [OPENED, CLOSED, DELETE], TRANSFERSTATE: [ONDISK, TRANSFERRED]
# Files can be in 5 different states:
# 1. Not in DB
# 2. (opened, ondisk): in this case sfopfn usually ends with .writing
#    (except between the final file renaming and the DB update)
# 3. (closed, ondisk): at this stage CastorScript can transfer them since
#    sfopfn is the actual file path and name
# 4. (closed, transferred): marked as such by CS after the transfer; at this
#    stage, Tier 0 can process them
# 5. (deleted, transferred)
#
# DB failure can happen anytime. The goal of this script is to:
# 1. bring these in state 1 and 2 to 3, so that CS takes over their life cycle
# 2. bring these in state 3 to 4, so that T0 can take over
# 3. bring these in state 4 to 5
# 4. report any outlier that doesn't match this description
# After this, CS instances should be able to trigger the remaining steps and
# eventually every file, LB and the run will be "transferred".
#
# Recovery procedure:
# *AFTER THE END OF THE RUN, WITH CS INSTANCES DISABLED*
# A. State 1 and 2 are in sqlite DB in state 3: create them directly in Oracle
#    from sqlite data
# B. Do step 3 first. Step 2 creates (ondisk, transferred) files (state
#    4) that cannot be deleted (we just checked that .COPIED exists). So, to
#    to avoid, checking these files for absence, we first do step 3, then step 2.
#    Check (ondisk,transferred): if file is absent, update as in
#    Database.Deletion
# C. State 3 means that CS could not update their status after
#    transfer: check presence of .COPIED and update as in Database.Transfer
# D. Anything that does not match is logged in a file for manual processing


if not gethostname().startswith('pc-atlas-cr-30'):
    print('error: must be executed on pc-atlas-cr-30')
    sys.exit(1)

if getuser() != 'atlascdr':
    print('error: must be executed as atlascdr')
    sys.exit(1)

ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
ap.add_argument('run_number', type=int,
        help='number of the run to fix')
ap.add_argument('oradb_url',
        help='URL of the oracle DB to fix, example: oracle://int8r/ATLAS_SFO_T0')
ap.add_argument('sfos', type=int, nargs='+',
        help='list of SFO servers (as int)')
ap.add_argument('-v', '--verbose', action='store_true',
        help='enable verbosity')
ap.add_argument('-d', '--dryrun', action='store_true',
        help="do not modify oracle DB")
ap.add_argument('-o', '--outliers_file', default='outliers',
        help='name of the file to write outliers to')
ap.add_argument('-r', '--report',
        help='file-by-file report (super-verbosity)')
args = ap.parse_args()

if args.verbose:
    print('parameters:')
    print(f'  sfos: {args.sfos}')
    print(f'  run#: {args.run_number}')
    print(f'  oracle URL: {args.oradb_url}')
    print(f'  dryrun: {args.dryrun}')
    print(f'  verbose: {args.verbose}')
    print(f'  report: {args.report}')

working_dir = str(args.run_number)
try:
    os.mkdir(working_dir)
    if args.verbose: print(f'created directory: {working_dir}')
except FileExistsError:
    if args.verbose: print(f'using existing directory: {working_dir}')
os.chdir(working_dir)

oracle_run_status(args.oradb_url, args.run_number, args.sfos, args.report)

def castorscript_is_running(computer, config_file):
    ps_cmd = ['ssh', '-x', computer,
        f'ps -ef | grep CastorScript.py | grep {config_file} | grep -v grep']
    psp = subprocess.run(ps_cmd, stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT, check=True)
    return psp.stdout.decode() != ''

if args.dryrun:
    if args.verbose: print('would be stopping CastorScript instances')
else:
    if args.verbose: print('stopping CastorScript instances')
    for i in args.sfos:
        if i < 10: i = f'0{i}'
        if os.path.exists(f'/mnt/daq_area_rw/castor/pc-tdq-sfo-{i}/atlascdr/prod.cfg'):
            os.rename(f'/mnt/daq_area_rw/castor/pc-tdq-sfo-{i}/atlascdr/prod.cfg',
                    f'/mnt/daq_area_rw/castor/pc-tdq-sfo-{i}/atlascdr/prod.stopped')
            if args.verbose: print(f'disabled CS on sfo-{i}')
        if castorscript_is_running(f'pc-tdq-sfo-{i}', 'prod.cfg'):
            stop_cs_cmd = ['ssh', '-x', f'pc-tdq-sfo-{i}',
                        '/sw/castor/tools/castor.signal', '12']
            subprocess.run(stop_cs_cmd, check=True)
            if args.verbose: print(f'sent stop signal to CS on sfo-{i}: waiting for CS to stop')
            while castorscript_is_running(f'pc-tdq-sfo-{i}', 'prod.cfg'):
                time.sleep(1)
            if args.verbose: print(f'CS stopped on sfo-{i}')

for i in args.sfos:
    if i < 10: i = f'0{i}'
    if os.path.exists(f'sfo-{i}.{args.run_number}.sqlite'):
        if args.verbose:
            print(f'using existing sqlite db: sfo-{i}.{args.run_number}.sqlite')
    else:
        scp_cmd = ['scp', f'pc-tdq-sfo-{i}:/dsk1/sqlite/sfo-sqlite-{args.run_number}.db',
                f'sfo-{i}.{args.run_number}.sqlite']
        subprocess.run(scp_cmd, check=True, stdout=subprocess.PIPE)
        if args.verbose: print(f'downloaded sqlite: sfo-{i}.{args.run_number}.sqlite')

if args.verbose: print('recovery step A: bring state 1 (missing) and 2 (opened) to 3 (closed)')
for i in args.sfos:
    if i < 10: i = f'0{i}'
    sqlite_filename = f'sfo-{i}.{args.run_number}.sqlite'
    if args.verbose: print(f'processing file: {sqlite_filename}')
    oracle_create_missing_and_close_opened(args.oradb_url,
            sqlite_filename, verbose=args.verbose,
            dryrun=args.dryrun, report=args.report)

if args.verbose: print('recovery step B: bring state 4 (closed,transferred) to'
        ' 5 (deleted) if deleted')
oracle_check_closed_transferred(args.oradb_url, args.run_number, args.outliers_file,
        verbose=args.verbose, dryrun=args.dryrun, report=args.report)

if args.verbose: print('recovery step C: bring state 3 (closed,ondisk) to '
        '4 (transferred) if .COPIED file exists')
oracle_check_closed_ondisk(args.oradb_url, args.run_number, args.outliers_file,
        verbose=args.verbose, dryrun=args.dryrun, report=args.report)

if os.path.exists(args.outliers_file) and os.path.getsize(args.outliers_file) != 0:
    print(f'there are some outliers: check file "{args.outliers_file}"')
    print('CastorScript instances NOT restarted: re-enable them after having'
            ' taken care of outliers')
else:
    if args.verbose: print('no outliers')
    if args.dryrun:
        if args.verbose: print('would be starting CastorScript instances')
    else:
        if args.verbose: print('starting CastorScript instances')
        for i in args.sfos:
            if i < 10: i = f'0{i}'
            os.rename(f'/mnt/daq_area_rw/castor/pc-tdq-sfo-{i}/atlascdr/prod.stopped',
                    f'/mnt/daq_area_rw/castor/pc-tdq-sfo-{i}/atlascdr/prod.cfg')
            if args.verbose: print(f're-enabled CS on sfo-{i}')
