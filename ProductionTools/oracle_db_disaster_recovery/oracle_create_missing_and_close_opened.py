#!/usr/bin/env python
import sqlite3
import coral_auth
import argparse
import datetime
import oracledb
oracledb.init_oracle_client()


def get_next_index(orac, seqname):
    index = orac.execute("""select SEQINDEX from SFO_TZ_SEQINDEX
            where SEQNAME=:seqname for update""",
            {'seqname': seqname}).fetchone()[0]
    orac.execute("""update SFO_TZ_SEQINDEX
            set SEQINDEX=:newindex, LASTMOD=:tstamp
            where SEQNAME=:seqname""",
            {'seqname': seqname, 'newindex': index+1,
            'tstamp': datetime.datetime.now()})
    orac.connection.commit()
    return index


def sync_files(sqc, orac, verbose, dryrun, reportf):
    inserted = 0
    updated = 0

    sq_files = sqc.execute("""select LFN, FILENR, SFOID, SFOHOST, RUNNR, LUMIBLOCKNR
            , STREAMTYPE, STREAM, TRANSFERSTATE, FILESTATE, FILEHEALTH, TZSTATE
            , FILESIZE, GUID, CHECKSUM, PFN, SFOPFN, NREVENTS, ENDTIME
            , ENDTIME_EPOCH, FILE_SEQNUM from SFO_TZ_FILE""").fetchall()

    for file in sq_files:
        filedict = {
            'lfn': file[0],
            'filenr': file[1],
            'sfoid': file[2],
            'sfohost': file[3],
            'runnr': file[4],
            'lumiblocknr': file[5],
            'streamtype': file[6],
            'stream': file[7],
            'transferstate': file[8],
            'filestate': file[9],
            'filehealth': file[10],
            'tzstate': file[11],
            'filesize': file[12],
            'abcdguid': file[13],
            'checksum': file[14],
            'pfn': file[15],
            'sfopfn': file[16],
            'nrevents': file[17],
            'endtime': datetime.datetime.utcfromtimestamp(file[19]),
            'endtimeepoch': file[19],
        }
        oraf = orac.execute("select LFN, FILESTATE, TRANSFERSTATE from SFO_TZ_FILE"
                " where LFN=:lfn", {'lfn': file[0]}).fetchone()
        if not oraf:
            # empty list <=> file entry does not exist
            if not dryrun:
                index = get_next_index(orac, 'FILE_SEQNUM')
                filedict['fileseqnum'] = index
                orac.execute("""insert into SFO_TZ_FILE(LFN, FILENR, SFOID, SFOHOST
                        , RUNNR, LUMIBLOCKNR, STREAMTYPE, STREAM, TRANSFERSTATE
                        , FILESTATE, FILEHEALTH, TZSTATE, FILESIZE, GUID, CHECKSUM
                        , PFN, SFOPFN, NREVENTS, ENDTIME, ENDTIME_EPOCH
                        , FILE_SEQNUM) values (:lfn, :filenr, :sfoid, :sfohost, :runnr
                        , :lumiblocknr, :streamtype, :stream, :transferstate, :filestate
                        , :filehealth, :tzstate, :filesize, :abcdguid, :checksum, :pfn
                        , :sfopfn, :nrevents, :endtime, :endtimeepoch, :fileseqnum
                        )""", filedict)
                orac.connection.commit()
            if reportf: reportf.write(f'[file] inserted: {file[0]}\n')
            inserted += 1
        elif oraf[1] == 'OPENED' and oraf[2] == 'ONDISK':
            if not dryrun:
                orac.execute("""update SFO_TZ_FILE set FILENR=:filenr, SFOID=:sfoid
                        , SFOHOST=:sfohost, RUNNR=:runnr, LUMIBLOCKNR=:lumiblocknr
                        , STREAMTYPE=:streamtype, STREAM=:stream
                        , TRANSFERSTATE=:transferstate, FILESTATE=:filestate
                        , FILEHEALTH=:filehealth, TZSTATE=:tzstate, FILESIZE=:filesize
                        , GUID=:abcdguid, CHECKSUM=:checksum, PFN=:pfn, SFOPFN=:sfopfn
                        , NREVENTS=:nrevents, ENDTIME=:endtime
                        , ENDTIME_EPOCH=:endtimeepoch
                        where LFN=:lfn""", filedict)
                orac.connection.commit()
            if reportf: reportf.write(f'[file] updated: {file[0]}\n')
            updated += 1

    if verbose:
        if dryrun:
            print(f'[file] would have created {inserted} missing and closed {updated} opened')
        else:
            print(f'[file] created {inserted} missing and closed {updated} opened')


def sync_lbs(sqc, orac, verbose, dryrun, reportf):
    inserted = 0
    updated = 0

    sq_lbs = sqc.execute("""select SFOID, RUNNR, LUMIBLOCKNR, STREAMTYPE,
            STREAM, STATE, MAXNRSFOS
            from SFO_TZ_LUMIBLOCK""").fetchall()

    for lb in sq_lbs:
        lbdict = {
            'sfoid': lb[0],
            'runnr': lb[1],
            'lumiblocknr': lb[2],
            'streamtype': lb[3],
            'stream': lb[4],
            'state': lb[5],
            'maxnrsfos': lb[6],
        }
        oralb = orac.execute("select STATE from SFO_TZ_LUMIBLOCK"
                " where SFOID=:sfoid and RUNNR=:runnr"
                " and LUMIBLOCKNR=:lumiblocknr and STREAMTYPE=:streamtype"
                " and STREAM=:stream", {
                'sfoid': lbdict['sfoid'],
                'runnr': lbdict['runnr'],
                'lumiblocknr': lbdict['lumiblocknr'],
                'streamtype': lbdict['streamtype'],
                'stream': lbdict['stream'],
                }).fetchone()
        if not oralb:
            if not dryrun:
                index = get_next_index(orac, 'LUMIBLOCK_SEQNUM')
                lbdict['lumiblock_seqnum'] = index
                lbdict['creation_time'] = datetime.datetime.now()
                lbdict['modification_time'] = datetime.datetime.now()
                orac.execute("""insert into SFO_TZ_LUMIBLOCK(SFOID, RUNNR,
                        LUMIBLOCKNR, STREAMTYPE, STREAM, STATE, MAXNRSFOS,
                        LUMIBLOCK_SEQNUM, CREATION_TIME, MODIFICATION_TIME)
                        values (:sfoid, :runnr, :lumiblocknr, :streamtype,
                        :stream, :state, :maxnrsfos, :lumiblock_seqnum,
                        :creation_time, :modification_time)""", lbdict)
                orac.connection.commit()
            if reportf: reportf.write(f'[lb] inserted: {lbdict["sfoid"]},'
                    f'{lbdict["runnr"]},{lbdict["lumiblocknr"]},'
                    f'{lbdict["streamtype"]},{lbdict["stream"]}\n')
            inserted += 1
        elif oralb[0] != 'TRANSFERRED' and oralb[0] != lbdict['state']:
            # note: do not overwrite TRANSFERRED objects; sqlite db never contain transferred objects
            if not dryrun:
                lbdict['modification_time'] = datetime.datetime.now()
                orac.execute("""update SFO_TZ_LUMIBLOCK set STATE=:state,
                        MAXNRSFOS=:maxnrsfos, MODIFICATION_TIME=:modification_time
                        where SFOID=:sfoid and RUNNR=:runnr
                        and LUMIBLOCKNR=:lumiblocknr and STREAMTYPE=:streamtype
                        and STREAM=:stream""", lbdict)
                orac.connection.commit()
            if reportf: reportf.write(f'[lb] updated: {lbdict["sfoid"]},'
                    f'{lbdict["runnr"]},{lbdict["lumiblocknr"]},'
                    f'{lbdict["streamtype"]},{lbdict["stream"]}\n')
            updated += 1

    if verbose:
        if dryrun:
            print(f'[lb] would have created {inserted} and updated {updated}')
        else:
            print(f'[lb] created {inserted} and updated {updated}')


def sync_streams(sqc, orac, verbose, dryrun, reportf):
    inserted = 0
    updated = 0

    sq_streams = sqc.execute("""select SFOID, RUNNR, STREAMTYPE,
            STREAM, STATE, DSNAME, PROJECT, MAXNRSFOS
            from SFO_TZ_RUN""").fetchall()

    for stream in sq_streams:
        streamdict = {
            'sfoid': stream[0],
            'runnr': stream[1],
            'streamtype': stream[2],
            'stream': stream[3],
            'state': stream[4],
            'dsname': stream[5],
            'project': stream[6],
            'maxnrsfos': stream[7],
        }
        orastream = orac.execute("select STATE from SFO_TZ_RUN"
                " where SFOID=:sfoid and RUNNR=:runnr"
                " and STREAMTYPE=:streamtype and STREAM=:stream", {
                'sfoid': streamdict['sfoid'],
                'runnr': streamdict['runnr'],
                'streamtype': streamdict['streamtype'],
                'stream': streamdict['stream'],
                }).fetchone()
        if not orastream:
            if not dryrun:
                index = get_next_index(orac, 'RUN_SEQNUM')
                streamdict['run_seqnum'] = index
                streamdict['creation_time'] = datetime.datetime.now()
                streamdict['modification_time'] = datetime.datetime.now()
                orac.execute("""insert into SFO_TZ_RUN(SFOID, RUNNR,
                        STREAMTYPE, STREAM, STATE, DSNAME, PROJECT, MAXNRSFOS,
                        RUN_SEQNUM, CREATION_TIME, MODIFICATION_TIME)
                        values (:sfoid, :runnr, :streamtype,
                        :stream, :state, :dsname, :project, :maxnrsfos,
                        :run_seqnum, :creation_time, :modification_time)""",
                        streamdict)
                orac.connection.commit()
            if reportf: reportf.write(f'[stream] inserted: {streamdict["sfoid"]},'
                    f'{streamdict["runnr"]},'
                    f'{streamdict["streamtype"]},{streamdict["stream"]}\n')
            inserted += 1
        elif orastream[0] != 'TRANSFERRED' and orastream[0] != streamdict['state']:
            # note: do not overwrite TRANSFERRED objects; sqlite db never contain transferred objects
            if not dryrun:
                streamdict['modification_time'] = datetime.datetime.now()
                orac.execute("""update SFO_TZ_RUN set STATE=:state,
                        DSNAME=:dsname, PROJECT=:project,
                        MAXNRSFOS=:maxnrsfos, MODIFICATION_TIME=:modification_time
                        where SFOID=:sfoid and RUNNR=:runnr
                        and STREAMTYPE=:streamtype and STREAM=:stream""",
                        streamdict)
                orac.connection.commit()
            if reportf: reportf.write(f'[stream] updated: {streamdict["sfoid"]},'
                    f'{streamdict["runnr"]},{streamdict["streamtype"]},'
                    f'{streamdict["stream"]}\n')
            updated += 1

    if verbose:
        if dryrun:
            print(f'[stream] would have created {inserted} and updated {updated}')
        else:
            print(f'[stream] created {inserted} and updated {updated}')


def oracle_create_missing_and_close_opened(oradb_url, sqlite_file,
        verbose=False, dryrun=False, report=None):

    reportf = None
    if report: reportf = open(report, 'a')

    try:
        user, pwd, dbn = coral_auth.get_connection_parameters_from_connection_string(
                oradb_url)
        oradb = oracledb.connect(user=user, password=pwd, dsn=dbn)
        orac = oradb.cursor()
    except Exception:
        print(f'error connecing to {oradb_url}')
        raise

    sqdb = sqlite3.connect(sqlite_file)
    sqc = sqdb.cursor()

    reportf = None
    if report:
        reportf = open(report, 'a')
        reportf.write('---- step A\n')

    sync_files(sqc, orac, verbose, dryrun, reportf)
    sync_lbs(sqc, orac, verbose, dryrun, reportf)
    sync_streams(sqc, orac, verbose, dryrun, reportf)

    if report:
        reportf.write('---- end of step A\n')
        reportf.close()
    sqdb.close()
    oradb.close()


if __name__ == '__main__':
    ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument("oradb_url",
            help="URL of the oracle DB to fix, example: oracle://int8r/ATLAS_SFO_T0")
    ap.add_argument("sqlite_file",
            help="sqlite file to extract information")
    ap.add_argument("-v", "--verbose", action="store_true",
            help="enable verbosity")
    ap.add_argument("-d", "--dryrun", action="store_true",
            help="do not modify oracle DB")
    ap.add_argument("-r", "--report",
            help="file-by-file report")
    args = ap.parse_args()

    oracle_create_missing_and_close_opened(args.oradb_url, args.sqlite_file,
            verbose=args.verbose, dryrun=args.dryrun, report=args.report)
