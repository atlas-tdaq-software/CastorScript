#!/usr/bin/env python
import coral_auth
import argparse
import oracledb
oracledb.init_oracle_client()


def _stream_status_fast(orac, run_number, sfoids):
    """
    orac: oracle cursor of an open connection to a oracle db
    run_number: ID of the run to filter objects
    sfoids: list of SFO application name to filter objects, cannot be empty
    """
    if len(sfoids) == 0: raise RuntimeError('empty SFO app list')

    req = ("select count(*) from SFO_TZ_RUN where RUNNR=:runnr"
            f" and STATE=:state and (SFOID='{sfoids[0]}'")
    for sfo in sfoids[1:]:
        req += f" or SFOID='{sfo}'"
    req += ")"

    opened = orac.execute(req, runnr=run_number, state='OPENED').fetchone()[0]
    closed = orac.execute(req, runnr=run_number, state='CLOSED').fetchone()[0]
    transferred = orac.execute(req, runnr=run_number, state='TRANSFERRED').fetchone()[0]
    print(f'  streams: {opened} opened, {closed} closed, {transferred} transferred')


def _lb_status_fast(orac, run_number, sfoids):
    """
    orac: oracle cursor of an open connection to a oracle db
    run_number: ID of the run to filter objects
    sfoids: list of SFO application name to filter objects, cannot be empty
    """
    if len(sfoids) == 0: raise RuntimeError('empty SFO app list')

    req = ("select count(*) from SFO_TZ_LUMIBLOCK where RUNNR=:runnr"
            f" and STATE=:state and (SFOID='{sfoids[0]}'")
    for sfo in sfoids[1:]:
        req += f" or SFOID='{sfo}'"
    req += ")"

    opened = orac.execute(req, runnr=run_number, state='OPENED').fetchone()[0]
    closed = orac.execute(req, runnr=run_number, state='CLOSED').fetchone()[0]
    transferred = orac.execute(req, runnr=run_number, state='TRANSFERRED').fetchone()[0]
    print(f'  lumiblocks: {opened} opened, {closed} closed, {transferred} transferred')


def _file_status_fast(orac, run_number, sfoids):
    """
    orac: oracle cursor of an open connection to a oracle db
    run_number: ID of the run to filter objects
    sfoids: list of SFO application name to filter objects, cannot be empty
    """
    if len(sfoids) == 0: raise RuntimeError('empty SFO app list')

    req = ("select count(*) from SFO_TZ_FILE where RUNNR=:runnr"
            " and FILESTATE=:fstate and TRANSFERSTATE=:tstate"
            f" and (SFOID='{sfoids[0]}'")
    for sfo in sfoids[1:]:
        req += f" or SFOID='{sfo}'"
    req += ")"

    opened_ondisk = orac.execute(req, runnr=run_number, fstate='OPENED',
            tstate='ONDISK').fetchone()[0]
    closed_ondisk = orac.execute(req, runnr=run_number, fstate='CLOSED',
            tstate='ONDISK').fetchone()[0]
    closed_transferred = orac.execute(req, runnr=run_number, fstate='CLOSED',
            tstate='TRANSFERRED').fetchone()[0]
    deleted_transferred = orac.execute(req, runnr=run_number, fstate='DELETED',
            tstate='TRANSFERRED').fetchone()[0]
    print(f'  files: {opened_ondisk} opened_ondisk, {closed_ondisk} closed_ondisk'
            f', {closed_transferred} closed_transferred'
            f', {deleted_transferred} deleted_transferred')


def oracle_run_status_fast(oradb_url, run_number, sfos):
    print('run status:')
    user, pwd, dbn = coral_auth.get_connection_parameters_from_connection_string(
            oradb_url)
    oradb = oracledb.connect(user=user, password=pwd, dsn=dbn)
    orac = oradb.cursor()

    sfoids = [f'SFO-{i}' for i in sfos]

    _stream_status_fast(orac, run_number, sfoids)
    _lb_status_fast(orac, run_number, sfoids)
    _file_status_fast(orac, run_number, sfoids)

    oradb.close()


def oracle_run_status_report(oradb_url, run_number, sfos, report):
    print('run status:')
    user, pwd, dbn = coral_auth.get_connection_parameters_from_connection_string(
            oradb_url)
    oradb = oracledb.connect(user=user, password=pwd, dsn=dbn)
    orac = oradb.cursor()

    sfoids = [f'SFO-{i}' for i in sfos]

    if report:
        reportf = open(report, 'a')
        reportf.write('---- status\n')

    opened = 0
    closed = 0
    transferred = 0
    stream_state = orac.execute("""select SFOID,STREAMTYPE,STREAM,STATE
            from SFO_TZ_RUN where RUNNR=:runnr and SFOID like 'SFO%'""",
            runnr=run_number).fetchall()
    for [sfoid, streamtype, stream, state] in stream_state:
        if sfoid in sfoids:
            if report: reportf.write(f'{sfoid}, {streamtype}_{stream}: {state}\n')
            if state == 'OPENED': opened += 1
            elif state == 'CLOSED': closed += 1
            elif state == 'TRANSFERRED': transferred += 1
            else: raise RuntimeError(f'unknown stream state: {state}')
    print(f'  streams: {opened} opened, {closed} closed, {transferred} transferred')

    opened = 0
    closed = 0
    transferred = 0
    lb_state = orac.execute("""select SFOID,STREAMTYPE,STREAM,LUMIBLOCKNR,STATE
            from SFO_TZ_LUMIBLOCK where RUNNR=:runnr and SFOID like 'SFO%'""",
            runnr=run_number).fetchall()
    for [sfoid, streamtype, stream, lb, state] in lb_state:
        if sfoid in sfoids:
            if report: reportf.write(f'{sfoid}, {streamtype}_{stream}, {lb}: {state}\n')
            if state == 'OPENED': opened += 1
            elif state == 'CLOSED': closed += 1
            elif state == 'TRANSFERRED': transferred += 1
            else: raise RuntimeError(f'unknown lb state: {state}')
    print(f'  lumiblocks: {opened} opened, {closed} closed, {transferred} transferred')

    opened_ondisk = 0
    closed_ondisk = 0
    closed_transferred = 0
    deleted_transferred = 0
    file_state = orac.execute("""select SFOID,LFN,FILESTATE,TRANSFERSTATE
            from SFO_TZ_FILE where RUNNR=:runnr and SFOID like 'SFO%'""",
            runnr=run_number).fetchall()
    for [sfoid, lfn, filestate, transferstate] in file_state:
        if sfoid in sfoids:
            if report: reportf.write(f'{lfn}: {filestate}, {transferstate}\n')
            if filestate == 'OPENED' and transferstate == 'ONDISK':
                opened_ondisk += 1
            elif filestate == 'CLOSED' and transferstate == 'ONDISK':
                closed_ondisk +=1
            elif filestate == 'CLOSED' and transferstate == 'TRANSFERRED':
                closed_transferred += 1
            elif filestate == 'DELETED' and transferstate == 'TRANSFERRED':
                deleted_transferred +=1
            else:
                raise RuntimeError(f'unknown file state: {filestate}, {transferstate}')
    print(f'  files: {opened_ondisk} opened_ondisk, {closed_ondisk} closed_ondisk'
            f', {closed_transferred} closed_transferred'
            f', {deleted_transferred} deleted_transferred')

    oradb.close()
    if report:
        reportf.write('---- end of status\n')
        reportf.close()


def oracle_run_status(oradb_url, run_number, sfos, report=None):
    if report:
        oracle_run_status_report(oradb_url, run_number, sfos, report)
    else:
        oracle_run_status_fast(oradb_url, run_number, sfos)


if __name__ == '__main__':
    ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument("oradb_url",
            help="URL of the oracle DB to fix, example: oracle://int8r/ATLAS_SFO_T0")
    ap.add_argument("run_number", type=int,
            help="number of the run to check")
    ap.add_argument('sfos', type=int, nargs='+',
            help='list of SFO servers (as int)')
    ap.add_argument("-r", "--report",
            help="file-by-file report")
    args = ap.parse_args()

    oracle_run_status(args.oradb_url, args.run_number, args.sfos,
            report=args.report)
