#!/usr/bin/env python
import sys
import logging
import argparse

sys.path.append('/sw/castor/Script')
import cs.Tools.Libraries.Database as Oracle
import cs.Tools.FilenameParsers.SFOFileNameParser as SFOFileNameParser


# Config for cs...Database: only needs a few paramaters of the CS config
class MockConfig:
    def __init__(self, url):
        self.DBURL = url
        self.DBFileTable = 'SFO_TZ_FILE'
        self.DBLBTable = 'SFO_TZ_LUMIBLOCK'
        self.DBRunTable = 'SFO_TZ_RUN'


# Mocking a SFOFileNameParser object for cs...Database.notTransFiles/notTransLBs
class MockParsed:
    def __init__(self, runnr, sfoid, lbnr, streamtype, streamname):
        self.runnr = runnr
        self.sfoid = sfoid
        self.lbnr = lbnr
        self.streamtype = streamtype
        self.streamname = streamname

    def AppId(self): return self.sfoid
    def RunNr(self): return self.runnr
    def LBNr(self): return self.lbnr
    def StreamType(self): return self.streamtype
    def StreamName(self): return self.streamname


def oracle_sync_transferred_files_to_lbs_and_streams(oradb_url,
        run_number, outliers_file, verbose=False, dryrun=False, report=None):
    del outliers_file
    conf = MockConfig(oradb_url)
    parser = SFOFileNameParser.SFOFileNameParser
    dblogger = logging.getLogger()
    dblogger.setLevel(logging.DEBUG)
    dblogger.addHandler(logging.StreamHandler(sys.stdout))
    oradb = Oracle.Database(conf, dblogger, parser)

    reportf = None
    if report:
        reportf = open(report, 'a')
        reportf.write('---- marking LBs and streams transferred if possible\n')

    updated_lbs = 0
    non_transferred_lbs = oradb.db.curs.execute("""select SFOID, LUMIBLOCKNR,
            STREAMTYPE, STREAM, STATE from SFO_TZ_LUMIBLOCK
            where RUNNR=:runnr and STATE!='TRANSFERRED' and SFOID like 'SFO%'
            """, runnr=run_number).fetchall()
    if verbose: print(f'Found {len(non_transferred_lbs)} non-transferred LBs')

    ilb = 0
    for [sfoid, lbnr, streamtype, streamname, state] in non_transferred_lbs:
        ilb += 1
        if verbose: print(f'checking LB {ilb}/{len(non_transferred_lbs)}')
        if state != 'CLOSED':
            msg = (f'cannot update LB ({run_number},{sfoid},{lbnr},'
                    f'{streamtype},{streamname}): state is not CLOSED')
            if verbose: print(msg)
            if reportf: reportf.write('{msg}\n')
            continue
        parsed = MockParsed(run_number, sfoid, lbnr, streamtype, streamname)
        if oradb.notTransFiles(parsed) == 0:
            updated_lbs += 1
            if not dryrun:
                oradb.lbtransfer(parsed)
                if reportf: reportf.write(f'marked LB({run_number},{sfoid},'
                        f'{lbnr},{streamtype},{streamname}) transferred\n')
            else:
                if reportf: reportf.write(f'would have marked LB({run_number},'
                        f'{sfoid},{lbnr},{streamtype},{streamname}) transferred\n')

    updated_streams = 0
    non_transferred_streams = oradb.db.curs.execute("""select SFOID, STREAMTYPE,
            STREAM, STATE from SFO_TZ_RUN
            where RUNNR=:runnr and STATE!='TRANSFERRED' and SFOID like 'SFO%'
            """, runnr=run_number).fetchall()
    if verbose: print(f'Found {len(non_transferred_streams)} non-transferred streams')

    istream = 0
    for [sfoid, streamtype, streamname, state] in non_transferred_streams:
        istream += 1
        if verbose: print(f'checking stream {istream}/{len(non_transferred_streams)}')
        if state != 'CLOSED':
            msg = (f'cannot update stream ({run_number},{sfoid},'
                    f'{streamtype},{streamname}): state is not CLOSED')
            if verbose: print(msg)
            if reportf: reportf.write('{msg}\n')
            continue
        parsed = MockParsed(run_number, sfoid, None, streamtype, streamname)
        if oradb.notTransLBs(parsed) == 0:
            updated_streams += 1
            if not dryrun:
                oradb.runtransfer(parsed)
                if reportf: reportf.write(f'marked stream({run_number},{sfoid},'
                        f'{streamtype},{streamname}) transferred\n')
            else:
                if reportf: reportf.write(f'would have marked stream({run_number},'
                        f'{sfoid},{streamtype},{streamname}) transferred\n')


    oradb.db.orcl.close()
    if reportf:
        reportf.write('---- end of marking LBs and streams transferred\n')
        reportf.close()
    if verbose:
        if dryrun:
            print(f'would have marked {updated_lbs} LBs as transferred')
            print(f'would have marked {updated_streams} streams as transferred')
        else:
            print(f'marked {updated_lbs} LBs as transferred')
            print(f'marked {updated_streams} streams as transferred')


if __name__ == '__main__':
    ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument("oradb_url",
            help="URL of the oracle DB to fix, example: oracle://int8r/ATLAS_SFO_T0")
    ap.add_argument("run_number", type=int,
            help="number of the run to check")
    ap.add_argument("outliers_file",
            help="file to print out outliers")
    ap.add_argument("-v", "--verbose", action="store_true",
            help="enable verbosity")
    ap.add_argument("-d", "--dryrun", action="store_true",
            help="do not modify oracle DB")
    ap.add_argument("-r", "--report",
            help="file-by-file report")
    args = ap.parse_args()

    oracle_sync_transferred_files_to_lbs_and_streams(args.oradb_url,
            args.run_number, args.outliers_file, verbose=args.verbose,
            dryrun=args.dryrun, report=args.report)
