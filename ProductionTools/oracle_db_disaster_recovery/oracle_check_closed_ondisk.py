#!/usr/bin/env python
import sys
import os
import subprocess
import logging
import argparse
import re

sys.path.append('/sw/castor/Script')
import cs.Tools.Libraries.Database as Oracle
import cs.Tools.FilenameParsers.SFOFileNameParser as SFOFileNameParser


# Config for cs...Database: only needs a few paramaters of the CS config
class MockConfig:
    def __init__(self, url):
        self.DBURL = url
        self.DBFileTable = 'SFO_TZ_FILE'
        self.DBLBTable = 'SFO_TZ_LUMIBLOCK'
        self.DBRunTable = 'SFO_TZ_RUN'


def oracle_check_closed_ondisk(oradb_url, run_number, outliers_file,
        verbose=False, dryrun=False, report=None):
    conf = MockConfig(oradb_url)
    parser = SFOFileNameParser.SFOFileNameParser
    dblogger = logging.getLogger()
    dblogger.setLevel(logging.ERROR)
    dblogger.addHandler(logging.StreamHandler(sys.stdout))
    oradb = Oracle.Database(conf, dblogger, parser)

    updated = 0
    if report:
        reportf = open(report, 'a')
        reportf.write('---- step B\n')

    ora_state_3 = oradb.db.curs.execute("""select SFOID, SFOPFN from SFO_TZ_FILE
            where RUNNR=:runnr and FILESTATE='CLOSED' and TRANSFERSTATE='ONDISK'
            and SFOID like 'SFO%'""", runnr=run_number).fetchall()
    if verbose: print(f'Found {len(ora_state_3)} (closed,ondisk)')

    with open(outliers_file, 'w') as outf:
        for [sfoid, sfopfn] in ora_state_3:
            m = re.match('sfo-([0-9]+)', sfoid, re.IGNORECASE)
            if not m:
                outf.write(f'sfoid does not match expected format: {sfoid}, file: sfopfn')
                continue
            sfonum = int(m.group(1))
            if sfonum < 10:
                sfoid = f'sfo-0{sfonum}'
            else:
                sfoid = f'sfo-{sfonum}'
                
            cmd = ['ssh', '-x', f'pc-tdq-{sfoid}', 'sh', '-c', f'"ls {sfopfn}*"']
            lsp = subprocess.run(cmd, stdout=subprocess.PIPE, check=True)
            file_exist = False
            if lsp.returncode == 0:
                for file in lsp.stdout.decode().split('\n'):
                    ext = os.path.splitext(file)[1]
                    if ext == '.COPIED':
                        # .COPIED file exists: mark file as transferred in DB

                        # Get remote dir from .COPIED file
                        cmd = ['ssh', '-x', f'pc-tdq-{sfoid}', 'grep', 'RemoteDir',
                                f'{sfopfn}.COPIED']
                        try:
                            output = subprocess.check_output(cmd)
                            remote_dir = output.decode().split()[2]
                        except subprocess.CalledProcessError:
                            outf.write(f'(closed,ondisk), error reading RemoteDir from'
                                    f' COPIED file: {sfoid} {sfopfn}\n')
                            continue

                        remote_filename = os.path.join(remote_dir, os.path.basename(sfopfn))
                        if report: reportf.write(f'marking {sfopfn} as transferred in DB\n')
                        if not dryrun:
                            oradb.Transfer(sfopfn, remote_filename)
                        updated += 1
                    elif ext == '.TOBECOPIED' or ext == '.COPYING':
                        pass
                        # CastorScript was taking care of the file: nothing to do
                        # processing will continue once restarted
                    elif ext == '.data':
                        file_exist = True

            if not file_exist:
                # no file: even if data file is absent
                # this should not be possible: since in the database the file is
                # not 'TRANSFERRED' it cannot be processed by T0, so it cannot
                # be migrated to tape, and cannot be deleted.
                outf.write(f'(closed,ondisk), no file: {sfoid} {sfopfn}\n')

    oradb.db.orcl.close()
    if report:
        reportf.write('---- end of step B\n')
        reportf.close()
    if verbose:
        if dryrun:
            print(f'would have marked {updated} as transferred')
        else:
            print(f'marked {updated} as transferred')


if __name__ == '__main__':
    ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument("oradb_url",
            help="URL of the oracle DB to fix, example: oracle://int8r/ATLAS_SFO_T0")
    ap.add_argument("run_number", type=int,
            help="number of the run to check")
    ap.add_argument("outliers_file",
            help="file to print out outliers")
    ap.add_argument("-v", "--verbose", action="store_true",
            help="enable verbosity")
    ap.add_argument("-d", "--dryrun", action="store_true",
            help="do not modify oracle DB")
    ap.add_argument("-r", "--report",
            help="file-by-file report")
    args = ap.parse_args()

    oracle_check_closed_ondisk(args.oradb_url, args.run_number, args.outliers_file,
            verbose=args.verbose, dryrun=args.dryrun, report=args.report)
