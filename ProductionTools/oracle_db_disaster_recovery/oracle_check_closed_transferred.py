#!/usr/bin/env python
import sys
import subprocess
import logging
import argparse
import re

sys.path.append('/sw/castor/Script')
import cs.Tools.Libraries.Database as Oracle
import cs.Tools.FilenameParsers.SFOFileNameParser as SFOFileNameParser


# Config for cs...Database: only needs a few paramaters of the CS config
class MockConfig:
    def __init__(self, url):
        self.DBURL = url
        self.DBFileTable = 'SFO_TZ_FILE'
        self.DBLBTable = 'SFO_TZ_LUMIBLOCK'
        self.DBRunTable = 'SFO_TZ_RUN'


def oracle_check_closed_transferred(oradb_url, run_number, outliers_file,
        verbose=False, dryrun=False, report=None):
    conf = MockConfig(oradb_url)
    parser = SFOFileNameParser.SFOFileNameParser
    dblogger = logging.getLogger()
    dblogger.setLevel(logging.DEBUG)
    dblogger.addHandler(logging.StreamHandler(sys.stdout))
    oradb = Oracle.Database(conf, dblogger, parser)

    updated = 0
    if report:
        reportf = open(report, 'a')
        reportf.write('---- step C\n')

    ora_state_4 = oradb.db.curs.execute("""select SFOID, SFOPFN from SFO_TZ_FILE
            where RUNNR=:runnr and FILESTATE='CLOSED' and SFOID like 'SFO%'
            and TRANSFERSTATE='TRANSFERRED'""", runnr=run_number).fetchall()
    if verbose: print(f'Found {len(ora_state_4)} (closed,transferred)')

    with open(outliers_file, 'w') as outf:
        for [sfoid, sfopfn] in ora_state_4:
            m = re.match('sfo-([0-9]+)', sfoid, re.IGNORECASE)
            if not m:
                outf.write(f'sfoid does not match expected format: {sfoid}, file: sfopfn')
                continue
            sfonum = int(m.group(1))
            if sfonum < 10:
                sfoid = f'sfo-0{sfonum}'
            else:
                sfoid = f'sfo-{sfonum}'

            cmd = ['ssh', '-x ', f'pc-tdq-{sfoid}', 'ls', sfopfn]
            lsp = subprocess.run(cmd, stderr=subprocess.PIPE, check=True)
            if lsp.returncode == 2 \
                    and 'No such file or directory' in lsp.stderr.decode():
                # File does not exist: mark it as deleted
                if report: reportf.write(f'marking {sfopfn} as deleted in DB\n')
                if not dryrun:
                    oradb.Deletion(sfopfn)
                updated += 1

    oradb.db.orcl.close()
    if report:
        reportf.write('---- end of step C\n')
        reportf.close()
    if verbose:
        if dryrun:
            print(f'would have marked {updated} as deleted')
        else:
            print(f'marked {updated} as deleted')


if __name__ == '__main__':
    ap = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument("oradb_url",
            help="URL of the oracle DB to fix, example: oracle://int8r/ATLAS_SFO_T0")
    ap.add_argument("run_number", type=int,
            help="number of the run to check")
    ap.add_argument("outliers_file",
            help="file to print out outliers")
    ap.add_argument("-v", "--verbose", action="store_true",
            help="enable verbosity")
    ap.add_argument("-d", "--dryrun", action="store_true",
            help="do not modify oracle DB")
    ap.add_argument("-r", "--report",
            help="file-by-file report")
    args = ap.parse_args()

    oracle_check_closed_transferred(args.oradb_url, args.run_number,
            args.outliers_file, verbose=args.verbose, dryrun=args.dryrun,
            report=args.report)
