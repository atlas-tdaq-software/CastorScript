#!/usr/bin/env bash

if [ "x$CMTRELEASE" != x ]; then
  # TDAQ environment already loaded
  echo "using $CMTRELEASE"
elif [ -r /sw/castor/script_setup.sh ]; then
  #P1
  source /sw/castor/script_setup.sh
elif [ -r /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh ]; then
  #testbed
  source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh
else
  echo "error: cannot setup environment"
  exit
fi

python -i -c "
import coral_auth
import oracledb
oracledb.init_oracle_client()
user,pwd,dbn = coral_auth.get_connection_parameters_from_connection_string('oracle://int8r/ATLAS_SFO_T0')
orcl = oracledb.connect(user=user, password=pwd, dsn=dbn)
curs = orcl.cursor()
print('orcl: connection to db object, curs: cursor on the connection')
"
