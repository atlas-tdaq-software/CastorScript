#!/usr/bin/env tdaq_python

from __future__ import print_function
from __future__ import division
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import range
from past.utils import old_div
import urllib.request, urllib.error, urllib.parse
import json
import uuid
from time import time
import sys

#PROXY = 'atlasgw-exp:3128'
PROXY = None
#ENDPOINT = 'https://rucio-lb-prod.cern.ch/traces/'
ENDPOINT = 'https://rucio-lb-prod-03.cern.ch/traces/'
DATA = {
        'uuid': str(uuid.uuid4()).replace('-', '').lower(),
        'account': '',
        'dataset': '',
        'scope': '',
        'eventType': 'sfo2eos',
        'clientState': 'DONE',
        'timeStart': time() * 1000000,
        'timeEnd': time() * 1000000 + 1000,
        'filename': 'flegoff.test',
        'filesize': 1000,
        'remoteSite': 'CERN-PROD_TZDISK',
        'localSite': 'CERN-P1_SFO'
}

try:
    if PROXY:
        proxy = urllib.request.ProxyHandler({'https': PROXY})
        opener = urllib.request.build_opener(proxy)
        urllib.request.install_opener(opener)
except Exception as exc:
    print('error:', str(exc))

start = time()
NB_REQ = 1000
for i in range(NB_REQ):
    try:
        req = urllib.request.Request(ENDPOINT)
        req.add_header('Content-Type', 'application/json')
        urllib.request.urlopen(req, json.dumps(DATA).encode())
        sys.stdout.write('.')
        sys.stdout.flush()
    except Exception as exc:
        print()
        print('error:', str(exc))
end = time()

print()
print('{} requests: average = {} s/request'.format(NB_REQ, old_div((end - start), NB_REQ)))
