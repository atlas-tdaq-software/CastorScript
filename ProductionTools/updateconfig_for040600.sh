#!/bin/bash

CONF=$1

sed '
s/mailList:/EmailLogList:/
s/mailLevel:/EmailLogLevel:/
s/mailSender:/EmailLogSender:/
s/lockFile:/LockFilePattern:/
s/pythonPathPrepend:/PythonPathPrepend:/
s/ERSenabled:/ERSEnabled:/
s/ers_error:/ERSEnvError:/
s/ers_info:/ERSEnvInfo:/
s/ers_warning:/ERSEnvWarning:/
s/ers_debug:/ERSEnvDebug:/
s/IS_enabled:/ISEnabled:/
s/IS_partition:/ISPartition:/
s/IS_server:/ISServer:/
s/IS_publication_period:/ISPublicationPeriod:/
s/fs_info_publishing_enabled:/ISFileSystemInfoEnabled:/
s/IS_wait_after_partition_valid:/ISWaitAfterParitionValid:/
s/connection:/DBURL:/
s/file_table:/DBFileTable:/
s/lb_table:/DBLBTable:/
s/run_table:/DBRunTable:/
s/DBTimeout:/MainThreadEventTimeout:/
s/Filenameparser:/FilenameParser:/
s/FilenameparserConf:/FilenameParserConf:/
s/DirList:/SrcDirs:/
s/nDel:/ManagerThreadClearedFilesPeriod:/
s/ManagerTimeout:/ManagerThreadEventTimeout:/
s/drivenPool:/DrivenPool:/
s/maxCopy:/MaxConcurrentCopy:/
s/maxRetry:/MaxCopyImmediateRetry:/
s/CopyTimeout:/CopyThreadEventTimeout:/
s/NoneTimeout:/TransferTimeout:/
s/avoidCopyOverlap:/StopOngoingTransferAfterLock:/
s/computeChecksum:/ComputeChecksumFromLocalFile:/
s/bwDevice:/BWDevice:/
s/bwLimit:/BWLimit:/
s/DeleteTimeout:/DeleteThreadEventTimeout:/
s/highCriticMark:/DeleteHighCriticalMark:/
s/lowCriticMark:/DeleteLowCriticalMark:/
s/lowWaterMark:/DeleteLowWaterMark:/
s/ignoreLock:/DeleteIgnoreLock:/
s/nFiles:/DeleteFileSystemUsageFilesPeriod:/
s/minFileTime:/DeleteMinFileAge:/
' -i $CONF
