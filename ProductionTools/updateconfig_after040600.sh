#!/bin/bash

CONF=$1

sed '
s/DeleteFileSystemUsageFilesPeriod: 10.*/DeleteFileSystemUsagePeriod: 300/
s/DeleteFileSystemUsageFilesPeriod: 2.*/DeleteFileSystemUsagePeriod: 60/
s/DeleteFileSystemUsageFilesPeriod: 1.*/DeleteFileSystemUsagePeriod: 60/
' -i $CONF
