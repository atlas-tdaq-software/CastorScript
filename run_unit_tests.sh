#!/bin/bash
cd `dirname $0`

if ! [ -v TDAQ_LCG_RELEASE ]; then
    source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-01
fi

PATTERN='*_[Tt]est.py'
if [ x"$1" = "xgitlab" ]; then
  PATTERN='*_Test.py'
fi

python -m unittest discover -t . -s UnitTests/ -p $PATTERN --verbose
