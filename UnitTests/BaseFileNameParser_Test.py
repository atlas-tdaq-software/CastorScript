import unittest

if __name__ == '__main__':
    import sys
    from os.path import dirname, abspath, join
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

from cs.Tools.FilenameParsers.BaseFileNameParser import BaseFileNameParser

class TestBaseFileNameParser(unittest.TestCase):

    def setUp(self):
        self.baseFNP = BaseFileNameParser('filename')

    def test_it_should_save_the_filename(self):
        self.assertEqual(self.baseFNP.filename, "filename")

if __name__ == '__main__':
    unittest.main()
