import unittest
import logging
import sys

if __name__ == '__main__':
    from os.path import dirname, abspath, join
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

from cs.Tools.MigrationCheckers import DDMMigrated

class TestDDMMigrated(unittest.TestCase):
    '''
    The class, i.e. the test fixture, is used to hold the checker which is
    stateless.
    Add verbose=True param to isMigrated call to investigate.
    '''

    @classmethod
    def setUpClass(cls):
        cls.conf = {
                'eosinstance': 'eosdummy',
                'basedir': '/eos/dummy/base',
                'backend': 'ddmmigratedteststorage',
                }

        cls.logger = logging.getLogger(__name__)
        cls.logger.setLevel(logging.ERROR)
        cls.logger.addHandler(logging.StreamHandler(sys.stdout))

        cls.mchecker = DDMMigrated.MigrationChecker(cls.conf, cls.logger, None)


    @classmethod
    def tearDownClass(cls):
        cls.mchecker.stop()


    def test_not_merged_simple(self):
        fn = 'data_test.00751981.physics_test.daq.RAW._lb0001._SFO-0._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_not_merged_simple_not_migrated(self):
        fn = 'data_test.00751981.physics_test.daq.RAW._lb0001._SFO-0._0002.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_not_merged_simple_not_exist(self):
        fn = 'data_test.00751981.physics_test.daq.RAW._lb0001._SFO-0._0003.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_not_merged_with_ts(self):
        fn = 'data_test.00751981.physics_test.daq.RAW._lb0002._SFO-0._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_not_merged_with_ts_wrong_migrated(self):
        fn = 'data_test.00751981.physics_test.daq.RAW._lb0004._SFO-0._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_not_merged_debug(self):
        fn = 'data_test.00751981.debug_crash.daq.RAW._lb0001._SFO-0._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_not_merged_debug_with_ts(self):
        fn = 'data_test.00751981.debug_wrongdata.daq.RAW._lb0002._SFO-0._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_simple(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0010._SFO-1._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0010._SFO-2._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_simple_not_migrated(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0011._SFO-1._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_not_exist(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0012._SFO-1._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_with_attemptnr(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0013._SFO-1._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0013._SFO-2._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_with_attemptnr_wrong_migrated(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0014._SFO-1._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0014._SFO-2._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_with_attemptnr_with_timestamp(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0015._SFO-1._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0015._SFO-2._0001.data'
        self.assertTrue(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_with_attemptnr_with_timestamp_wrong_attemptnr_migrated(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0016._SFO-1._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0016._SFO-2._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

    def test_merged_with_attemptnr_with_timestamp_wrong_timestamp_migrated(self):
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0017._SFO-1._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))
        fn = 'data_test.00751981.physics_merged.daq.RAW._lb0017._SFO-2._0001.data'
        self.assertFalse(TestDDMMigrated.mchecker.isMigrated(fn))

if __name__ == '__main__':
    unittest.main()
