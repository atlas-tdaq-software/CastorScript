import time
from subprocess import Popen, PIPE
import tempfile
import os
import shutil


#For timing timeouts, timer starts with __init__ function
class Timer():
    def __init__(self, seconds):
        self.startTime = time.time()
        self.endTime = self.startTime + seconds
        self.timedout = False

    def timesUp(self):
        if (self.endTime < time.time()):
            self.timedout = True
            return True
        else:
            return False

    def timesNotUp(self):
        return not self.timesUp()


def createTmpDir():
    """
    Caller is responsible for erasing the directories created here
    """
    dirs = {}
    dirs["tmp"] = tempfile.mkdtemp()

    dirs["src"] = dirs["tmp"] + "/src"
    dirs["des"] = dirs["tmp"] + "/des"
    dirs["log"] = dirs["tmp"] + "/log"

    try:
        os.makedirs(dirs["src"])
        os.makedirs(dirs["des"])
        os.makedirs(dirs["log"])
    except:
        shutil.rmtree(dirs["tmp"])
        raise

    return dirs


class DirectoryChecker():
    """For checking the contents of a directory either once, or until a condition is met or timeout is reached."""
    def __init__(self, dirPath):
        self.dirPath = dirPath
        self.lsOutput = None
        self.wasEmpty = None
        self.timerUtil = None

    def waitForAnyFile(self, seconds):
        "Waits for the specified amount of time for the dir to be empty, returns a tuple (bool: timedout, str: lsOutput)"
        self.timerUtil = Timer(seconds)
        while self.checkIfEmpty() and self.timerUtil.timesNotUp():
            pass
        return (self.timerUtil.timedout, self.getLsOutput())

    def waitForEmptyDir(self, seconds):
        "Waits for the specified amount of time for the dir to contain files, returns a tuple (bool: timedout, str: lsOutput)"
        self.timerUtil = Timer(seconds)
        while self.checkIfNotEmpty() and self.timerUtil.timesNotUp():
            pass
        return (self.timerUtil.timedout, self.getLsOutput())

    def checkIfEmpty(self):
        if len(self.getLsOutput()) > 1:
            self.wasEmpty = False
            return False
        else:
            self.wasEmpty = True
            return True

    def checkIfNotEmpty(self):
        return not self.checkIfEmpty()

    def checkIfContains(self, substring):
        if self.getLsOutput().find(substring) > -1:
            return True
        else:
            return False

    def getLsOutput(self):
        self.lsOutput = Popen(["ls", self.dirPath],bufsize=-1, stdout=PIPE).communicate()[0].decode()
        return self.lsOutput
