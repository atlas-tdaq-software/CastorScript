import unittest
import signal
import shutil
from subprocess import Popen
import os

if __name__ == '__main__':
    import sys
    from os.path import dirname, abspath, join
    ROOT_DIR = abspath(join(dirname(__file__), '..'))
    sys.path.append(ROOT_DIR)

from UnitTests.DirUtils import DirectoryChecker
from UnitTests.DirUtils import createTmpDir


# I'm a developer, I want to see if the script can copy files.
class TestCanCopyFilesLocally(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.dirs = createTmpDir()

        # config file
        with open(os.path.join(self.dirs["tmp"], 'localcopy_test.cfg'), 'a+') as cf:
            cf.write("LogDir: '" + self.dirs['log'] + "'\n")
            cf.write("SrcDirs: ['" + self.dirs['src'] + "']\n")
            cf.write("CopyDir: '" + self.dirs['des'] + "'\n")
            cf.write("""BackendModule: 'localstorage'
                    MainThreadEventTimeout: 1
                    DataFilePattern: ['*.data']
                    ManagerThreadEventTimeout: 1
                    EOSInstance: 'eosdummy'
                    CopyThreadEventTimeout: 1
                    TransferTimeout: 0
                    DeleteThreadEventTimeout: 1
                    """)

        # data file
        test_file = open(os.path.join(self.dirs["src"], 'file.data'), "w")
        test_file.close()

        base_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
        scriptcommand = ["python", base_dir + "/Script/CastorScript.py",
                self.dirs["tmp"] + "/localcopy_test.cfg" ]
        self.castorscriptprocess = Popen(scriptcommand)


    @classmethod
    def tearDownClass(self):
        # stop process
        self.castorscriptprocess.send_signal(signal.SIGUSR2)
        self.castorscriptprocess.wait()
        # remove directories and files
        shutil.rmtree(self.dirs["tmp"])


    # Test cases:

    def test_localCopy_script_has_been_instanciated(self):
        #test if the script runs by Instance
        self.assertNotEqual(self.castorscriptprocess, None,
                msg="castorscript is not running")


    def test_localCopy_can_produce_logfiles(self):

        seconds = 10
        timeout, output = DirectoryChecker(self.dirs["log"]).waitForAnyFile(seconds)

        timeoutMessage = f"Timeout, found no log files within a timeframe of {seconds} seconds"
        self.assertFalse(timeout, msg=timeoutMessage)

        outNotFoundMessage = f"No files ending with .log found in log directory, found: {output}"
        self.assertTrue(output.find(".log") > -1, msg=outNotFoundMessage)


    def test_localCopy_can_copy_files_to_des_directory(self):

        seconds = 10
        timeout, output = DirectoryChecker(self.dirs["des"]).waitForAnyFile(seconds)

        timeoutMessage = "Timeout, found no files within a timeframe of " + str(seconds) + " seconds"
        self.assertFalse(timeout, msg=timeoutMessage)

        dataNotFoundMessage = "No files ending with .data found in des directory, found:" + output
        self.assertTrue(output.find(".data") > -1, msg=dataNotFoundMessage)


    def test_localCopy_src_files_was_deleted(self):

        seconds = 5
        timeout, output = DirectoryChecker(self.dirs["src"]).waitForEmptyDir(seconds)

        timeoutMessage = "Timeout, found no empty dir within a timeframe of " + str(seconds) + " seconds"
        self.assertFalse(timeout, msg=timeoutMessage)

        logsNotFoundMessage = "No files ending with .out found in log directory, found:" + output
        self.assertTrue(output.find("file") == -1, msg=logsNotFoundMessage)


if __name__ == '__main__':
    unittest.main()
