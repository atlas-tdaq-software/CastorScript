import unittest
import unittest.mock
import os
import threading
import queue
from time import time
import logging
from copy import deepcopy
import sys
from unittest.mock import ANY
import signal

if __name__ == '__main__':
    from os.path import dirname, abspath, join
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

import cs.Threads.CopyThread as CopyThread
from cs.Threads.FileMetaData import FileMetaData as FMD
import cs.Tools.ConfigParser as ConfigParser
import cs.Tools.Libraries.Database as Database
import cs.Tools.Constants as Constants


def mocklog(loggername, filename, config):
    # Mocks LogUtils.enable_file_logging
    del loggername, filename, config
    if mocklog.logger is None:
        mocklog.logger = logging.getLogger(__file__)
        mocklog.logger.setLevel(logging.DEBUG)
        mocklog.logger.addHandler(logging.StreamHandler(sys.stdout))
        mocklog.logger.disabled = True # comment this out if you want the logs
    return mocklog.logger
mocklog.logger = None


class TestCopyThread(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.this_dir = os.path.dirname(__file__)
        cls.config_path = os.path.abspath(os.path.join(cls.this_dir, "../Configs/"))

        # do not access conf directly, use getBasicConf()
        cls.basic_conf = ConfigParser.ConfigHolder(os.path.join(
                cls.config_path, 'minimal.cfg'))
        cls.basic_conf.SrcDirs = ['/srcdir']
        cls.basic_conf.backend = unittest.mock.Mock(spec_set=cls.basic_conf.backend)
        cls.basic_conf.backend.copystate.return_value = 0
        cls.basic_conf.backend.backgroundcopy.return_value = (None, 751981)
        cls.basic_conf.backend.sizechecksum.return_value = (1423, 'deadbeef')

        cls.ev = threading.Event()
        cls.dblock = threading.Lock()
        cls.dq = queue.Queue() # delete queue
        cls.copyq = queue.Queue()
        cls.clrq = queue.Queue()
        cls.ddmq = queue.Queue()

        # Patch (and save) a few stuff for all tests
        cls.backup_osstat = os.stat
        os.stat = unittest.mock.Mock()
        os.stat.return_value.st_size = 1423 # filesize

        cls.backup_osrename = os.rename
        os.rename = unittest.mock.Mock()

        cls.backup_enable_file_logging = CopyThread.enable_file_logging
        CopyThread.enable_file_logging = mocklog

        cls.backup_adler32 = CopyThread.adler32
        CopyThread.adler32 = unittest.mock.Mock()
        CopyThread.adler32.return_value = 'deadbeef'

        cls.backup_oskill = os.kill
        os.kill = unittest.mock.Mock()

    @classmethod
    def tearDownClass(cls):
        os.stat = cls.backup_osstat
        os.rename = cls.backup_osrename
        CopyThread.enable_file_logging = cls.backup_enable_file_logging
        CopyThread.adler32 = cls.backup_adler32
        os.kill = cls.backup_oskill


    @classmethod
    def getBasicConf(cls):
        c = deepcopy(cls.basic_conf)
        return c


    def setUp(self):
        self.c = type(self).getBasicConf()
        with type(self).dq.mutex: type(self).dq.queue.clear()
        with type(self).copyq.mutex: type(self).copyq.queue.clear()
        with type(self).clrq.mutex: type(self).clrq.queue.clear()
        with type(self).ddmq.mutex: type(self).ddmq.queue.clear()
        self.c.backend.reset_mock()
        os.stat.reset_mock()
        os.rename.reset_mock()
        CopyThread.adler32.reset_mock()
        os.kill.reset_mock()


    def test_basic(self):
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        dst = os.path.join(fmd.remote_dir, os.path.basename(fmd.file_name))

        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        self.assertEqual(fmd.file_name, ct.transfers[0].file_info.file_name)
        self.c.backend.mkdir.assert_called_once_with(
                fmd.remote_dir, fmd.eos_instance, ANY)
        self.c.backend.backgroundcopy.assert_called_once_with(
                fmd.file_name, dst, fmd.eos_instance, ANY)
        os.rename.assert_called_once_with(fmd.file_name + Constants.tobecopied_ext,
                  fmd.file_name + Constants.copying_ext)

        os.rename.reset_mock()
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.files_copied)
        self.assertEqual(fmd, type(self).dq.get())
        os.rename.assert_called_once_with(fmd.file_name + Constants.copying_ext,
                  fmd.file_name + Constants.copied_ext)


    def test_basic_with_db(self):
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        ct.db = unittest.mock.Mock(spec_set=Database.Database)
        ct.db.FileInfo.return_value = ('deadbeef', 1423, 'SANE')

        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        dst = os.path.join(fmd.remote_dir, os.path.basename(fmd.file_name))

        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        self.assertEqual(fmd.file_name, ct.transfers[0].file_info.file_name)

        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.files_copied)
        self.assertEqual(fmd, type(self).dq.get())
        ct.db.Transfer.assert_called_once_with(fmd.file_name, dst)


    def test_truncated_file(self):
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        ct.db = unittest.mock.Mock(spec_set=Database.Database)
        ct.db.FileInfo.return_value = ('coincoin', 14, 'TRUNCATED')

        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)

        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        self.assertEqual(fmd.file_name, ct.transfers[0].file_info.file_name)
        ct.db.UpdateTruncated.assert_called_once_with(fmd.file_name, 1423, 'deadbeef')

        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.files_copied)
        self.assertEqual(fmd, type(self).dq.get())


    def test_local_checksum(self):
        self.c.ComputeChecksumFromLocalFile = True
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)

        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)

        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        self.assertEqual(fmd.file_name, ct.transfers[0].file_info.file_name)
        CopyThread.adler32.assert_called_once_with(fmd.file_name)

        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.files_copied)
        self.assertEqual(fmd, type(self).dq.get())


    def test_concurrent_transfers(self):
        self.c.MaxConcurrentCopy = 2
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd0 = FMD('/srcdir/totransfer0', '/eos/dummy', 'eosdummy', 0)
        fmd1 = FMD('/srcdir/totransfer1', '/eos/dummy', 'eosdummy', 0)
        fmd2 = FMD('/srcdir/totransfer2', '/eos/dummy', 'eosdummy', 0)

        type(self).copyq.put(fmd0)
        type(self).copyq.put(fmd1)
        type(self).copyq.put(fmd2)
        ct.startTransfers()
        self.assertEqual(2, ct.nbtransfers)
        self.assertEqual(fmd0.file_name, ct.transfers[0].file_info.file_name)
        self.assertEqual(fmd1.file_name, ct.transfers[1].file_info.file_name)

        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(2, ct.files_copied)
        self.assertEqual(fmd0, type(self).dq.get())
        self.assertEqual(fmd1, type(self).dq.get())

    @unittest.mock.patch('cs.Threads.CopyThread.CopyThread.getCurrentBW')
    def test_bw_limit(self, mock_gcb):
        self.c.MaxConcurrentCopy = 2
        self.c.BWDevice = 'eth0'
        self.c.BWLimit = 1
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd0 = FMD('/srcdir/totransfer0', '/eos/dummy', 'eosdummy', 0)
        fmd1 = FMD('/srcdir/totransfer1', '/eos/dummy', 'eosdummy', 0)

        mock_gcb.return_value = 0
        type(self).copyq.put(fmd0)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        self.assertEqual(2, mock_gcb.call_count)

        mock_gcb.reset_mock()
        mock_gcb.return_value = 1
        type(self).copyq.put(fmd1)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        self.assertEqual(fmd1, type(self).copyq.get())
        mock_gcb.assert_called_once()


    @unittest.mock.patch('cs.Threads.CopyThread.dir_is_locked')
    def test_locked_dir(self, mock_dil):
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)

        mock_dil.return_value = True
        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(fmd, type(self).clrq.get())


    def test_transfer_timeout_noretry(self):
        self.c.TransferTimeout = 1
        self.c.MaxCopyImmediateRetry = 0
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)

        type(self).copyq.put(fmd)
        with unittest.mock.patch('cs.Threads.CopyThread.time') as mock_time:
            mock_time.return_value = time() - 10
            ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)

        self.c.backend.copystate.return_value = None
        os.rename.reset_mock()
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(0, ct.files_copied)
        self.assertEqual(1, ct.problematic_transfer_failures)
        self.assertEqual(fmd, type(self).clrq.get())
        os.kill.assert_called_once_with(751981, signal.SIGKILL)
        os.rename.assert_called_once_with(fmd.file_name + Constants.copying_ext,
                  fmd.file_name + Constants.problematic_ext)


    def test_transfer_timeout_retry(self):
        self.c.TransferTimeout = 1
        self.c.MaxCopyImmediateRetry = 1
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        dst = os.path.join(fmd.remote_dir, os.path.basename(fmd.file_name))

        type(self).copyq.put(fmd)
        with unittest.mock.patch('cs.Threads.CopyThread.time') as mock_time:
            mock_time.return_value = time() - 10
            ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        os.rename.assert_called_once_with(
            fmd.file_name + Constants.tobecopied_ext,
            fmd.file_name + Constants.copying_ext)

        os.rename.reset_mock()
        self.c.backend.copystate.return_value = None
        ct.checkTransfers()
        self.assertEqual(1, ct.nbtransfers)
        self.assertEqual(0, ct.files_copied)
        self.assertEqual(1, ct.simple_transfer_failures)
        os.kill.assert_called_once_with(751981, signal.SIGKILL)
        self.c.backend.remove.assert_called_once_with(dst, fmd.eos_instance, ANY)
        self.assertEqual(1, ct.transfers[0].retry)
        os.rename.assert_not_called()

        self.c.backend.copystate.return_value = 0
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.files_copied)
        self.assertEqual(1, ct.simple_transfer_failures)
        os.rename.assert_called_once_with(
            fmd.file_name + Constants.copying_ext,
            fmd.file_name + Constants.copied_ext)


    @unittest.mock.patch('cs.Threads.CopyThread.dir_is_locked')
    def test_stop_transfer_after_lock(self, mock_dil):
        self.c.StopOngoingTransferAfterLock = True
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        dst = os.path.join(fmd.remote_dir, os.path.basename(fmd.file_name))

        mock_dil.return_value = False
        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)

        os.rename.reset_mock()
        self.c.backend.copystate.return_value = None
        mock_dil.return_value = True
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.simple_transfer_failures)
        self.assertEqual(fmd, type(self).clrq.get())
        os.rename.assert_called_once_with(fmd.file_name + Constants.copying_ext,
                  fmd.file_name + Constants.tobecopied_ext)
        os.kill.assert_called_once_with(751981, signal.SIGKILL)
        self.c.backend.remove.assert_called_once_with(dst, fmd.eos_instance, ANY)


    def test_transfer_process_failure(self):
        self.c.MaxCopyImmediateRetry = 0
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)

        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)

        self.c.backend.copystate.return_value = 1
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.problematic_transfer_failures)
        self.assertEqual(fmd, type(self).clrq.get())


    def test_transfer_failure_local_remote_filesize(self):
        self.c.MaxCopyImmediateRetry = 0
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)

        self.c.backend.sizechecksum.return_value = (2, 'deadbeef') # wrong size
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.problematic_transfer_failures)
        self.assertEqual(fmd, type(self).clrq.get())


    def test_transfer_failure_remote_db_filesize(self):
        self.c.MaxCopyImmediateRetry = 0
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        ct.db = unittest.mock.Mock(spec_set=Database.Database)
        ct.db.FileInfo.return_value = ('deadbeef', 1423, 'SANE')
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)

        self.c.backend.sizechecksum.return_value = (2, 'deadbeef') # wrong size
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.problematic_transfer_failures)
        self.assertEqual(fmd, type(self).clrq.get())


    def test_transfer_failure_checksum(self):
        self.c.MaxCopyImmediateRetry = 0
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        ct.db = unittest.mock.Mock(spec_set=Database.Database)
        ct.db.FileInfo.return_value = ('deadbeef', 1423, 'SANE')
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)

        self.c.backend.sizechecksum.return_value = (1423, '00000000') # wrong csum
        ct.checkTransfers()
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.problematic_transfer_failures)
        self.assertEqual(fmd, type(self).clrq.get())


    def test_transfer_success_failed_db(self):
        # Check that a critical log message is generated when DB is configured,
        # transfer was successful, but DB is unreachable
        self.c.DBURL = 'beautifuldb'
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        ct.logger = unittest.mock.Mock()

        type(self).copyq.put(fmd)
        ct.startTransfers()
        self.assertEqual(1, ct.nbtransfers)
        # a warning is generated because filesize and checksum could not be
        # retrieved from DB
        ct.logger.warning.assert_called_once()

        ct.checkTransfers()
        # transfer succeeds
        self.assertEqual(0, ct.nbtransfers)
        self.assertEqual(1, ct.files_copied)
        # but critical log is generated
        ct.logger.critical.assert_called_once()


    def test_delete_disabled(self):
        self.c.DeleteEnabled = False
        ct = CopyThread.CopyThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq, type(self).ddmq)
        fmd = FMD('/srcdir/totransfer', '/eos/dummy', 'eosdummy', 0)
        type(self).copyq.put(fmd)
        ct.startTransfers()
        ct.checkTransfers()
        self.assertEqual(1, ct.files_copied)
        # file sent to manager thread and not to delete thread
        self.assertEqual(fmd, type(self).clrq.get())
        self.assertEqual(0, type(self).dq.qsize())


if __name__ == '__main__':
    unittest.main()
