import unittest
import mock
from mock import patch, call
import socket
import os

if __name__ == '__main__':
    import sys
    from os.path import dirname, abspath, join
    ## add CastorScript/Script to path so imports keeps working
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

import cs.Tools.LogConfig as LogConfig
import cs.Threads.ERSThread as ERSThread
from cs.Tools.ConfigParser import ConfigHolder

# As a developer, i would like to add a
# new LogBook for my module "myModule" so that I can log messages

# As developers, we would like to configure the build-in logging feature of python
# So that it works with the ERS / MTS system at the ATLAS experiment

class TestLogConfig(unittest.TestCase):

    def setUp(self):
        # We define what we want the TDAQ_APPLICATION_NAME should be
        # We want it to be unique to the host and configuration filename
        self.app_name =  "CastorScript-{}-forSpecialUseCase".format(socket.gethostname())
        ## the ConfigHolder object is mocked. We expect it to be properly configured.
        self.mock_cfg = mock.create_autospec(ConfigHolder)

        ## ConfigHolder will parse the filename of the config, to a FileName property.
        # We specify an ERS-specific configuration in a file and give it a name.
        self.mock_cfg.configure_mock(FileName="Conf-forSpecialUseCase.cfg",
                                        ERSPartition="initial",
                                        ERSEnvFatal="mts",
                                        ERSEnvError="mts",
                                        ERSEnvInfo="mts",
                                        ERSEnvWarning="mts",
                                        ERSEnvDebug="mts",
                                        ERSLogLevel="debug",
                                        EmailLogSender="anyone@cern.ch",
                                        EmailLogList=["noone@dkcern.ch"],
                                        EmailLogLevel="debug")

class TestMakeTDAQAppName(TestLogConfig):

    def test_it_should_create_config_specific_name(self):

        # We see that "myLog" has the defined name.
        self.assertEqual(self.app_name, LogConfig.make_tdaq_app_name(self.mock_cfg))

    def test_it_should_create_simple_name(self):
        simple_app_name = "CastorScript-{}".format(socket.gethostname())
        self.mock_cfg.configure_mock(FileName="Conf.cfg")
        # We see that "myLog" has the defined name.
        self.assertEqual(simple_app_name, LogConfig.make_tdaq_app_name(self.mock_cfg))

        self.mock_cfg.configure_mock(FileName="Config.cfg")
        self.assertEqual(simple_app_name, LogConfig.make_tdaq_app_name(self.mock_cfg))

class TestSetEnvironment(TestLogConfig):

    @patch.dict('os.environ', {'TDAQ_PARTITION': 'NotSet',
                                    'TDAQ_APPLICATION_NAME':"NotSet",
                                    'TDAQ_ERS_ERROR': "NotSet",
                                    'TDAQ_ERS_INFO':'NotSet',
                                    'TDAQ_ERS_WARNING':'NotSet',
                                    'TDAQ_ERS_DEBUG':'NotSet'})
    def test_it_should_set_env_vars_according_to_config(self):

        ## For the test, we need to create instance again but in the patched context,
        ## for the os.environ patch to work
        # We create the LogBook with a ConfigHolder object.
        ERSThread.set_environment(self.mock_cfg)

        ##
        def wasNotSetError(param, expected, actual):
            return "%s was not set to: %s but: %s" % ( param, expected, actual )

        # We check the environment variables.
        # We see they have been set, according to our configuration.
        self.assertEqual(self.app_name, os.environ['TDAQ_APPLICATION_NAME'], msg=wasNotSetError("TDAQ_APPLICATION_NAME", self.app_name, os.environ['TDAQ_APPLICATION_NAME']))
        self.assertEqual("initial", os.environ['TDAQ_PARTITION'], msg=wasNotSetError("TDAQ_PARTITION", "initial", os.environ['TDAQ_PARTITION']))
        self.assertEqual("mts", os.environ['TDAQ_ERS_ERROR'], msg=wasNotSetError("TDAQ_ERS_ERROR", "mts", os.environ['TDAQ_ERS_ERROR']))
        self.assertEqual("mts", os.environ['TDAQ_ERS_INFO'], msg=wasNotSetError("TDAQ_ERS_INFO", "mts", os.environ['TDAQ_ERS_INFO']))
        self.assertEqual("mts", os.environ['TDAQ_ERS_WARNING'], msg=wasNotSetError("TDAQ_ERS_WARNING", "mts", os.environ['TDAQ_ERS_WARNING']))
        self.assertEqual("mts", os.environ['TDAQ_ERS_DEBUG'], msg=wasNotSetError("TDAQ_ERS_DEBUG", "mts", os.environ['TDAQ_ERS_DEBUG']))

class TestEnableMailLogging(TestLogConfig):

    @patch('logging.getLogger')
    def test_it_should_request_root_logger(self, mock_getLogger):

        LogConfig.enable_mail_logging(self.mock_cfg)

        # We see that the the root logger gets requested
        CALLS = [call(),]
        mock_getLogger.assert_has_calls(CALLS, any_order=True)

if __name__ == "__main__":
    unittest.main()
