import unittest
import time
import threading
import os
import logging
import sys

if __name__ == '__main__':
    SCRIPT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

from cs.Threads.ERSThread import ERSThread
import cs.Tools.ConfigParser as ConfigParser

class TestERSThread(unittest.TestCase):
    base_file_name = 'ers_thread_test'

    def test_instantiate_handler_and_log_all_severities(self):
        base_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
        config = ConfigParser.ConfigHolder(
                os.path.join(base_dir, 'Configs/minimal.cfg'))

        # override ERS config
        config.LogDir = '.'
        config.ERSEnabled = True
        config.ERSTimeout = 1
        config.ERSLogLevel = 'debug'
        config.ERSPartition = 'initial'
        # redirect ers to files (no idea why it does not work when using
        # the same file for all severity)
        config.ERSEnvFatal = f'file({TestERSThread.base_file_name}.critical)'
        config.ERSEnvError = f'file({TestERSThread.base_file_name}.error)'
        config.ERSEnvWarning = f'file({TestERSThread.base_file_name}.warning)'
        config.ERSEnvInfo = f'file({TestERSThread.base_file_name}.info)'
        config.ERSEnvDebug = f'file({TestERSThread.base_file_name}.debug)'

        event = threading.Event()
        ers_thread = ERSThread(config, event)

        try:
            ers_thread.start()

            timeout_s = 5
            start_time = time.time()
            ok = (ers_thread.ers_handler is not None)
            while not ok and (time.time() < start_time + timeout_s):
                ok = (ers_thread.ers_handler is not None)
                time.sleep(1)
            self.assertIsNotNone(ers_thread.ers_handler)

            # allow all log levels at the root of loggers
            logging.getLogger().setLevel(logging.DEBUG)
            logging.critical("critical")
            logging.error("error")
            logging.warning("warning")
            logging.info("info")
            logging.debug("debug")

            for ext in ['critical', 'error', 'warning', 'info', 'debug']:
                with open(TestERSThread.base_file_name + '.' + ext, 'r') as ers_log_file:
                    lines = ers_log_file.readlines()
                    self.assertGreaterEqual(len(lines), 1)
                    self.assertTrue(ext in lines[0])
            event.set()
            ers_thread.join()
        except:
            event.set()
            ers_thread.join()
            raise


    @classmethod
    def setUpClass(cls):
        TestERSThread.cleanup()

    @classmethod
    def tearDownClass(cls):
        TestERSThread.cleanup()

    @classmethod
    def cleanup(cls):
        try: os.remove('ersthread.log')
        except OSError: pass # ignore missing files
        for ext in ['critical', 'error', 'warning', 'info', 'debug']:
            try: os.remove(TestERSThread.base_file_name + '.' + ext)
            except OSError: pass # ignore missing files


if __name__ == '__main__':
    unittest.main()
