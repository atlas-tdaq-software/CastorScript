import unittest
import unittest.mock
import os
import threading
import queue
from time import time
import logging
from copy import deepcopy
import sys
import pathlib
from pathlib import PosixPath as PP

if __name__ == '__main__':
    from os.path import dirname, abspath, join
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

import cs.Threads.ManagerThread as ManagerThread
from cs.Threads.FileMetaData import FileMetaData as FMD
import cs.Tools.ConfigParser as ConfigParser
import cs.Tools.Constants as Constants


class TestManagerThread(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.this_dir = os.path.dirname(__file__)
        cls.config_path = os.path.abspath(os.path.join(cls.this_dir, "../Configs/"))

        # do not access conf directly, use getBasicConf()
        cls.basic_conf = ConfigParser.ConfigHolder(os.path.join(
                cls.config_path, 'minimal.cfg'))
        cls.basic_conf.SrcDirs = ['/srcdir']
        cls.basic_conf.CopyDir = '/eos/dummy'
        cls.basic_conf.EOSInstance = 'eosdummy'
        cls.basic_conf.backend = unittest.mock.Mock(spec_set=cls.basic_conf.backend)

        cls.ev = threading.Event()
        cls.dblock = threading.Lock()
        cls.dq = queue.Queue() # delete queue
        cls.copyq = queue.Queue()
        cls.clrq = queue.Queue()

        # Patch (and save) a few stuff for all tests
        cls.backup_osgetsize = os.path.getsize
        os.path.getsize = unittest.mock.Mock()
        os.path.getsize.return_value = 1423 # filesize

        cls.backup_osrename = os.rename
        os.rename = unittest.mock.Mock()

        cls.backup_osremove = os.remove
        os.remove = unittest.mock.Mock()

        cls.backup_enable_file_logging = ManagerThread.enable_file_logging
        ManagerThread.enable_file_logging = mocklog

        cls.backup_pathlib_glob = pathlib.Path.glob
        pathlib.Path.glob = unittest.mock.Mock()

        cls.backup_gfmd = ManagerThread.ManagerThread.getFileMetaData
        ManagerThread.ManagerThread.getFileMetaData = mockgetFileMetaData


    @classmethod
    def tearDownClass(cls):
        os.path.getsize = cls.backup_osgetsize
        os.rename = cls.backup_osrename
        os.remove = cls.backup_osremove
        pathlib.Path.glob = cls.backup_pathlib_glob
        ManagerThread.enable_file_logging = cls.backup_enable_file_logging


    @classmethod
    def getBasicConf(cls):
        c = deepcopy(cls.basic_conf)
        return c


    def setUp(self):
        self.c = type(self).getBasicConf()
        with type(self).dq.mutex: type(self).dq.queue.clear()
        with type(self).copyq.mutex: type(self).copyq.queue.clear()
        with type(self).clrq.mutex: type(self).clrq.queue.clear()
        os.path.getsize.reset_mock()
        os.rename.reset_mock()
        os.remove.reset_mock()


    def test_initialize(self):
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/file1.data'
        file2 = '/srcdir/subdir/file2.data'
        file3 = '/srcdir/doesnotexist.data'
        pathlib.Path.glob.return_value = [
            PP(file1 + '.COPIED'),
            PP(file2 + '.COPIED'),
            PP(file3 + '.COPIED')]
        mt.initialize()
        self.assertEqual(2, len(mt.managedfiles))
        self.assertEqual(2, type(self).dq.qsize())
        # ordered is given by mock_getmtime
        self.assertEqual(file2, type(self).dq.get().file_name)
        self.assertEqual(file1, type(self).dq.get().file_name)
        # .COPIED files but missing data file: .COPIED must be deleted
        self.assertTrue(unittest.mock.call(file3 + '.COPIED')
                in os.remove.mock_calls)


    def test_basic(self):
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        fmd1 = FMD('/srcdir/file1.data', '/eos/dummy', 'eosdummy', 2)
        fmd2 = FMD('/srcdir/subdir/file2.data', '/eos/dummy/subdir', 'eosdummy', 1)
        pathlib.Path.glob.return_value = [PP(fmd1.file_name), PP(fmd2.file_name)]
        
        mt.searchFilesToTransfer()
        self.assertEqual(2, len(mt.managedfiles))
        self.assertEqual(2, type(self).copyq.qsize())

        # Check order and put into clrq (as if files had been copied and deleted)
        tmp = type(self).copyq.get()
        self.assertEqual(fmd2, tmp)
        type(self).clrq.put(tmp)
        tmp = type(self).copyq.get()
        self.assertEqual(fmd1, tmp)
        type(self).clrq.put(tmp)

        mt.clearManagedFiles()
        self.assertEqual(0, len(mt.managedfiles))


    def test_copy_disabled(self):
        self.c.CopyEnabled = False
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        pathlib.Path.glob.return_value = [PP('/srcdir/file1.data')]
        mt.searchFilesToTransfer()
        self.assertEqual(1, len(mt.managedfiles))
        self.assertEqual(0, type(self).copyq.qsize())
        self.assertEqual(1, type(self).dq.qsize())


    def test_exclude_helper_extensions(self):
        # DataFilePattern is too permissive (*): check that helper files are
        # excluded anyways
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/file1.data'
        pathlib.Path.glob.return_value = [
                PP(file1+Constants.tobecopied_ext),
                PP(file1+Constants.copying_ext),
                PP(file1+Constants.copied_ext),
                PP(file1+Constants.problematic_ext)]

        mt.searchFilesToTransfer()
        self.assertEqual(0, len(mt.managedfiles))


    def test_already_managed(self):
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/file1.data'
        pathlib.Path.glob.return_value = [PP(file1)]

        mt.searchFilesToTransfer()
        self.assertEqual(1, len(mt.managedfiles))
        self.assertEqual(file1, type(self).copyq.get().file_name)
        mt.searchFilesToTransfer()
        self.assertEqual(1, len(mt.managedfiles))
        self.assertEqual(0, type(self).copyq.qsize())


    def test_excludefileregex(self):
        self.c.ExcludeFileRegex = '.*DONOTTRANSFER.*'
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/file1.data'
        file2 = '/srcdir/file2DONOTTRANSFER.data'
        pathlib.Path.glob.return_value = [PP(file1), PP(file2)]

        mt.searchFilesToTransfer()
        self.assertEqual(1, len(mt.managedfiles))
        self.assertEqual(file1, type(self).copyq.get().file_name)


    def test_existingCOPIEDfile(self):
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/file1.data'
        pathlib.Path.glob.return_value = [PP(file1)]

        with unittest.mock.patch('os.path.isfile', new=mockisfile_existingCOPIEDfile):
            mt.searchFilesToTransfer()
        self.assertEqual(0, len(mt.managedfiles))


    def test_smallfiles(self):
        self.c.MinSizekB = 10
        self.c.RemoveSmallFiles = True
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/file1.data'
        pathlib.Path.glob.return_value = [PP(file1)]

        with unittest.mock.patch('os.path.getsize') as opgs:
            opgs.return_value = 1
            mt.searchFilesToTransfer()
        self.assertEqual(0, len(mt.managedfiles))
        os.remove.assert_called_once_with(file1)


    def test_problematic(self):
        self.c.ProblDelayLimit = 2
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        fmd = FMD('/srcdir/file1.data', '/eos/dummy', 'eosdummy', 2)
        pathlib.Path.glob.return_value = [PP(fmd.file_name)]

        with unittest.mock.patch('os.path.isfile', new=mockisfile_problematic):
            mt.searchFilesToTransfer()
            # On first check, problematic files are always retried
            self.assertEqual(1, len(mt.managedfiles))
            self.assertEqual(fmd, type(self).copyq.get())

            # clear from managed files
            type(self).clrq.put(fmd)
            mt.clearManagedFiles()
            self.assertEqual(0, len(mt.managedfiles))
            self.assertTrue(fmd.file_name in mt.problematic)

            mt.searchFilesToTransfer()
            # From the second check on, retry time is checked against ProblDelay
            # for now the timeout hasn't expired
            self.assertEqual(0, len(mt.managedfiles))
            self.assertEqual(0, type(self).copyq.qsize())
            self.assertTrue(fmd.file_name in mt.problematic)

            mt.logger = unittest.mock.Mock()
            with unittest.mock.patch('cs.Threads.ManagerThread.time') as mocktime:
                mocktime.return_value = time() + self.c.ProblDelay * 2
                mt.searchFilesToTransfer()
            # with patched time, timeout is expired and ProblDelay exceeded
            self.assertEqual(1, len(mt.managedfiles))
            self.assertEqual(fmd, type(self).copyq.get())
            # a critical log must be issued if the duration between retries
            # exceeds ProblDelayLimitd
            mt.logger.critical.assert_called_once()


    def test_missingdb(self):
        self.c.DBURL = 'basededonnees'
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/file1.data'
        pathlib.Path.glob.return_value = [PP(file1)]
        mt.logger = unittest.mock.Mock()
        mt.searchFilesToTransfer()
        # a warning message is logged with the filename in it
        mt.logger.warning.assert_called_once()
        self.assertTrue(file1 in mt.logger.warning.call_args[0])
        self.assertEqual(0, len(mt.managedfiles))


    def test_file_db(self):
        self.c.DBURL = 'basededonnees'
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        mt.db = unittest.mock.Mock()
        file1 = '/srcdir/file1.data'
        pathlib.Path.glob.return_value = [PP(file1)]
        mt.logger = unittest.mock.Mock()

        # first, DB can't be accessed
        mt.db.FileExists.return_value = (False, False)
        mt.searchFilesToTransfer()
        mt.logger.warning.assert_called_once()
        self.assertTrue(file1 in mt.logger.warning.call_args[0])
        self.assertEqual(0, len(mt.managedfiles))

        # then, file is not in DB
        mt.logger.reset_mock()
        mt.db.FileExists.return_value = (True, False)
        mt.searchFilesToTransfer()
        mt.logger.warning.assert_called_once()
        self.assertTrue(file1 in mt.logger.warning.call_args[0])
        self.assertEqual(0, len(mt.managedfiles))

        # finally file is in DB
        mt.db.FileExists.return_value = (True, True)
        mt.searchFilesToTransfer()
        self.assertEqual(1, len(mt.managedfiles))


    def test_file_removed(self):
        mt = ManagerThread.ManagerThread(self.c, type(self).ev, type(self).dblock,
                type(self).copyq, type(self).dq, type(self).clrq)
        file1 = '/srcdir/doesnotexist.data'
        pathlib.Path.glob.return_value = [PP(file1)]

        mt.searchFilesToTransfer()
        os.remove.assert_has_calls([
                unittest.mock.call(file1 + Constants.tobecopied_ext),
                unittest.mock.call(file1 + Constants.copying_ext),
                unittest.mock.call(file1 + Constants.copied_ext),
                unittest.mock.call(file1 + Constants.problematic_ext),
                ])


def mocklog(loggername, filename, config):
    # Mocks LogUtils.enable_file_logging
    del loggername, filename, config
    if mocklog.logger is None:
        mocklog.logger = logging.getLogger(__file__)
        mocklog.logger.setLevel(logging.DEBUG)
        mocklog.logger.addHandler(logging.StreamHandler(sys.stdout))
        mocklog.logger.disabled = True # comment this out if you want the logs
    return mocklog.logger
mocklog.logger = None

def mockgetFileMetaData(self, filename, confdir):
    # Mocks ManagerThread.getFileMetaData
    subdir = ''
    if self.conf.ReplicateTreeStructure and os.path.dirname(filename) != os.path.normpath(confdir):
        subdir = os.path.relpath(os.path.dirname(filename), os.path.normpath(confdir))

    remotedir = os.path.normpath(os.path.join(self.conf.CopyDir, subdir))
    eosinstance = self.conf.EOSInstance
    try:
        mtime = mockgetmtime(filename)
    except:
        return None
    return FMD(filename, remotedir, eosinstance, mtime)

def mockgetmtime(filename):
    if os.path.basename(filename) == 'file1.data': return 2
    if os.path.basename(filename) == 'file2.data': return 1
    if os.path.basename(filename) == 'file2.zip': return 0
    raise FileNotFoundError(filename)

def mockisfile_existingCOPIEDfile(filename):
    if filename == '/srcdir/file1.data.COPIED': return True
    return False

def mockisfile_problematic(filename):
    if filename == '/srcdir/file1.data.PROBLEMATIC': return True
    return False


if __name__ == '__main__':
    unittest.main()
