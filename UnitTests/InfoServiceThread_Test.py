import unittest
import unittest.mock
import threading
import queue

if __name__ == '__main__':
    import sys
    from os.path import dirname, abspath, join
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

from cs.Threads.InfoServiceThread import InfoServiceThread
from cs.Threads.CopyThread import CopyThread
from cs.Threads.DeleteThread import DeleteThread
from cs.Threads.ManagerThread import ManagerThread
from cs.Tools.ConfigParser import ConfigHolder

# as a developer I want to publish information to the information service
class TestInfoServiceThread(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        ## we mock the event so that the thread won't be left hanging
        self.event = threading.Event()

        ## we mock the copythread with queues
        self.mock_CopyThread = unittest.mock.create_autospec(CopyThread)

        self.copy_queue = queue.Queue(3)
        self.copy_queue.put("item 1")
        self.copy_queue.put("item 2")

        self.delete_queue = queue.Queue(3)
        self.delete_queue.put("item 3")
        self.delete_queue.put("item 4")
        self.delete_queue.put("item 5")

        self.mock_CopyThread.configure_mock(
            copyqueue = self.copy_queue,
            deletequeue = self.delete_queue,
            simple_transfer_failures = 5,
            problematic_transfer_failures = 5,
            files_copied = 5,
            transfers = ["one","two","three"],
            nbtransfers = 3)

        ## we mock the deletethread
        self.mock_DeleteThread = unittest.mock.create_autospec(DeleteThread)
        self.mock_DeleteThread.configure_mock(files_deleted=5)
        self.mock_DeleteThread.configure_mock(SrcDirs=[])

        ## we mock the managerthread
        self.mock_ManagerThread = unittest.mock.create_autospec(ManagerThread)
        self.mock_ManagerThread.getNbCurrentlyProblematic.return_value = 2

        ## we mock the configuration
        self.mock_cfg = unittest.mock.create_autospec(ConfigHolder)
        self.mock_cfg.configure_mock(ERSPartition="initial",
            LogDir="",
            LogLevel="",
            FileName="Conf_test.cfg",
            SrcDirs=[],
            ISPartition="initial",
            ISServer="RunParams",
            ISPublicationPeriod="1",
            ISFileSystemInfoEnabled=True,
            ISCpuStatsEnabled=True,
            ISStorageIoStatsDevices=['abc'],
            ISWaitAfterParitionValid=60)

        ## we're not interested in any logging for this test
        with unittest.mock.patch("cs.Threads.InfoServiceThread.enable_file_logging"):
            self.info_thread = InfoServiceThread(self.mock_cfg, self.event,
                    self.mock_CopyThread, self.mock_DeleteThread,
                    self.mock_ManagerThread)


class TestUpdateCSInfo(TestInfoServiceThread):

    def test_it_should_populate_the_IS_data_object(self):
        mock_ISObject = unittest.mock.Mock()
        with unittest.mock.patch('ispy.ISObject', return_value=mock_ISObject):
            self.info_thread.update_cs_info()

            self.assertGreater(mock_ISObject.uptime_seconds, 0.0)
            self.assertEqual(mock_ISObject.files_to_be_copied, 2)
            self.assertEqual(mock_ISObject.files_to_be_deleted, 3)
            self.assertEqual(mock_ISObject.files_being_copied, 3)
            self.assertEqual(mock_ISObject.files_copied,5)
            self.assertEqual(mock_ISObject.files_deleted,5)
            self.assertEqual(mock_ISObject.simple_transfer_failures,5)
            self.assertEqual(mock_ISObject.problematic_transfer_failures,5)
            self.assertEqual(mock_ISObject.files_currently_problematic,2)



class TestGetProcessUptimeInSeconds(TestInfoServiceThread):
    def test_it_should_return_a_positive_value(self):
        self.assertGreaterEqual(self.info_thread.get_process_uptime_in_seconds(), 0)


if __name__ == "__main__":
    unittest.main()
