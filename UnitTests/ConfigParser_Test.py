import unittest
import os
import re
import logging
import tempfile

if __name__ == '__main__':
    import sys
    from os.path import dirname, abspath, join
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

import cs.Tools.ConfigParser as ConfigParser

class TestConfigHolder(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.this_dir = os.path.dirname(__file__)
        cls.config_path = os.path.abspath(os.path.join(cls.this_dir, "../Configs/"))

    def __init__(self, *args, **kwargs):
        super(TestConfigHolder, self).__init__(*args, **kwargs)
        self.c = None # the config to test


    def test_default_values(self):
        # Enforcing persistence of default values
        self.c = ConfigParser.ConfigHolder(os.path.join(
                TestConfigHolder.config_path, 'minimal.cfg'))
        self.assert_equal_del('LogLevel', 'WARNING')
        self.assert_equal_del('LogFileMode', 'a')
        self.assert_equal_del('LogFileMaxBytes', 1000000)
        self.assert_equal_del('LogFileBackupCount', 4)
        self.assert_equal_del('EmailLogList', [])
        self.assert_equal_del('EmailLogLevel', 'CRITICAL')
        self.assert_equal_del('EmailLogSender', 'atlascdr@cern.ch')
        self.assert_equal_del('LockFilePattern', None)
        self.assert_equal_del('ERSEnabled', False)
        self.assert_equal_del('ERSTimeout', 10)
        self.assert_equal_del('ERSLogLevel', 'error')
        self.assert_equal_del('ERSPartition', 'initial')
        self.assert_equal_del('ERSEnvFatal', '')
        self.assert_equal_del('ERSEnvError', '')
        self.assert_equal_del('ERSEnvWarning', '')
        self.assert_equal_del('ERSEnvInfo', '')
        self.assert_equal_del('ERSEnvDebug', '')
        self.assert_equal_del('ISEnabled', False)
        self.assert_equal_del('ISPartition', 'initial')
        self.assert_equal_del('ISServer', 'RunParams')
        self.assert_equal_del('ISPublicationPeriod', 5)
        self.assert_equal_del('ISFileSystemInfoEnabled', False)
        self.assert_equal_del('ISWaitAfterParitionValid', 60)
        self.assert_equal_del('ISCpuStatsEnabled', False)
        self.assert_equal_del('ISStorageIoStatsDevices', [])
        self.assert_equal_del('DBURL', None)
        self.assert_equal_del('DBFileTable', '')
        self.assert_equal_del('DBLBTable', '')
        self.assert_equal_del('DBRunTable', '')
        self.assert_equal_del('DBReconnectTimeout', 3600)
        self.assert_equal_del('FilenameParser', 'BaseFileNameParser')
        self.assert_equal_del('FilenameParserConf', {})
        self.assert_equal_del('BackendModule', 'eosstorage')
        self.assert_equal_del('Keytab', None)
        self.assert_equal_del('KrbUser', None)
        self.assert_equal_del('KrbCache', None)
        self.assert_equal_del('RecursiveSourceFolder', True)
        self.assert_equal_del('ReplicateTreeStructure', True)
        self.assert_equal_del('ExcludeFileRegex', None)
        self.assert_equal_del('ManagerThreadClearedFilesPeriod', 10)
        self.assert_equal_del('ProblDelay', 600)
        self.assert_equal_del('ProblScalingFactor', 0.5)
        self.assert_equal_del('ProblDelayLimit', 36000)
        self.assert_equal_del('MinSizekB', None)
        self.assert_equal_del('RemoveSmallFiles', False)
        self.assert_equal_del('SkipSymlinkFiles', False)
        self.assert_equal_del('DrivenPool', [])
        self.assert_equal_del('CopyEnabled', True)
        self.assert_equal_del('MaxConcurrentCopy', 1)
        self.assert_equal_del('MaxCopyImmediateRetry', 2)
        self.assert_equal_del('StopOngoingTransferAfterLock', False)
        self.assert_equal_del('ComputeChecksumFromLocalFile', False)
        self.assert_equal_del('BWDevice', None)
        self.assert_equal_del('BWLimit', 0)
        self.assert_equal_del('DeleteEnabled', True)
        self.assert_equal_del('DeleteHighCriticalMark', 90)
        self.assert_equal_del('DeleteLowCriticalMark', 80)
        self.assert_equal_del('DeleteLowWaterMark', [0])
        self.assert_equal_del('DeleteIgnoreLock', False)
        self.assert_equal_del('DeleteFileSystemUsagePeriod', 300)
        self.assert_equal_del('DeleteMinFileAge', 0)
        self.assert_equal_del('MigrationCheck', False)
        self.assert_equal_del('MigrationCheckDelay', 1800)
        self.assert_equal_del('MigrationModuleName', None)
        self.assert_equal_del('MigrationModuleConf', {})
        self.assert_equal_del('DeleteExcludeFileRegex', None)
        self.assert_equal_del('DdmMonitoringEnabled', False)
        self.assert_equal_del('DdmMonitoringTimeout', 10)
        self.assert_equal_del('DdmMonitoringProxy', 'atlasgw-exp:3128')
        self.assert_equal_del('DdmMonitoringEndpoint', 'https://rucio-lb-prod.cern.ch/traces/')
        self.assert_equal_del('DdmMonitoringQueueMaxSize', 200)
        self.assert_equal_del('MigrationCheckMaxDurationHour', None)
        self.assert_equal_del('MigrationCheckMaxDurationLogLevel', None)

        # test and remove parameters that are valued in the config
        self.assert_equal_del('FileName', 'minimal.cfg')
        self.assert_equal_del('LogDir', '.')
        self.assert_equal_del('MainThreadEventTimeout', 1)
        self.assert_equal_del('SrcDirs', ['.'])
        self.assert_equal_del('DataFilePattern', ['*'])
        self.assert_equal_del('ManagerThreadEventTimeout', 1)
        self.assert_equal_del('CopyDir', '')
        self.assert_equal_del('EOSInstance', '')
        self.assert_equal_del('CopyThreadEventTimeout', 1)
        self.assert_equal_del('TransferTimeout', 0)
        self.assert_equal_del('DeleteThreadEventTimeout', 1)

        del self.c.backend
        self.assertEqual(len(self.c.__dict__), 0,
                f'untested parameters: {self.c.__dict__}')



    def test_modified_values(self):
        # Test that configured values are set correctly
        self.c = ConfigParser.ConfigHolder(os.path.join(
                TestConfigHolder.config_path,
                'configparsertest_modifiedvalues.cfg'))
        self.assert_equal_del('FileName', 'configparsertest_modifiedvalues.cfg')
        self.assert_equal_del('LogDir', '.')
        self.assert_equal_del('MainThreadEventTimeout', 1)
        self.assert_equal_del('SrcDirs', ['.'])
        self.assert_equal_del('DataFilePattern', ['*.data'])
        self.assert_equal_del('ManagerThreadEventTimeout', 1)
        self.assert_equal_del('CopyDir', 'dest')
        self.assert_equal_del('EOSInstance', 'eosdummy')
        self.assert_equal_del('CopyThreadEventTimeout', 1)
        self.assert_equal_del('TransferTimeout', 1)
        self.assert_equal_del('DeleteThreadEventTimeout', 1)

        self.assert_equal_del('LogLevel', 'globalinfo')
        self.assert_equal_del('LogFileMode', 'filemode')
        self.assert_equal_del('LogFileMaxBytes', 1)
        self.assert_equal_del('LogFileBackupCount', 1)
        self.assert_equal_del('EmailLogList', ['toto@africa.rain'])
        self.assert_equal_del('EmailLogLevel', 'mailinfo')
        self.assert_equal_del('EmailLogSender', 'bibi@cocolapin')
        self.assert_equal_del('LockFilePattern', '*.lock')
        self.assert_equal_del('ERSEnabled', True)
        self.assert_equal_del('ERSTimeout', 1)
        self.assert_equal_del('ERSLogLevel', 'ersinfo')
        self.assert_equal_del('ERSPartition', 'erspartition')
        self.assert_equal_del('ERSEnvFatal', 'file(ers.fatal)')
        self.assert_equal_del('ERSEnvError', 'file(ers.error)')
        self.assert_equal_del('ERSEnvWarning', 'file(ers.warning)')
        self.assert_equal_del('ERSEnvInfo', 'file(ers.info)')
        self.assert_equal_del('ERSEnvDebug', 'file(ers.debug)')
        self.assert_equal_del('ISEnabled', True)
        self.assert_equal_del('ISPartition', 'ispartition')
        self.assert_equal_del('ISServer', 'isserver')
        self.assert_equal_del('ISPublicationPeriod', 1)
        self.assert_equal_del('ISFileSystemInfoEnabled', True)
        self.assert_equal_del('ISWaitAfterParitionValid', 1)
        self.assert_equal_del('ISCpuStatsEnabled', True)
        self.assert_equal_del('ISStorageIoStatsDevices', ['abc', 'def'])
        self.assert_equal_del('DBURL', 'basededonnees')
        self.assert_equal_del('DBFileTable', 'filetable')
        self.assert_equal_del('DBLBTable', 'lbtable')
        self.assert_equal_del('DBRunTable', 'runtable')
        self.assert_equal_del('DBReconnectTimeout', 1)
        self.assert_equal_del('FilenameParser', 'filenameparser')
        self.assert_equal_del('FilenameParserConf', {'fnparam': 'fnvalue'})
        self.assert_equal_del('BackendModule', 'localreader')
        self.assertEqual(self.c.backend.config_dict['READ_SCRIPT'], 'readscript.sh')
        self.assert_equal_del('Keytab', 'kkeytab')
        self.assert_equal_del('KrbUser', 'kkrbuser')
        self.assert_equal_del('KrbCache', 'kkrbcache')
        self.assert_equal_del('RecursiveSourceFolder', False)
        self.assert_equal_del('ReplicateTreeStructure', False)
        self.assert_equal_del('ExcludeFileRegex', '.*excludefileregex.*')
        self.assert_equal_del('ManagerThreadClearedFilesPeriod', 1)
        self.assert_equal_del('ProblDelay', 1)
        self.assert_equal_del('ProblScalingFactor', 1)
        self.assert_equal_del('ProblDelayLimit', 1)
        self.assert_equal_del('MinSizekB', 1)
        self.assert_equal_del('RemoveSmallFiles', True)
        self.assert_equal_del('SkipSymlinkFiles', True)

        # DrivenPool is not straightforward to check
        self.assertEqual(type(self.c.DrivenPool), type([]))
        self.assertEqual(len(self.c.DrivenPool), 1)
        self.assertEqual(self.c.DrivenPool[0].targetdir, 'altdest')
        self.assertEqual(self.c.DrivenPool[0].eosinstance, 'eosdriven')
        self.assertEqual(self.c.DrivenPool[0].projecttag.re, re.compile('ptag'))
        del self.c.__dict__['DrivenPool']

        self.assert_equal_del('CopyEnabled', True)
        self.assert_equal_del('MaxConcurrentCopy', 10)
        self.assert_equal_del('MaxCopyImmediateRetry', 1)
        self.assert_equal_del('StopOngoingTransferAfterLock', True)
        self.assert_equal_del('ComputeChecksumFromLocalFile', True)
        self.assert_equal_del('BWDevice', 'eth0')
        self.assert_equal_del('BWLimit', 1)
        self.assert_equal_del('DeleteEnabled', True)
        self.assert_equal_del('DeleteHighCriticalMark', 1)
        self.assert_equal_del('DeleteLowCriticalMark', 1)
        self.assert_equal_del('DeleteLowWaterMark', [1])
        self.assert_equal_del('DeleteIgnoreLock', True)
        self.assert_equal_del('DeleteFileSystemUsagePeriod', 1)
        self.assert_equal_del('DeleteMinFileAge', 1)
        self.assert_equal_del('MigrationCheck', True)
        self.assert_equal_del('MigrationCheckDelay', 1)
        self.assert_equal_del('MigrationModuleName', 'cassette')
        self.assert_equal_del('MigrationModuleConf', {'mparam': 'mvalue'})
        self.assert_equal_del('DeleteExcludeFileRegex', '.*donotdelete.*')
        self.assert_equal_del('DdmMonitoringEnabled', True)
        self.assert_equal_del('DdmMonitoringTimeout', 1)
        self.assert_equal_del('DdmMonitoringProxy', 'ddmproxy')
        self.assert_equal_del('DdmMonitoringEndpoint', 'ddmendpoint')
        self.assert_equal_del('DdmMonitoringQueueMaxSize', 1)
        self.assert_equal_del('MigrationCheckMaxDurationHour', 64)
        self.assert_equal_del('MigrationCheckMaxDurationLogLevel', logging.ERROR)

        del self.c.backend
        self.assertEqual(len(self.c.__dict__), 0,
                f'untested parameters: {self.c.__dict__}')


    def assert_equal_del(self, p, v):
        """
        To make sure that new parameters are tested here, they are removed
        from the object after checking their expected value and at the end
        of the test we check that the object is empty
        """
        self.assertEqual(self.c.__dict__[p], v)
        del self.c.__dict__[p]


    def test_storage_iostats_devices(self):
        with open(os.path.join(TestConfigHolder.config_path, 'minimal.cfg')) as mini:
            config = mini.readlines()
        with tempfile.TemporaryDirectory() as tmpd:
            # modif config and write file
            config.append('ISEnabled: True\n')
            config.append(f"ISStorageIoStatsDevices: ['sd0', '/dev/sd1', '{tmpd}/linktosd2']\n")
            with open(f'{tmpd}/cfg', 'w') as cfgfile:
                cfgfile.write(''.join(config))
            # create mock device file and symlink
            with open(f'{tmpd}/sd2', 'w'): pass
            os.symlink(f'{tmpd}/sd2', f'{tmpd}/linktosd2')
            # test
            self.c = ConfigParser.ConfigHolder(f'{tmpd}/cfg')
            self.assertEqual(self.c.ISStorageIoStatsDevices, ['sd0', 'sd1', 'sd2'])


if __name__ == '__main__':
    unittest.main()
