import unittest
import unittest.mock
import os
import threading
import queue
from time import time, sleep
import logging
from copy import deepcopy
import sys

if __name__ == '__main__':
    from os.path import dirname, abspath, join
    SCRIPT_DIR = abspath(join(dirname(__file__), '../Script'))
    sys.path.append(SCRIPT_DIR)

import cs.Threads.DeleteThread as DeleteThread
from cs.Threads.FileMetaData import FileMetaData as FMD
import cs.Tools.ConfigParser as ConfigParser


def mocklog(loggername, filename, config):
    # Mocks LogUtils.enable_file_logging
    del loggername, filename, config
    if mocklog.logger is None:
        mocklog.logger = logging.getLogger(__file__)
        mocklog.logger.setLevel(logging.DEBUG)
        mocklog.logger.addHandler(logging.StreamHandler(sys.stdout))
        mocklog.logger.disabled = True # comment this out if you want the logs
    return mocklog.logger
mocklog.logger = None


def mockdel(dt, fmd):
    # Mocks DeleteThread.delete to remove actual file system interactions

    dt.logger.info('deleting file %s', fmd.file_name)
    #increment files deleted, used by the Information Service Thread
    dt.files_deleted += 1

    # Send back to manager thread for final cleaning
    dt.clearqueue.put(fmd)

    try:
        del dt.migration_waitroom[fmd.file_name]
    except KeyError:
        # There are several cases in which the files are not in the
        # migration waiting room: migration check is disabled, high critical
        # mark was reached, etc.
        pass


class TestDeleteThread(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.this_dir = os.path.dirname(__file__)
        cls.config_path = os.path.abspath(os.path.join(cls.this_dir, "../Configs/"))

        # do not access conf directly, use getBasicConf()
        cls.basic_conf = ConfigParser.ConfigHolder(os.path.join(
                cls.config_path, 'minimal.cfg'))
        cls.basic_conf.SrcDirs = ['/srcdir']

        # We want each test to deepcopy this one but module object cannot
        # be deepcopied, so we remove it from config
        cls.conf_backend = cls.basic_conf.backend
        del cls.basic_conf.__dict__['backend']

        cls.ev = threading.Event()
        cls.dblock = None
        cls.dq = queue.Queue() # delete queue
        cls.cq = queue.Queue() # clear queue


    @classmethod
    def getBasicConf(cls):
        c = deepcopy(cls.basic_conf)
        c.backend = cls.conf_backend
        return c


    def setUp(self):
        self.c = type(self).getBasicConf()
        with type(self).dq.mutex: type(self).dq.queue.clear()
        with type(self).cq.mutex: type(self).cq.queue.clear()


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread._getFileSystemUsage')
    def test_basic(self, mock_gfsu):
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)
        mock_gfsu.return_value = {'/': 10}
        fmd = FMD('/srcdir/todelete', '/eos/dummy', 'eosdummy', 0)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted)
        self.assertEqual(fmd, type(self).cq.get())


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread._getFileSystemUsage')
    def test_exclude_regex(self, mock_gfsu):
        self.c.DeleteExcludeFileRegex = '.*DONOTDELETE.*'
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)
        mock_gfsu.return_value = {'/': 10}

        fmd = FMD('/srcdir/fileDONOTDELETE.data', '/eos/dummy', 'eosdummy', 0)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).cq.get()) # but still sent to Manager

        fmd = FMD('/srcdir/todelete', '/eos/dummy', 'eosdummy', 0)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted)
        self.assertEqual(fmd, type(self).cq.get())


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread._getFileSystemUsage')
    def test_min_file_age(self, mock_gfsu):
        self.c.DeleteMinFileAge = 100
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)
        mock_gfsu.return_value = {'/': 10}

        fmd = FMD('/srcdir/tropjeune', '/eos/dummy', 'eosdummy', time())
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ

        fmd = FMD('/srcdir/todelete', '/eos/dummy', 'eosdummy', time() - 110)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted)
        self.assertEqual(fmd, type(self).cq.get())


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('cs.Threads.DeleteThread.dir_is_locked')
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread._getFileSystemUsage')
    def test_locked_dir(self, mock_gfsu, mock_dil):
        self.c.DeleteIgnoreLock = False
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)
        mock_gfsu.return_value = {'/': 10}

        mock_dil.return_value = True
        fmd = FMD('/srcdir/file', '/eos/dummy', 'eosdummy', 0)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ

        # unlock directory
        mock_dil.return_value = False
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted)
        self.assertEqual(fmd, type(self).cq.get())


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread._getFileSystemUsage')
    def test_low_watermark(self, mock_gfsu):
        self.c.DeleteLowWaterMark = [20]
        self.c.DeleteFileSystemUsagePeriod = 1
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)

        mock_gfsu.return_value = {'/': 10}
        fmd = FMD('/srcdir/file', '/eos/dummy', 'eosdummy', 0)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ

        # make file system usage high enough
        # but file still not deleted because cache not updated
        # (has to happen within 2 seconds from previous iteration)
        mock_gfsu.return_value = {'/': 30}
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ

        # sleep to trigger fs usage cache update
        sleep(1)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted)
        self.assertEqual(fmd, type(self).cq.get())


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('os.path.getctime')
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.checkMigration')
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.getFileSystemUsage')
    def test_migration_delay(self, mock_gfsu, mock_cm, mock_gct):
        self.c.MigrationCheck = True
        self.c.MigrationCheckDelay = 1
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)
        mock_gfsu.return_value = 10
        mock_cm.return_value = True
        mock_gct.return_value = time()

        fmd = FMD('/srcdir/file', '/eos/dummy', 'eosdummy', 0)
        # file is migrated but MigrationCheckDelay is not yet expired
        # has to happen within MigrationCheckDelay seconds from previous time() call
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ
        self.assertTrue('/srcdir/file' in dt.migration_waitroom)

        # same test: this time the transfer time is accessed through the dictionary
        mock_gct.return_value = 0 # if ctime is (wrongly) accessed again file will be deleted
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ
        self.assertTrue('/srcdir/file' in dt.migration_waitroom)

        # sleep to expire migration check delay
        sleep(1)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted)
        self.assertEqual(fmd, type(self).cq.get())


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.checkMigration')
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.getFileSystemUsage')
    def test_migration(self, mock_gfsu, mock_cm):
        self.c.MigrationCheck = True
        self.c.MigrationCheckDelay = 0
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)
        mock_gfsu.return_value = 10

        mock_cm.return_value = False
        fmd = FMD('/srcdir/file', '/eos/dummy', 'eosdummy', 0)
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ

        mock_cm.return_value = True
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted)
        self.assertEqual(fmd, type(self).cq.get())


    @unittest.mock.patch('cs.Threads.DeleteThread.enable_file_logging', new=mocklog)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.delete', new=mockdel)
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.checkMigration')
    @unittest.mock.patch('cs.Threads.DeleteThread.DeleteThread.getFileSystemUsage')
    def test_critical_marks(self, mock_gfsu, mock_cm):
        self.c.MigrationCheck = True
        self.c.MigrationCheckDelay = 0
        self.c.DeleteHighCriticalMark = 90
        self.c.DeleteLowCriticalMark = 80
        dt = DeleteThread.DeleteThread(self.c, type(self).ev,
                type(self).dblock, type(self).dq, type(self).cq)
        mock_cm.return_value = False
        fmd = FMD('/srcdir/file', '/eos/dummy', 'eosdummy', 0)

        mock_gfsu.return_value = 10
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ

        mock_gfsu.return_value = 85 # still not
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(0, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ

        mock_gfsu.return_value = 95 # yes, even if not migrated
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(1, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).cq.get()) # and put back in DeleteQ

        mock_gfsu.return_value = 85 # still since we haven't been below the low critical mark
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(2, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).cq.get()) # and put back in DeleteQ

        mock_gfsu.return_value = 75 # still since we haven't been below the low critical mark
        type(self).dq.put(fmd)
        dt.loopIteration()
        self.assertEqual(2, dt.files_deleted) # file not deleted
        self.assertEqual(fmd, type(self).dq.get()) # and put back in DeleteQ


if __name__ == '__main__':
    unittest.main()
